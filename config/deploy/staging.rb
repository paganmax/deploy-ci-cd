set :stage, :staging
set :deploy_to, '/var/www/vrmalldemo'
set :repo_url, 'git@github.com:agt-hainm/vrmall_gcp.git'
set :branch, 'main'

server '34.146.252.241', user: 'hainm', roles: %w{web app db}

fetch(:default_env).merge!(wp_env: :staging)
