<?php

/**
 * Plugin Name: VRMALL MOCK API
 */

if (!defined('WPINC')) {
    die;
}

class VrmallMockApi
{
    public static function inventories()
    {
        return file_exists($path = __DIR__.'/data/inventories.json')
            ? json_decode(file_get_contents($path), true)
            : [];
    }

    public static function storeShopItems(WP_REST_Request $request)
    {
        if (null === $body = $request->get_json_params()) {
            return [];
        }

        file_put_contents(__DIR__.'/data/items.json', json_encode($body));

        return $body;
    }

    public static function getShopItems()
    {
        return file_exists($path = __DIR__.'/data/items.json')
            ? json_decode(file_get_contents($path), true)
            : [];
    }
}

add_action('rest_api_init', function () {
    register_rest_route('vrmall', 'inventories', [
        'methods' => 'GET',
        'callback' => [VrmallMockApi::class, 'inventories'],
    ]);

    register_rest_route('vrmall', 'shop/items', [
        'methods' => 'POST',
        'callback' => [VrmallMockApi::class, 'storeShopItems'],
    ]);

    register_rest_route('vrmall', 'shop/items', [
        'methods' => 'GET',
        'callback' => [VrmallMockApi::class, 'getShopItems'],
    ]);
});
