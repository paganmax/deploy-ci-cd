<?php

if ( ! defined('WPINC')) {
    die;
}

add_filter('manage_users_columns', function ($columns) {
    unset($columns['posts']);
    $columns['user_registered']    = __('Date');
    $columns['user_phone_number']  = __('電話番号');
    $columns['user_active_status'] = __('有効/非有効');

    return $columns;
});

add_action('manage_users_custom_column', function ($value, $column_name, $user_id) {
    $user = get_userdata($user_id);
    if ('user_registered' == $column_name) {
        return $user->user_registered;
    }

    if ('user_phone_number' == $column_name) {
        return get_user_meta($user_id, 'billing_phone', true);
    }

    if ('user_active_status' == $column_name) {
        $status = get_user_meta($user_id, 'active', true);

        if ($status === '') {
            return '';
        }

        return intval($status) === 0 ? '非有効' : '有効';
    }

    return $value;
}, 10, 3);

add_action('admin_head', function () {
    global $_wp_admin_css_colors;
    $_wp_admin_css_colors = 0;
});

add_filter('woocommerce_customer_meta_fields', function ($show_fields) {
    unset($show_fields['shipping']);

    return $show_fields;
});

add_action('admin_enqueue_scripts', function () {
    wp_enqueue_script('custom_user_delete_page_admin_scripts',
        plugin_dir_url(__FILE__).'/scripts/admin-delete-user.js');
});


function remove_user_role_not_use()
{
    wp_roles()->remove_role('editor');
    wp_roles()->remove_role('author');
    wp_roles()->remove_role('contributor');
    wp_roles()->remove_role('customer');
    wp_roles()->remove_role('translator');
}

add_action('init', 'remove_user_role_not_use');

function hide_role_not_use()
{
    wp_enqueue_script('my_custom_script_hide_role', plugins_url('scripts/hide_role_not_use.js', __FILE__),
        array('jquery'),
        '1.0',
        true);
}

add_action('wp_enqueue_scripts', 'hide_role_not_use');
add_action('admin_enqueue_scripts', 'hide_role_not_use');


add_filter('user_row_actions', function ($actions, $user_object) {
    unset($actions['view']);
    return $actions;
}, 10, 2);

function hide_button_login()
{
    if (is_user_logged_in()) {
        wp_enqueue_script('my_custom_script_button_login', plugins_url('scripts/hide_button_login_when_login.js', __FILE__),
            array('jquery'),
            '1.0',
            true);
    }
}

add_action('wp_enqueue_scripts', 'hide_button_login');
