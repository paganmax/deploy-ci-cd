<?php

/**
 * Plugin Name: VRMALL
 */

if (! defined('WPINC')) {
    die;
}

require_once 'api/route.php';
require_once 'booth.php';
require_once 'product.php';
require_once 'decoration.php';
require_once 'post-media.php';
require_once 'option-pages.php';
require_once 'custom-acf.php';
require_once 'dashboard.php';
require_once 'auth.php';
require_once 'send-email.php';
//require_once 'sign-up.php';
require_once 'sign-up-function.php';
require_once 'sign-up-sub.php';
require_once 'sign-up-shop.php';
require_once 'hide-woocommerce.php';
require_once 'cf7-custom.php';
require_once 'forms/production_request/production_request.php';
require_once 'forms/contact-form-mail-page.php';
require_once 'validation-password.php';
require_once 'export-csv.php';
require_once 'custom-user-page.php';
require_once 'change-wordpress-login-logo.php';
require_once "hide-post-and-page.php";
