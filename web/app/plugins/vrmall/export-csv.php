<?php

if (! defined('WPINC')) {
    die;
}

add_action('views_edit-3d_model_request', function ($views) {
    $url = admin_url('admin-ajax.php?action=3d_model_request_export_csv&nonce='.wp_create_nonce('3d_model_request_export_csv'));
    $views['export_csv'] = '<a href="'.$url.'" target="_blank" class="page-title-action">CSV出力</a>';
    return $views;
});

add_action('wp_ajax_3d_model_request_export_csv', 'vrmall_3d_model_request_export_csv_ajax_handler');
add_action('wp_ajax_nopriv_3d_model_request_export_csv', 'vrmall_3d_model_request_export_csv_ajax_handler');

function vrmall_3d_model_request_export_csv_ajax_handler()
{
    if (! current_user_can('manage_options')) {
        wp_send_json_error();
    }

    ob_start();

    $f = fopen('php://output', 'w');

    $items = get_posts([
        'post_status' => 'any',
        'post_type' => '3d_model_request',
        'posts_per_page' => -1,
    ]);

    fputcsv($f, [
        'お問合せ元',
        'メールアドレス',
        '電話番号',
        '日付',
        '商品1',
        '商品2',
        '商品3',
        '商品4',
        '商品5',
        '商品6',
        '商品7',
        '商品8',
        '商品9',
        '商品10',
    ]);

    foreach ($items as $item) {
        $fields = get_fields($item->ID);

        $request_user = $fields['vrmall_3d_model_request_user'] ?? [];
        $request_user_email = $fields['vrmall_3d_model_request_email'] ?? '';
        $request_user_phone = $fields['vrmall_3d_model_request_phone_number'] ?? '';
        $products = $fields['vrmall_3d_model_request_products'] ?? [];

        $data = [
            $request_user['user_firstname'].' '.$request_user['user_lastname'],
            $request_user_email,
            $request_user_phone,
            $item->post_date,
        ];

        foreach ($products as $product) {
            $data[] = $product['vrmall_3d_model_request_product_name'] ?? '';
        }

        fputcsv($f, $data);
    }

    fclose($f);

    $csv = ob_get_clean();

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=3d_model_request_'.date('Ymd').'.csv');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    echo "\xEF\xBB\xBF";
    echo $csv;

    exit();
}
