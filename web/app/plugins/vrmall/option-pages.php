<?php

if (! defined('WPINC')) {
    die;
}

if (function_exists('acf_add_options_page')) {
    $parent = acf_add_options_page([
        'page_title' => __('一般設定'),
        'menu_title' => __('一般設定'),
        'menu_slug' => 'vrmall-settings',
        'capability' => 'manage_options',
        'update_button' => __('更新'),
        'updated_message' => __('新しい設定は更新されました'),
    ]);

    acf_add_options_sub_page([
        'page_title' => __('一般設定'),
        'menu_title' => __('一般設定'),
        'menu_slug' => 'vrmall-settings-general',
        'parent_slug' => $parent['menu_slug'],
        'capability' => 'manage_options',
        'update_button' => __('更新'),
        'updated_message' => __('新しい設定は更新されました'),
    ]);

    acf_add_options_sub_page([
        'page_title' => __('シーン設定'),
        'menu_title' => __('シーン設定'),
        'menu_slug' => 'vrmall-settings-scene',
        'parent_slug' => $parent['menu_slug'],
        'capability' => 'manage_options',
        'update_button' => __('更新'),
        'updated_message' => __('新しい設定は更新されました'),
    ]);
}
