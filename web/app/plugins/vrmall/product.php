<?php

if ( ! defined('WPINC')) {
    die;
}

function restrict_index_product(WP_Query $query)
{
    if ( ! function_exists('get_current_screen')) {
        return $query;
    }

    if ( ! $query->is_admin || 'edit-product' !== get_current_screen()->id) {
        return $query;
    }

    if ( ! current_user_can('manage_options') && 'product' === $query->get('post_type')) {
        $query->set('author', get_current_user_id());
    }

    return $query;
}

function restrict_editing_product($capabilities, $capability, $args)
{
    if (('edit_post' != $args[0] && 'delete_post' != $args[0]) || ! empty($capabilities['manage_options']) || empty($capabilities['edit_posts'])) {
        return $capabilities;
    }

    $post = get_post($args[2]);

    if ($post->post_type !== 'product') {
        return $capabilities;
    }

    if ((int) $post->post_author !== get_current_user_id()) {
        $capabilities[$capability[0]] = false;
    }

    return $capabilities;
}

function restrict_count_product(WP_Query $query)
{
    if ( ! function_exists('get_current_screen')) {
        return $query;
    }

    if ( ! $query->is_admin || 'edit-product' !== get_current_screen()->id) {
        return $query;
    }

    if ( ! current_user_can('manage_options') && 'product' === $query->get('post_type')) {
        add_filter('views_edit-product', function ($views) {
            return fix_post_counts($views, 'product');
        });
    }

    return $query;
}

add_filter('pre_get_posts', 'restrict_index_product');
add_filter('pre_get_posts', 'restrict_count_product');
add_filter('user_has_cap', 'restrict_editing_product', 10, 3);

function hide_import_button($hook)
{
    wp_enqueue_script('my_custom_script_product', plugins_url('scripts/hide-import-product.js', __FILE__), array('jquery'),
        '1.0',
        true);
}

add_action('admin_enqueue_scripts', 'hide_import_button');
