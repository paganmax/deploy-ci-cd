<?php

if ( ! defined('WPINC')) {
    die;
}


function my_enqueue($hook)
{
    if ('user-new.php' != $hook) {
        return;
    }
    wp_enqueue_script('my_custom_script_add_user', plugins_url('scripts/sign_up_scripts.js', __FILE__), array('jquery'),
        '1.0',
        true);
}

add_action('admin_enqueue_scripts', 'my_enqueue');

function my_enqueue_disable($hook)
{
    if ('profile.php' != $hook && 'user-edit.php' != $hook) {
        return;
    }
    wp_enqueue_script('my_custom_script_update_user', plugins_url('scripts/visible_sign_up_scripts.js', __FILE__), array('jquery'),
        '1.0',
        true);
}

add_action('admin_enqueue_scripts', 'my_enqueue_disable');


function my_enqueue_contact_form()
{
    wp_enqueue_script('my_custom_script_contact_form', plugins_url('scripts/contact_form_scripts.js', __FILE__), array('jquery'),
        '1.0',
        true);
}

add_action('wp_enqueue_scripts', 'my_enqueue_contact_form');

function get_user_by_meta_data($meta_key, $meta_value)
{
    $user_query = new WP_User_Query(
        array(
            "meta_key"   => $meta_key,
            "meta_value" => $meta_value,
            "fields"     => "ID"
        )
    );

    return $user_query->get_results();
}


function custom_filter_wpcf7_is_tel($result, $tel)
{
    $result = preg_match('/^[+]?[0-9][0-9]{9,10}$/', $tel);

    return $result;
}

add_filter('wpcf7_is_tel', 'custom_filter_wpcf7_is_tel', 10, 2);


function my_wpcf7_form_elements($html)
{
    $text = '▽ お選びください';
    $html = str_replace('--Please Select--', ''.$text.'', $html);

    return $html;
}

add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');


function custom_user_profile_fields($user)
{

    if ( ! current_user_can('manage_options')) {
        return false;
    }
    if (is_object($user)) {

        $user_active     = get_user_meta($user->ID, "active", true);
        $display_active  = get_user_meta($user->ID, "display_active", true);
        $name_store      = get_user_meta($user->ID, "name_store", true);
        $department      = get_user_meta($user->ID, "department", true);
        $know_sign_sub   = get_user_meta($user->ID, "know_sign_sub", true);
        $reason_sign_sub = get_user_meta($user->ID, "reason_sign_sub", true);

        $id_signup = esc_attr(get_the_author_meta('id_signup', $user->ID));

        if ($user_active == 1) {
            $active = "入力済";
        } else {
            $active = "未入力";
        }
    } else {
        $id_signup = null;
    } ?>
    <div id="info_sugmit">
        <h3>お客様の基本情報</h3>
        <table class="form-table">
            <tr style="display: none">
                <th><label for="id_signup">User sign up</label></th>
                <td>
                    <input type="text" class="regular-text" name="id_signup" value="<?php echo $id_signup ?? ""; ?>"
                           id="id_signup"/><br/>
                    <input type="text" class="regular-text" name="status_active"
                           value="<?php echo $display_active ?? ""; ?>"
                           id="status_active"/><br/>
                </td>
            </tr>
            <tr>
                <th><label>基本情報入力状態</label></th>
                <td>
                    <label id="active"><?php echo $active ?></label><br/>
                </td>
            </tr>
            <tr>
                <th><label>所属部署/役職</label></th>
                <td>
                    <input type="text" class="regular-text" name="department"
                           id="department" value="<?php echo $department ?? ""; ?>"/><br/>
                </td>
            </tr>
            <tr>
                <th><label for="id_signup">店舗名（屋号）</label></th>
                <td>
                    <input type="text" class="regular-text" name="name_store"
                           id="name_store" value="<?php echo $name_store ?? ""; ?>"/><br/>
                </td>
            </tr>
            <tr>
                <th><label>展示会の開催を何で知りましたか</label></th>
                <td>
                    <textarea rows="3" cols="30" name="know_sign_sub" id="know_sign_sub"
                              style="width: 350px;margin-bottom:6px;text-align: left;"
                              value="<?php echo $know_sign_sub ?? "" ?>">
                              <?php echo $know_sign_sub ?? "" ?>
                        </textarea>
                </td>
            </tr>
            <tr>
                <th><label>ご来場の目的</label></th>
                <td>
                   <textarea rows="3" cols="30" name="reason_sign_sub" id="reason_sign_sub"
                             style="width: 350px;margin-bottom:6px;text-align: left;"
                             value="<?php echo $reason_sign_sub ?? "" ?>">
                          <?php echo $reason_sign_sub ?? "" ?>
                </textarea>
                </td>
            </tr>
        </table>
    </div>
    <?php
}

add_action('show_user_profile', 'custom_user_profile_fields');
add_action('edit_user_profile', 'custom_user_profile_fields');
add_action("user_new_form", "custom_user_profile_fields");



add_action('admin_enqueue_scripts','loadScripts');
function loadScripts()
{
    wp_enqueue_style("jquery-ui-css", "http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/themes/smoothness/jquery-ui.min.css");
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-dialog');
    wp_enqueue_style( 'wp-jquery-ui-dialog' );
}
