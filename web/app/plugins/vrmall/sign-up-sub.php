<?php

if ( ! defined('WPINC')) {
    die;
}

add_filter('wpcf7_validate', 'email_sub_already_in_db', 10, 2);

function email_sub_already_in_db($result, $tags)
{
    $form = WPCF7_Submission::get_instance();

    $contact_form    = WPCF7_ContactForm::get_current();
    $contact_form_id = $contact_form->id();
    if ($contact_form_id == FORM_SIGN_UP_SUB_STEP_1) {
        $email = $form->get_posted_data('your-email-sign-sub');
        $user  = get_user_by("email", $email);
        if (array_intersect(["subscriber", "shop_manager"], $user->roles)) {
            $result->invalidate('your-email-sign-sub', EMAIL_EXISTS);
        };
    }

    return $result;
}


add_filter('wpcf7_k_password_validation_filter', 'my_wpcf7_validate', 10, 2);
function my_wpcf7_validate($result, $tag)
{

    $form = WPCF7_Submission::get_instance();
    $pass = $form->get_posted_data('your-pass-sign-sub');
    if (strlen($pass) < 8) {
        $result->invalidate($tag, __("英数字含む８桁以上を入力する必要があります。", 'your-pass-sign-sub'));
    }

    if (preg_match("/([^a-zA-Z0-9])/", $pass)) {
        $result->invalidate($tag,
            __("数字と文字しかご入力できません", 'your-pass-sign-sub'));
    }

    if ( ! preg_match("/(?=.*[0-9])(?=.*[a-zA-Z])/", $pass) && ! preg_match("/([^a-zA-Z0-9])/", $pass)) {
        $result->invalidate($tag,
            __("数字と文字混合をご入力ください", 'your-pass-sign-sub'));
    }

    return $result;

}

//step 1
function save_email_to_send_sub_token_registration($contact_form)
{
    $submission = WPCF7_Submission::get_instance();

    $posted_data = $submission->get_posted_data();

    $contact_form    = WPCF7_ContactForm::get_current();
    $contact_form_id = $contact_form->id();
    if ($contact_form_id == FORM_SIGN_UP_SUB_STEP_1) {
        if (empty($invalid_fields)) {
            $user = get_user_by("email", $posted_data["your-email-sign-sub"]);

            if ( ! array_intersect(["subscriber", "shop_manager"], $user->roles)) {
                $token = bin2hex(openssl_random_pseudo_bytes(16));

                $random_value   = rand(1, 999999);
                $create_user_id = str_pad($random_value, 5, '0', STR_PAD_LEFT);

                $user_query = array(
                    'user_login' => $create_user_id,
                    'user_email' => $posted_data["your-email-sign-sub"],
                    'role'       => 'subscriber',
                );
                $user_id    = wp_insert_user($user_query);

                add_user_meta($user_id, "user_sub_token", $token);
                add_user_meta($user_id, "active", 0);
                add_user_meta($user_id, "display_active", 1);


                //send email
                $title   = get_field('title_email_active_sub', 'options');
                $message = get_field('content_email_active_sub', 'options');
                $headers = ['Content-Type: text/html; charset=UTF-8'];

                $link_token = get_home_url().'/subscriber/?ut='.$token;

                $replaces = [
                    '[get_link_token]' => sprintf("<a href ='".$link_token."'>$link_token</a>"),
                ];

                $message = strtr($message, $replaces);

                $contact_form->skip_mail = true;

                wp_mail($posted_data["your-email-sign-sub"], $title, $message, $headers);
            }
        }
    }

}

add_action('wpcf7_before_send_mail', 'save_email_to_send_sub_token_registration', 10, 1);


add_action('wp_footer', 'redirect_page_sub');

function redirect_page_sub()
{ ?>
    ​
    <script type="text/javascript">
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        jQuery(document).ready(function () {
            // invalid
            document.addEventListener('wpcf7invalid', function (e) {
                if (<?php echo FORM_SIGN_UP_SUB_STEP_1 ?> == e.detail.contactFormId
            )
                {
                    var respone = e.detail.apiResponse.invalid_fields;

                    let message = '<?php echo EMAIL_EXISTS ?>';

                    for (element of respone) {
                        if (element.message.includes(message)) {
                            jQuery('.wpcf7-response-output').hide(0);
                            jQuery('#dialog_sign_sub').modal('show');
                        }
                    }
                }
            }, false);

            // after send mail
            document.addEventListener('wpcf7mailsent', function (e) {
                if (<?php echo FORM_SIGN_UP_SUB_STEP_2 ?> == e.detail.contactFormId
            )
                {
                    jQuery('.wpcf7-response-output').hide(0);
                    jQuery('#success_dialog').modal('show');
                }
            }, false);

        });
    </script>
    <?php
}


// step 2

function create_new_user_subscribe_registration($contact_form)
{
    $submission = WPCF7_Submission::get_instance();

    $posted_data     = $submission->get_posted_data();
    $contact_form    = WPCF7_ContactForm::get_current();
    $contact_form_id = $contact_form->id;

    if ($contact_form_id == FORM_SIGN_UP_SUB_STEP_2) {
        if (empty($invalid_fields)) {
            $user_query = new WP_User_Query(
                array(
                    "meta_key"   => "user_sub_token",
                    "meta_value" => $posted_data['your-token-sign-sub'],
                    "fields"     => "ID"
                )
            );

            $users      = $user_query->get_results();
            $user_id    = $users[0];
            $user_email = get_userdata($users[0])->user_email;

            if ( ! empty($users[0])) {
                wp_set_password($posted_data["your-pass-sign-sub"], $user_id);

                wp_update_user(
                    array(
                        'ID'           => $user_id,
                        'display_name' => $posted_data["your-name-sign-sub"],
                    )
                );

                update_user_meta($user_id, "first_name", $posted_data["your-name-sign-sub"]);
                update_user_meta($user_id, "active", 1);
                update_user_meta($user_id, "user_sub_token", "");

                update_user_meta($user_id, "billing_email", $user_email);
                update_user_meta($user_id, "billing_first_name", $posted_data["your-name-sign-sub"]);
                update_user_meta($user_id, "billing_company", $posted_data["your-company-sign-sub"]);
                update_user_meta($user_id, "billing_address_1", $posted_data["your-address-sign-sub"]);
                update_user_meta($user_id, "billing_phone", $posted_data["your-phone-sign-sub"]);


                $your_know = str_replace(", その他", "", implode(", ", $posted_data["your-know-sign-sub"]));
                if ( ! empty($posted_data["your-know-another-sign-sub"])) {
                    $your_know = $your_know.", ".$posted_data["your-know-another-sign-sub"];
                }

                $your_reason = str_replace(", その他", "", implode(", ", $posted_data["your-reason-sign-sub"]));
                if ( ! empty($posted_data["your-reason-another-sign-sub"])) {
                    $your_reason .= ", ".$posted_data["your-reason-another-sign-sub"];
                }

                add_user_meta($user_id, "know_sign_sub", $your_know);
                add_user_meta($user_id, "reason_sign_sub", $your_reason);

                $name    = $posted_data["your-name-sign-sub"];
                $company = $posted_data["your-company-sign-sub"];
                $address = $posted_data["your-address-sign-sub"];
                $phone   = $posted_data["your-phone-sign-sub"];


                $replaces = [
                    '[name_subscriber]'            => $name,
                    '[email_subscriber]'           => $user_email,
                    '[company_name_subscriber]'    => $company,
                    '[address_subscriber]'         => $address,
                    '[phone_subscriber]'           => $phone,
                    '[why_know_vrmall_subscriber]' => $your_know,
                    '[reason_sign_up_subscriber]'  => $your_reason,
                ];


                $contact_form->skip_mail = true;
                $headers                 = ['Content-Type: text/html; charset=UTF-8'];

                //mail to sub
                $title   = get_field('title_email_success_sub', 'options');
                $message = get_field('content_email_success_sub', 'options');
                $message = strtr($message, $replaces);
                wp_mail($user_email, $title, $message, $headers);

                //mail to admin
                $title_to_admin   = get_field('title_email_admin_sign_sub', 'options');
                $message_to_admin = get_field('content_email_admin_sign_up', 'options');
                $message_to_admin = strtr($message_to_admin, $replaces);
                wp_mail(get_option('admin_email'), $title_to_admin, $message_to_admin, $headers);
            }

        }
    }
}


add_action('wpcf7_before_send_mail', 'create_new_user_subscribe_registration', 10, 1);

function enqueue_scripts()
{
    wp_register_script('global-script', plugins_url('scripts/variable_global.js', __FILE__), array('jquery'), '', true);
    wp_localize_script('global-script', 'script_vars', array(
        'VRUI_BASE_URL' => rtrim(\Roots\WPConfig\Config::get('VRUI_BASE_URL'))
    ));
    wp_enqueue_script('global-script');
}

add_action('wp_enqueue_scripts', 'enqueue_scripts');
add_action('admin_enqueue_scripts', 'enqueue_scripts');

function hide_button_modal()
{
    wp_enqueue_script('my_custom_script_button_modal', plugins_url('scripts/hide_modal_contact_form.js', __FILE__),
        array('jquery'),
        '1.0',
        true);
}

add_action('wp_enqueue_scripts', 'hide_button_modal');


