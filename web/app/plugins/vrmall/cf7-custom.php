<?php

if (! defined('WPINC')) {
    die;
}

add_action('wpcf7_init', function () {
    wpcf7_add_form_tag('vrmall_booth_name', function ($tag) {
        $value = '';
        $validation_error = wpcf7_get_validation_error($tag->name);
        $class = wpcf7_form_controls_class($tag->type, 'wpcf7-text');

        if (isset($_GET['booth_id']) && $booth_id = intval($_GET['booth_id'])) {
            $booth = get_post($booth_id);

            if ($booth && $booth->post_type === 'booth') {
                $value = $booth->post_title;
            }
        }

        $atts = [];
        $atts['type'] = 'text';
        $atts['readonly'] = 'readonly';
        $atts['size'] = $tag->get_size_option('40');
        $atts['class'] = $tag->get_class_option($class);
        $atts['value'] = $value;
        $atts['name'] = $tag->name;

        $atts = wpcf7_format_atts($atts);

        $html = sprintf(
            '<span class="wpcf7-form-control-wrap %1$s"><input %2$s />%3$s</span>',
            sanitize_html_class($tag->name), $atts, $validation_error
        );

        return $html;
    }, ['name-attr' => true,]);
});

add_action('wpcf7_before_send_mail', 'wpcf7_change_recipient');

function wpcf7_change_recipient($form)
{
    $submission = WPCF7_Submission::get_instance();
    $posted_data = $submission->get_posted_data();

    // 来場者から出展者様へのお問合せフォーム
    if ($posted_data['form_name'] === 'inquiry' && $booth_id = $posted_data['booth_id']) {
        $owners = vrmall_get_booth_owners($booth_id);

        if (is_array($owners) && isset($owners[0])) {
            $owner = $owners[0];

            $mail = $form->prop('mail');
            $mail['recipient'] = $owner->user_email;
            $form->set_properties(['mail' => $mail]);
        }
    }
}

/**
 * @param $booth_id
 * @return WP_User[]
 */
function vrmall_get_booth_owners($booth_id): array
{
    $args = [
        'count_total' => false,
        'role' => 'shop_manager',
        'meta_query' => [
            [
                'key' => 'booths',
                'value' => '"'.$booth_id.'"',
                'compare' => 'LIKE',
            ],
        ],
    ];

    return (new WP_User_Query($args))->get_results();
}
