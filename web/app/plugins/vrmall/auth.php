<?php

if (! defined('WPINC')) {
    die;
}

add_filter('jwt_auth_token_before_dispatch', function ($data, WP_User $user) {
    $data['roles'] = $user->roles ?? [];

    return $data;
}, 10, 2);
