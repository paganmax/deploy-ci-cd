<?php

function get_current_user_booth_ids(): array
{
    $fields = get_fields('user_'.get_current_user_id());

    if (is_array($posts = $fields['booths'] ?? [])) {
        return array_map(function (WP_Post $post) {
            return $post->ID;
        }, $posts);
    }

    return [];
}

// Fix post counts
function fix_post_counts($views, $post_type)
{
    global $wp_query;
    unset($views['mine']);

    $types = [
        ['status' => null],
        ['status' => 'publish'],
        ['status' => 'draft'],
        ['status' => 'pending'],
        ['status' => 'trash'],
    ];
    foreach ($types as $type) {
        $query = [
            'post_type' => $post_type,
            'post_status' => $type['status'],
        ];

        if ($post_type === 'booth') {
            $query['post__in'] = get_current_user_booth_ids();
        } else {
            $query['author'] = get_current_user_id();
        }

        $result = new WP_Query($query);
        if ($type['status'] == null) :
            $class = ($wp_query->query_vars['post_status'] == null) ? ' class="current"' : '';
            $views['all'] = sprintf(
                '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>',
                admin_url('edit.php?post_type='.$post_type),
                $class,
                $result->found_posts,
                __('All')
            );
        elseif ($type['status'] == 'publish') :
            $class = ($wp_query->query_vars['post_status'] == 'publish') ? ' class="current"' : '';
            $views['publish'] = sprintf(
                '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>',
                admin_url('edit.php?post_status=publish&post_type='.$post_type),
                $class,
                $result->found_posts,
                __('Publish')
            );
        elseif ($type['status'] == 'draft') :
            $class = ($wp_query->query_vars['post_status'] == 'draft') ? ' class="current"' : '';
            $views['draft'] = sprintf(
                '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>',
                admin_url('edit.php?post_status=draft&post_type='.$post_type),
                $class,
                $result->found_posts,
                __('Draft')
            );
            hide_or_un_hide_draft($result, $views);
        elseif ($type['status'] == 'pending') :
            $class = ($wp_query->query_vars['post_status'] == 'pending') ? ' class="current"' : '';
            $views['pending'] = sprintf(
                '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>',
                admin_url('edit.php?post_status=pending&post_type='.$post_type),
                $class,
                $result->found_posts,
                __('Pending')
            );
            hide_or_un_hide_pending($result, $views);
        elseif ($type['status'] == 'trash') :
            $class = ($wp_query->query_vars['post_status'] == 'trash') ? ' class="current"' : '';
            $views['trash'] = sprintf(
                '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>',
                admin_url('edit.php?post_status=trash&post_type='.$post_type),
                $class,
                $result->found_posts,
                __('Trash')
            );
            hide_or_un_hide_trash($result, $views);
        endif;
    }

    return $views;
}

function hide_or_un_hide_trash($result, &$views)
{
    if ($result->found_posts <= 0) {
        unset($views['trash']);
    }
}

function hide_or_un_hide_pending($result, &$views)
{
    if ($result->found_posts <= 0) {
        unset($views['pending']);
    }
}

function hide_or_un_hide_draft($result, &$views)
{
    if ($result->found_posts <= 0) {
        unset($views['draft']);
    }
}
