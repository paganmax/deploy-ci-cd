<?php

if ( ! defined('WPINC')) {
    die;
}

function remove_dashboard_widgets()
{

    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
    remove_meta_box('dashboard_activity', 'dashboard', 'normal');//since 3.8
    remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
    remove_meta_box('woocommerce_dashboard_status', 'dashboard', 'normal');
    remove_meta_box('woocommerce_dashboard_recent_reviews', 'dashboard', 'normal');
    remove_meta_box('wc_admin_dashboard_setup', 'dashboard', 'normal');

    remove_action('welcome_panel', 'wp_welcome_panel');
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets', 20);

function remove_core_updates()
{
    global $wp_version;

    return (object) ['last_checked' => time(), 'version_checked' => $wp_version,];
}

add_filter('pre_site_transient_update_core', 'remove_core_updates');
add_filter('pre_site_transient_update_plugins', 'remove_core_updates');
add_filter('pre_site_transient_update_themes', 'remove_core_updates');

function hide_box_dashboard()
{
    echo '<style>
    #dashboard-widgets .postbox-container .empty-container{
        display: none;
    }
  </style>';
}

add_action('admin_head', 'hide_box_dashboard');

function remove_menu_wp_admin()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_node('view-store');
    $wp_admin_bar->remove_node('view-site');
    $wp_admin_bar->remove_menu('search');
    $wp_admin_bar->remove_node('widgets');
    if ( ! current_user_can('manage_options')) {
        $wp_admin_bar->remove_menu('customize');
        $wp_admin_bar->remove_menu('edit');
        $wp_admin_bar->remove_node('menus');
        $wp_admin_bar->remove_node('themes');
    }
}

add_action('wp_before_admin_bar_render', 'remove_menu_wp_admin', 0);

add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;

    if ($pagenow === 'edit-comments.php' || $pagenow === 'options-discussion.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page and option page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
    remove_submenu_page('options-general.php', 'options-discussion.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});


add_filter('admin_title', 'my_admin_title', 10, 2);

function my_admin_title($admin_title, $title)
{
    return get_bloginfo('name').' &bull; '.$title;
}


add_filter('login_title', 'custom_login_title', 99, 1);
function custom_login_title($origtitle)
{
    return get_bloginfo('name');
}

add_action('template_redirect', 'redirect_login_when_not_login');

function redirect_login_when_not_login()
{
    global $wp;
    $url = add_query_arg($wp->query_vars);

    if ( ! is_user_logged_in() && (str_contains(strtolower($url),
                strtolower("/my-account/?page&pagename=my-account")) || str_contains(strtolower($url),
                strtolower("/my-account/edit-account/")) ||
                                   str_contains(strtolower($url),
                                       strtolower("/my-account/customer-logout/")))) {
        wp_redirect(WP_SITEURL.'/wp-login.php');
        exit();
    }
}

function admin_bar()
{

    if (is_user_logged_in()) {
        add_filter('show_admin_bar', '__return_true', 1000);
    }
}

add_action('init', 'admin_bar');


add_action('init', 'redirect_dashboard_when_user_login');
function redirect_dashboard_when_user_login()
{
    global $wp;
    $current_user = wp_get_current_user();
    $user_data    = get_userdata($current_user->ID);
    $user_roles   = $user_data->roles ?? [];
    $url          = add_query_arg($wp->query_vars);

    if (($url === "/wp/wp-login.php" || str_contains(strtolower($url),
                strtolower("/wp/wp-login.php?redirect_to="))) && is_user_logged_in() && array_intersect([
            "administrator", "shop_manager"
        ], $user_roles)) {
        wp_redirect(admin_url());
        exit();
    }

    if (($url === "/wp/wp-login.php" || str_contains(strtolower($url),
                strtolower("/wp/wp-login.php?redirect_to="))) && is_user_logged_in() && ! array_intersect([
            "administrator", "shop_manager"
        ], $user_roles)) {
        wp_redirect(home_url().'/my-account/');
        exit();
    }
}

