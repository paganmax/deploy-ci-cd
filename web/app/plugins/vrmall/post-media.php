<?php

if ( ! defined('WPINC')) {
    die;
}

function restrict_index_media(WP_Query $query)
{
    if ($query->get('post_type') === 'attachment' && ! current_user_can('manage_options')) {
        $query->set('author', get_current_user_id());
    }

    $query->set('meta_query', [
        [
            'key' => 'vrmall_video_thumbnail',
            'compare' => 'NOT EXISTS',
        ],
    ]);

    return $query;
}

add_filter('pre_get_posts', 'restrict_index_media');

function upload_mime_types()
{
    $list_mime            = wp_get_mime_types();
    $list_mime_can_upload = [
        "jpg|jpeg|jpe" => "image/jpeg",
        "png"          => "image/png",
        "mp4|m4v"      => "video/mp4",
        "pdf"          => "application/pdf",
        "zip"          => "application/zip",
        "gz|gzip"      => "application/x-gzip",
        "rar"          => "application/rar"
    ];

    if ( ! current_user_can('manage_options')) {
        $list_mime = $list_mime_can_upload;
    }
    $more_mime = [
        "bin"  => "application/octet-stream",
        "gltf" => "application/json",
        "obj"  => "text/plain",
        "zip"  => "application/zip",
        "mtl"  => "text/plain",
        "stl"  => "application/octet-stream",
        "fbx"  => "text/plain",
    ];

    return array_merge($list_mime, $more_mime);
}

add_filter('upload_mimes', 'upload_mime_types', 1, 1);

function generate_video_thumbnail($metadata, $attachment_id)
{
    $mime_type = get_post_mime_type($attachment_id);

    if (false === strpos($mime_type, 'video/') || ! shell_exec('command -v ffmpeg')) {
        return $metadata;
    }

    $filepath = get_attached_file($attachment_id);

    $upload_dir = wp_upload_dir();
    $base_path = pathinfo($filepath, PATHINFO_DIRNAME);
    $filename = pathinfo($filepath, PATHINFO_FILENAME);
    $output_path = $base_path.'/'.$filename.'.png';

    $output = ltrim(str_replace($upload_dir['basedir'], '', $base_path), '/');

    shell_exec(sprintf('ffmpeg -i "%s" -ss 00:00:01.000 -vframes 1 "%s"', $filepath, $output_path));

    $attachment = [
        'post_mime_type' => 'image/png',
        'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
        'post_content' => '',
        'post_status' => 'inherit',
    ];

    $thumb_id = wp_insert_attachment($attachment, $output.'/'.basename($output_path), $attachment_id);

    add_post_meta($thumb_id, 'vrmall_video_thumbnail', '1');

    $attach_data = wp_generate_attachment_metadata($thumb_id, $output_path);
    wp_update_attachment_metadata($thumb_id, $attach_data);

    set_post_thumbnail($attachment_id, $thumb_id);

    return $metadata;
}

add_filter('wp_generate_attachment_metadata', 'generate_video_thumbnail', 10, 3);

function delete_video_thumbnail_after_video_delete($id)
{
    global $wpdb;

    $post = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_parent = %d", $id));

    if ($post) {
        $post = get_post($post);

        if ('attachment' === $post->post_type) {
            wp_delete_attachment($post->ID, true);
        }
    }
}

add_action('delete_attachment', 'delete_video_thumbnail_after_video_delete', 10, 1);

add_image_size('booth_preparing_xl', 1024, 512, true);
add_image_size('booth_preparing_l', 768, 512, true);
add_image_size('booth_preparing_m', 512, 512, true);
add_image_size('signboard', 1840, 640, true);

add_filter('acf/validate_attachment/name=vrmall_booth_signboard_outside_signboard_image', 'signboard_validate_attachment', 10, 5);

function signboard_validate_attachment($errors, $file, $attachment, $field, $context)
{
    $required_width = 1840;
    $required_height = 640;
    $error_message = '警告: 画像のサイズが正しくありません';

    if (isset($attachment['id'])) {
        $width = $attachment['width'];
        $height = $attachment['height'];
    } else if (isset($attachment['tmp_name'])) {
        [$width, $height] = getimagesize($attachment['tmp_name']);
    }

    if (isset($width) && isset($height)) {
        if ($height !== (int) round(($required_height / $required_width) * $width)) {
            $errors[] = __($error_message);
        }
    }

    return $errors;
}
