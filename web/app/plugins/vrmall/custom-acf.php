<?php

if (! defined('WPINC')) {
    die;
}

add_filter('acf/settings/save_json', 'customAcfJsonSavePoint');

function customAcfJsonSavePoint(): string
{
    return \Roots\WPConfig\Config::get('WP_CONTENT_DIR').'/acf-json';
}
