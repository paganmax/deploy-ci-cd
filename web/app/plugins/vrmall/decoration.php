<?php

if (! defined('WPINC')) {
    die;
}

function restrict_index_decoration(WP_Query $query)
{
    if (! function_exists('get_current_screen')) {
        return $query;
    }

    if (! $query->is_admin || 'edit-decoration' !== get_current_screen()->id) {
        return $query;
    }

    if (! current_user_can('manage_options') && 'decoration' === $query->get('post_type')) {
        $query->set('author', get_current_user_id());
    }

    return $query;
}

function restrict_editing_decoration($capabilities, $capability, $args)
{
    if (('edit_post' != $args[0] && 'delete_post' != $args[0]) || ! empty($capabilities['manage_options']) || empty($capabilities['edit_posts'])) {
        return $capabilities;
    }

    $post = get_post($args[2]);

    if ($post->post_type !== 'decoration') {
        return $capabilities;
    }

    if ((int) $post->post_author !== get_current_user_id()) {
        $capabilities[$capability[0]] = false;
    }

    return $capabilities;
}

function restrict_count_decoration(WP_Query $query)
{
    if (! function_exists('get_current_screen')) {
        return $query;
    }

    if (! $query->is_admin || 'edit-decoration' !== get_current_screen()->id) {
        return $query;
    }

    if (! current_user_can('manage_options') && 'decoration' === $query->get('post_type')) {
        add_filter('views_edit-decoration', function ($views) {
            return fix_post_counts($views, 'decoration');
        });
    }

    return $query;
}

add_filter('pre_get_posts', 'restrict_index_decoration');
add_filter('pre_get_posts', 'restrict_count_decoration');
add_filter('user_has_cap', 'restrict_editing_decoration', 10, 3);
