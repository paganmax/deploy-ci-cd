<?php
if ( ! defined('WPINC')) {
    die;
}

function remove_add_to_cart()
{
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
}

add_action('init', 'remove_add_to_cart');

function custom_my_account_menu_items($items)
{
    unset($items['orders']);
    unset($items['view-order']);
    unset($items['payment-methods']);
    unset($items['edit-address']);
    unset($items['downloads']);
    $items['edit-account'] = "基本情報";

    return $items;
}

add_filter('woocommerce_account_menu_items', 'custom_my_account_menu_items');


function chance_text_billing_to_information_wc($translated_text, $text, $domain)
{
    switch ($translated_text) {
        case '名':
            $translated_text = __('名前', 'woocommerce');
            break;
        case 'アカウントセクションとレビューではこの名前が表示されます':
            $translated_text = __('', 'woocommerce');
            break;
    }

    return $translated_text;
}

add_filter('gettext', 'chance_text_billing_to_information_wc', 20, 3);


add_filter('woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98);
function wcs_woo_remove_reviews_tab($tabs)
{
    unset($tabs['reviews']);

    return $tabs;
}

add_filter('woocommerce_save_account_details_required_fields', 'wc_save_account_details_required_fields');
function wc_save_account_details_required_fields($required_fields)
{
    unset($required_fields['account_last_name']);

    return $required_fields;
}

function change_text_account_woo()
{
    ?>
    <style type="text/css">
        .woocommerce-EditAccountForm .woocommerce-form-row--last {
            display: none !important;
        }

        .woocommerce-EditAccountForm .woocommerce-form-row--first {
            width: 881px !important;
        }
    </style>
    <?php
}

add_action('wp_enqueue_scripts', 'change_text_account_woo');


add_filter('wp_logout', 'woocemerce_redirect_after_logout', 20, 1);

function woocemerce_redirect_after_logout()
{

    wp_redirect(WP_SITEURL.'/wp-login.php');
    exit();
}


function woocommerce_new_pass_redirect()
{
    wp_redirect(WP_SITEURL.'/wp-login.php');
    exit();
}

add_action('woocommerce_customer_reset_password', 'woocommerce_new_pass_redirect');
