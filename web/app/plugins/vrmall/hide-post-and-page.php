<?php
if ( ! defined('WPINC')) {
    die;
}


function hide_permalink_detail_post()
{ ?>
    <style type="text/css">
        /*post*/
        .post-php.post-type-post #edit-slug-box {
            display: none;
        }

        .post-php.post-type-post #post-preview {
            display: none;
        }

        /*skybox*/
        .post-php.post-type-skybox #edit-slug-box {
            display: none;
        }

        .post-php.post-type-skybox #post-preview {
            display: none;
        }

        /*decoration*/
        .post-php.post-type-decoration #edit-slug-box {
            display: none;
        }

        .post-php.post-type-decoration #post-preview {
            display: none;
        }

        /*hall*/
        .post-php.post-type-hall #edit-slug-box {
            display: none;
        }

        .post-php.post-type-hall #post-preview {
            display: none;
        }

        /*booth_template*/
        .post-php.post-type-booth_template #edit-slug-box {
            display: none;
        }

        .post-php.post-type-booth_template #post-preview {
            display: none;
        }

        /*booth_decoration*/
        .post-php.post-type-booth_decoration #edit-slug-box {
            display: none;
        }

        .post-php.post-type-booth_decoration #post-preview {
            display: none;
        }

        /*signupshop*/
        .post-php.post-type-signupshop #edit-slug-box {
            display: none;
        }

        .post-php.post-type-signupshop #post-preview {
            display: none;
        }

        /*product*/
        .post-php.post-type-product #edit-slug-box {
            display: none;
        }

        .post-php.post-type-product #post-preview {
            display: none;
        }

        /*page*/
        .post-php.post-type-page #edit-slug-box {
            display: none;
        }

        .post-php.post-type-page #post-preview {
            display: none;
        }
    </style>
    <?php
}

add_action('admin_enqueue_scripts', 'hide_permalink_detail_post');


function ts_redirect_product_pages()
{

    if (is_singular('post') || is_singular('skybox') ||
        is_singular('decoration') || is_singular('hall') ||
        is_singular('booth_template') || is_singular('booth_decoration')
        || is_singular('signupshop') || is_singular('product')
        || is_singular('shop') || is_singular('cart')) {
        wp_safe_redirect(home_url()."/404");
        exit;
    }
}

add_action('template_redirect', 'ts_redirect_product_pages');
