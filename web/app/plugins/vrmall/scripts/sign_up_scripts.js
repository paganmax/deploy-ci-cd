jQuery(document).ready(function () {
    const queryString = location.search;
    const urlParams = new URLSearchParams(queryString);
    const email = urlParams.get('email');
    const post_id = urlParams.get('post_id');
    const name = urlParams.get('name');
    const user_id = urlParams.get('create_id');
    const role = urlParams.get('role');

    const department = urlParams.get('department');
    const name_store = urlParams.get('name_store');
    const active = urlParams.get('active');

    jQuery('textarea#know_sign_sub').html(jQuery('textarea#know_sign_sub').html().trim());

    jQuery('textarea#reason_sign_sub').html(jQuery('textarea#reason_sign_sub').html().trim());

    if (post_id != null) {
        jQuery('#info_sugmit').show();

        jQuery('#id_signup').val(post_id);
        jQuery('#user_login').val(user_id);
        jQuery('#email').val(email);
        jQuery('#first_name').val(name);
        jQuery('#role').val(role);
        jQuery('#department').val(department);
        jQuery('#name_store').val(name_store);
        jQuery('#active').val(active);
        jQuery('#active').text(active);
    } else {
        jQuery('#info_sugmit').hide();
    }
});
