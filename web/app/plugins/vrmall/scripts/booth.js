jQuery(function () {
    jQuery('[data-name="vrmall_booth_signboard_outside"]')
        .find('.values > .layout[data-layout="vrmall_booth_signboard_outside_signboard_group"]')
        .find('[data-name="remove-layout"]')
        .remove();

    jQuery(document).on('click', '[data-event="add-row"]', function () {
        jQuery('[data-name="vrmall_booth_signboard_inside"]')
            .find('.values > .layout[data-layout="vrmall_booth_signboard_inside_decoration_group"]')
            .find('[data-name="vrmall_booth_signboard_inside_decoration_metadata"]')
            .css({ display: 'none' })
    });

    jQuery('[data-name="vrmall_booth_signboard_inside"]')
        .find('.values > .layout[data-layout="vrmall_booth_signboard_inside_decoration_group"]')
        .find('[data-name="vrmall_booth_signboard_inside_decoration_metadata"]')
        .css({ display: 'none' })

    jQuery('[data-name="vrmall_booth_signboard_inside"]')
        .find('.values > .layout[data-layout="vrmall_booth_signboard_inside_decoration_group"]')
        .find('[data-name="vrmall_booth_signboard_inside_decoration_metadata"] textarea')
        .each(function () {
            jQuery(this).data('vrmall_last_value', encodeURIComponent(jQuery(this).val()));
        })

    jQuery('[data-name="vrmall_booth_signboard_inside"]')
        .find('.values > .layout[data-layout="vrmall_booth_signboard_inside_decoration_group"]')
        .find('[data-name="vrmall_booth_signboard_inside_decoration"] select')
        .on('change', function () {
            const id = jQuery(this).val();
            const lastId = jQuery(this).data('vrmall_last_id');
            const lastMetadataValue = decodeURIComponent(jQuery(this).closest('.acf-field').next().find('textarea').data('vrmall_last_value'));

            if (id === lastId) {
                jQuery(this).closest('.acf-field').next().find('textarea').val(lastMetadataValue);
            } else {
                jQuery(this).closest('.acf-field').next().find('textarea').val('');
            }
        });

    jQuery('[data-name="vrmall_booth_signboard_inside"]')
        .find('.values > .layout[data-layout="vrmall_booth_signboard_inside_decoration_group"]')
        .find('[data-name="vrmall_booth_signboard_inside_decoration"] select').each(function () {
        var value = jQuery(this).val();
        jQuery(this).data('vrmall_last_id', value);
    })
});
