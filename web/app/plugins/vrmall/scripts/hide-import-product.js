jQuery(document).ready(function () {
    var anchor = document.getElementsByClassName("page-title-action");
    for (let i = 0; i < anchor.length; i++) {
        let item = jQuery(anchor[i]);

        if (item.attr('href').toLowerCase().indexOf("/wp/wp-admin/edit.php?post_type=product&page=product_importer") >= 0) {
            item.hide();
        }

        if (item.attr('href').toLowerCase().indexOf("/wp/wp-admin/edit.php?post_type=product&page=product_exporter") >= 0) {
            item.hide();
        }
    }
});
