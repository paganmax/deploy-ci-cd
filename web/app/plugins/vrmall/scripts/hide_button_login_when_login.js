jQuery(document).ready(function () {
    const queryString = location.search;
    const urlParams = new URLSearchParams(queryString);
    const url = window.location.origin + "/wp/wp-login.php";
    let a = jQuery("a");
    for (let i = 0; i < a.length; i++) {
        let item = jQuery(a[i]);
        if (item.attr('href') == "/wp/wp-login.php") {
            item.hide();
        }
    }
});
