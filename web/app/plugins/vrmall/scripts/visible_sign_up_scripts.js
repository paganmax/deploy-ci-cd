jQuery(document).ready(function () {

    const queryString = location.search;
    const urlParams = new URLSearchParams(queryString);
    const user_id = urlParams.get('user_id');
    const post_id = urlParams.get('post_id');
    const role = urlParams.get('role');
    const company = urlParams.get('company');
    const address = urlParams.get('address');
    const phone = urlParams.get('phone');
    const name = urlParams.get('name');
    const department = urlParams.get('department');
    const name_store = urlParams.get('name_store');

    jQuery('textarea#know_sign_sub').html(jQuery('textarea#know_sign_sub').html().trim());

    jQuery('textarea#reason_sign_sub').html(jQuery('textarea#reason_sign_sub').html().trim());

    //company
    if (user_id != null && post_id != null && role != null) {
        jQuery('#role').val(role);
        jQuery('#billing_company').val(company);
        jQuery('#billing_address_1').val(address);
        jQuery('#billing_phone').val(phone);
        jQuery('#first_name').val(name);
        jQuery('#billing_first_name').val(name);
        jQuery('#department').val(department);
        jQuery('#name_store').val(name_store);
    }
});
