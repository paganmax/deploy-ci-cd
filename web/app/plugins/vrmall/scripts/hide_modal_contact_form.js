jQuery(document).ready(function () {
    jQuery("#modal_close").click(function () {
        jQuery('#modal_close').modal('hide');
        window.location.href = script_vars.VRUI_BASE_URL;
    });

    jQuery("#success_modal_close").click(function () {
        jQuery('#success_dialog').modal('hide');
        window.location.href = script_vars.VRUI_BASE_URL;
    });
});
