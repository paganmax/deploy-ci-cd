jQuery(document).ready(function () {

    document.addEventListener( 'wpcf7submit', function( event ) {

        var button = jQuery('.wpcf7-submit[disabled]');

        var old_value = button.attr('data-value');

        button.prop('disabled', false);

        button.val(old_value);

    }, false );

    jQuery('form.wpcf7-form').on('submit',function() {

        var form = jQuery(this);
        var button = form.find('input[type=submit]');
        var current_val = button.val();

        button.attr('data-value', current_val);

        button.prop("disabled", true);

        button.val("送信中...");

    });


});


