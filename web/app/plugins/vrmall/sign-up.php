<?php

if ( ! defined('WPINC')) {
    die;
}


function filter_wpcf7_posted_data($posted_data)
{
    if ( ! empty($posted_data["your-company"]) && ! empty($posted_data["your-email"])
         && ! empty($posted_data["your-name"]) && ! empty($posted_data["your-address"])) {
        $my_query = array(
            'post_title'  => $posted_data["your-company"],
            'post_type'   => 'signup',
            'post_status' => 'publish',
            'post_author' => 1
        );
        $post_id  = wp_insert_post($my_query);
        update_field("field_612edd2cc1a5d", $posted_data["your-company"], $post_id);
        update_field("field_612edd85c1a5e", $posted_data["your-email"], $post_id);
        update_field("field_612ede04c1a5f", $posted_data["your-name"], $post_id);
        update_field("field_612ede1ec1a60", $posted_data["your-address"], $post_id);
    }

    return $posted_data;
}

add_filter('wpcf7_posted_data', 'filter_wpcf7_posted_data', 10, 1);


add_filter('manage_signup_posts_columns', 'set_custom_edit_signup_columns');
function set_custom_edit_signup_columns($columns)
{
    $offset = array_search('title', array_keys($columns));
    unset($columns['date']);
    unset($columns['title']);

    return array_merge(array_slice($columns, 0, $offset),
        ['company' => __('Company', 'text_domain')],
        ['email' => __('Email', 'text_domain')],
        ['name' => __('Name', 'text_domain')],
        ['address' => __('Address', 'text_domain')],
        ['submit_time' => __('Submit Time', 'text_domain')],
        ['create_account' => __('User', 'text_domain')]
        , array_slice($columns, $offset, null));
}

add_action('manage_signup_posts_custom_column', 'custom_signup_column', 10, 2);
function custom_signup_column($column, $post_id)
{
    switch ($column) {
        case 'company' :
            echo get_post_meta($post_id, 'company', true);
            break;
        case 'email' :
            echo get_post_meta($post_id, 'email', true);
            break;
        case 'name' :
            echo get_post_meta($post_id, 'name', true);
            break;
        case 'address' :
            echo get_post_meta($post_id, 'address', true);
            break;
        case 'submit_time' :
            echo get_the_time('Y/m/d', $post_id)." at ".get_the_time('g:i a', $post_id);
            break;
        case 'create_account' :
            $user_meta  = get_user_by_meta_data("id_signup", $post_id);
            $user_count = count($user_meta);

            if ($user_count <= 0) {

                $random_value   = rand(1, 999999);
                $create_user_id = str_pad($random_value, 5, '0', STR_PAD_LEFT);

                printf('<a class="button button-primary" href="'.get_home_url().'/wp/wp-admin/user-new.php?name='
                       .get_post_meta($post_id, 'name', true).'&post_id='
                       .$post_id.'&email='
                       .get_post_meta($post_id, 'email', true). '&create_id='
                       .$create_user_id.'
                    ">Add New User</a>');
            } else {
                $user_id   = $user_meta[0];
                $user_name = get_user_by('id', $user_id)->user_login;
                printf('<a href="'.get_home_url().'/wp/wp-admin/user-edit.php?user_id='.$user_id.'">User '.$user_name.' is created</label></a>');
            }
            break;
    }
}

function save_custom_user_profile_fields($user_id)
{
    if ( ! current_user_can('manage_options')) {
        return false;
    }

    update_user_meta($user_id, 'id_signup', $_POST['id_signup']);
}

add_action('user_register', 'save_custom_user_profile_fields');

function hide_edit_signup_update(){ ?>
    <style type="text/css">
        .post-php.post-type-signup #edit-slug-box {display:none;}
    </style><?php
}
add_action( 'admin_enqueue_scripts', 'hide_edit_signup_update' );




