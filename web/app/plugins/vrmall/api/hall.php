<?php

if (! defined('WPINC')) {
    die;
}

class Hall
{
    public function get_halls()
    {
        $posts = get_posts([
            'post_status' => 'publish',
            'post_type' => 'hall',
            'posts_per_page' => -1,
        ]);

        return ['data' => array_map([$this, 'map_hall'], $posts)];
    }

    public function store_hall_settings(WP_REST_Request $request)
    {
        $items = $request->get_param('data');

        if (! is_array($items)) {
            $response = new WP_REST_Response([
                'code' => 'bad_request',
                'message' => 'Request body not valid',
            ]);
            $response->set_status(400);

            return $response;
        }

        foreach ($items as $item) {
            $id = $item['id'] ?? null;

            if (! $id && ! get_post($id)) {
                continue;
            }

            $extend_point = $item['extend_point'] ?? '';
            $position_offset = $item['position_offset'] ?? [];
            $rotation_offset = $item['rotation_offset'] ?? [];

            update_post_meta($id, 'hall_settings', compact('extend_point', 'position_offset', 'rotation_offset'));
        }

        return ['data' => ['status' => 'OK']];
    }

    protected function map_hall(WP_Post $post)
    {
        $fields = get_fields($post->ID);
        $settings = get_post_meta($post->ID, 'hall_settings');
        $settings = isset($settings[0]) ? $settings[0] : $settings;

        return [
                'id' => $post->ID,
                'uid' => $fields['hall_uid'] ?? null,
                'name' => $post->post_title,
                'model_file' => $fields['hall_model_file'] ?? null,
                'floors' => array_map([$this, 'map_floor'], $fields['hall_floors'] ?? []),
            ] + $settings;
    }

    protected function map_floor($floor)
    {
        return [
            'id' => $floor['hall_floor_id'] ?? null,
            'name' => $floor['hall_floor_name'] ?? null,
            'display' => isset(($floor['hall_floor_display'] ?? [])[0]) && ($floor['hall_floor_display'] ?? [])[0] === 'true',
        ];
    }
}
