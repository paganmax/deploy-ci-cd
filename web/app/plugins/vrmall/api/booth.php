<?php

if (! defined('WPINC')) {
    die;
}

class Booth
{
    const SLOTS_ON_HALL = 'slots_on_hall';

    public static function booth_owner_is($booth_id, $user_id): bool
    {
        $fields = get_field_objects('user_'.$user_id);

        $ids = array_map(function ($item) {
            return $item->ID;
        }, $fields['booths']['value'] ?? []);

        return in_array($booth_id, $ids);
    }

    public function get_current_user_booths()
    {
        $user_id = get_current_user_id();
        $fields = get_field_objects('user_'.$user_id);
        $ids = array_map(function ($item) {
            return $item->ID;
        }, $fields['booths']['value'] ?? []);

        if (empty($ids)) {
            return ['data' => []];
        }

        $posts = get_posts([
            'include' => $ids,
            'post_status' => 'publish',
            'post_type' => 'booth',
            'posts_per_page' => -1,
        ]);

        return ['data' => array_map([$this, 'map_post'], $posts)];
    }

    public function get_booths()
    {
        $posts = get_posts([
            'post_status' => 'publish',
            'post_type' => 'booth',
            'posts_per_page' => -1,
        ]);

        return ['data' => array_map([$this, 'map_post'], $posts)];
    }

    public function get_current_user_booth_by_id(WP_REST_Request $request)
    {
        $id = $request->get_param('id');
        $post = get_post($id);

        if (! $post) {
            $response = new WP_REST_Response([
                'code' => 'booth_not_found',
                'message' => 'Booth not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        return ['data' => $this->map_post($post)];
    }

    public function get_product_position(WP_REST_Request $request)
    {
        return $this->get_post_meta($request->get_param('id'), 'vrmall_booth_product_position');
    }

    public function store_product_position(WP_REST_Request $request)
    {
        $this->sync_booth_inside_image_video_data($request->get_param('id'), $request->get_json_params());

        return $this->store_post_meta(
            $request->get_param('id'),
            'vrmall_booth_product_position',
            $request->get_json_params()
        );
    }

    public function get_decoration_position(WP_REST_Request $request)
    {
        return $this->get_post_meta($request->get_param('id'), 'vrmall_booth_decoration_position');
    }

    public function store_decoration_position(WP_REST_Request $request)
    {
        $this->sync_booth_inside_decoration_data($request->get_param('id'), $request->get_json_params());

        return $this->store_post_meta(
            $request->get_param('id'),
            'vrmall_booth_decoration_position',
            $request->get_json_params()
        );
    }

    public function get_other_decoration_position(WP_REST_Request $request)
    {
        return $this->get_post_meta($request->get_param('id'), 'vrmall_booth_other_decoration_position');
    }

    public function store_other_decoration_position(WP_REST_Request $request)
    {
        return $this->store_post_meta(
            $request->get_param('id'),
            'vrmall_booth_other_decoration_position',
            $request->get_json_params()
        );
    }

    public function store_booth_location_in_hall(WP_REST_Request $request)
    {
        $id = $request->get_param('id');
        $locations = $request->get_param('slots_on_hall');

        if (! is_array($locations)) {
            $response = new WP_REST_Response([
                'code' => 'bad_request',
                'message' => 'Request body not valid',
            ]);
            $response->set_status(400);

            return $response;
        }

        return $this->store_post_meta($id, self::SLOTS_ON_HALL, $locations);
    }

    private function map_post(WP_Post $post): array
    {
        $fields = get_field_objects($post->ID);
        $locations = get_post_meta($post->ID, self::SLOTS_ON_HALL);
        $preparing_image_id = get_field('booth_preparing_image', 'options');

        return [
            'id' => $post->ID,
            'name' => $post->post_title,
            'signboard' => $this->get_booth_signboard($fields),
            'booth_inside' => $this->get_booth_signboard_inside($fields),
            'booth_outside' => array_values(array_filter($this->get_booth_signboard_outside($fields))),
            'skybox' => $this->get_booth_skybox($fields),
            'active' => $this->get_booth_active_status($fields),
            'open_close_status' => $this->get_booth_open_close_status($fields),
            'preparing_image_url' => wp_get_attachment_image_url($preparing_image_id, null),
            'preparing_image_xl_url' => wp_get_attachment_image_url($preparing_image_id, 'booth_preparing_xl'),
            'preparing_image_l_url' => wp_get_attachment_image_url($preparing_image_id, 'booth_preparing_l'),
            'preparing_image_m_url' => wp_get_attachment_image_url($preparing_image_id, 'booth_preparing_m'),
            'size' => $this->get_booth_size($fields),
            'address' => $this->get_booth_address($fields),
            'slots_on_hall' => (is_array($locations[0]) ? $locations[0] : $locations) ?? [],
            'template' => $this->get_booth_template($fields),
        ];
    }

    private function get_booth_signboard($fields): array
    {
        $items = array_filter($fields['vrmall_booth_signboard_outside']['value'] ?? [], function ($item) {
            return $item['acf_fc_layout'] === 'vrmall_booth_signboard_outside_signboard_group';
        });

        $images = $items[0]['vrmall_booth_signboard_outside_signboard_images'] ?? [];

        return is_array($images)
            ? array_map(function ($image) {
                return [
                    'url' => wp_get_attachment_image_url($image['vrmall_booth_signboard_outside_signboard_image']['ID'], 'signboard'),
                    'display_duration' => intval($image['vrmall_booth_signboard_outside_signboard_image_display_duration']),
                ];
            }, $images)
            : [];
    }

    private function get_booth_signboard_inside($fields): array
    {
        $objects = $fields['vrmall_booth_signboard_inside']['value'] ?? [];

        return is_array($objects)
            ? array_map(function ($object) {
                switch ($object['acf_fc_layout']) {
                    case 'vrmall_booth_signboard_inside_image_group':
                        $object_type = 'image';
                        $object_id = $object['vrmall_booth_signboard_inside_image_object_id'];
                        break;
                    case 'vrmall_booth_signboard_inside_video_group':
                        $object_type = 'video';
                        $object_id = $object['vrmall_booth_signboard_inside_video_object_id'];
                        break;
                    case 'vrmall_booth_signboard_inside_decoration_group':
                        $object_type = 'decoration';
                        $object_id = $object['vrmall_booth_signboard_inside_decoration_object_id'];
                        break;
                    default:
                        $object_type = '';
                        $object_id = '';
                }

                return [
                    'type' => $object_type,
                    'object_id' => $object_id,
                    'images' => array_map(function ($image) {
                        return [
                            'signboard_size_url' => wp_get_attachment_image_url($image['vrmall_booth_signboard_inside_image']['ID'], 'signboard'),
                            'url' => $image['vrmall_booth_signboard_inside_image']['url'],
                            'display_duration' => $image['vrmall_booth_signboard_inside_image_display_duration'],
                        ];
                    }, $object['vrmall_booth_signboard_inside_images'] ?? []),
                    'videos' => array_map(function ($video) {
                        return [
                            'url' => $video['vrmall_booth_signboard_inside_video']['url'],
                        ];
                    }, $object['vrmall_booth_signboard_inside_videos'] ?? []),
                    'decorations' => array_map(function ($decoration) {
                        $decoration_fields = get_fields($decoration['vrmall_booth_signboard_inside_decoration']);

                        return [
                            '3d_url' => $decoration_fields['url_3d'] ?? '',
                            'icon' => $decoration_fields['icon'] ?? '',
                        ];
                    }, $object['vrmall_booth_signboard_inside_decorations'] ?? []),
                ];
            }, $objects)
            : [];
    }

    private function get_booth_signboard_outside($fields): array
    {
        $objects = $fields['vrmall_booth_signboard_outside']['value'] ?? [];

        return is_array($objects)
            ? array_map(function ($object) {
                switch ($object['acf_fc_layout']) {
                    case 'vrmall_booth_signboard_outside_image_group':
                        $object_type = 'image';
                        $object_id = $object['vrmall_booth_signboard_outside_image_object_id'];
                        break;
                    case 'vrmall_booth_signboard_outside_video_group':
                        $object_type = 'video';
                        $object_id = $object['vrmall_booth_signboard_outside_video_object_id'];
                        break;
                }

                if (! isset($object_type)) {
                    return null;
                }

                return [
                    'type' => $object_type,
                    'object_id' => $object_id,
                    'images' => array_map(function ($image) {
                        return [
                            'signboard_size_url' => wp_get_attachment_image_url($image['vrmall_booth_signboard_outside_image']['ID'], 'signboard'),
                            'url' => $image['vrmall_booth_signboard_outside_image']['url'],
                            'display_duration' => $image['vrmall_booth_signboard_outside_image_display_duration'],
                        ];
                    }, $object['vrmall_booth_signboard_outside_images'] ?? []),
                    'videos' => array_map(function ($video) {
                        return [
                            'url' => $video['vrmall_booth_signboard_outside_video']['url'],
                        ];
                    }, $object['vrmall_booth_signboard_outside_videos'] ?? []),
                ];
            }, $objects)
            : [];
    }

    private function get_booth_skybox($fields): ?array
    {
        $skybox = $fields['skybox'];

        if (! $skybox || ! ($skybox['value'] ?? null)) {
            return null;
        }

        $post = $skybox['value'];
        $skyboxFields = get_fields($post->ID);

        $data = [];

        if ($model = $skyboxFields['model'] ?? []) {
            $data['model'] = [
                'url' => wp_get_attachment_url($model['ID']),
            ];
        }

        if (is_array($gallery = $skyboxFields['gallery'] ?? [])) {
            $data['gallery'] = array_map(function ($item) {
                return [
                    'mime_type' => $item['mime_type'],
                    'type' => $item['type'],
                    'url' => wp_get_attachment_url($item['ID']),
                ];
            }, $gallery);
        }

        return $data;
    }

    private function get_booth_active_status($fields): bool
    {
        return (bool) $fields['active']['value'];
    }

    private function get_booth_open_close_status($fields): string
    {
        return $fields['open_close_status']['value'] ? 'opening' : 'preparing';
    }

    private function get_booth_size($fields): string
    {
        return strtolower($fields['size']['value']);
    }

    private function get_booth_address($fields): string
    {
        return strtoupper($fields['address']['value']);
    }

    private function get_booth_template($field): ?array
    {
        $id = $field['booth_setting__booth_template']['value'];

        if ($id && $template = get_post($id)) {
            $thumbnail_url = get_the_post_thumbnail_url($template, '');

            return [
                'id' => $template->ID,
                'name' => $template->post_title,
                'description' => $template->post_content,
                'thumbnail' => $thumbnail_url ? $thumbnail_url : '',
                '3d_model_url' => get_field('3d_model', $template->ID),
            ];
        }

        return null;
    }

    private function store_post_meta($post_id, $key, $data)
    {
        $post = get_post($post_id);

        if (! $post) {
            $response = new WP_REST_Response([
                'code' => 'booth_not_found',
                'message' => 'Booth not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        update_post_meta($post_id, $key, $data);

        return compact('data');
    }

    private function get_post_meta($post_id, $key)
    {
        $post = get_post($post_id);

        if (! $post) {
            $response = new WP_REST_Response([
                'code' => 'booth_not_found',
                'message' => 'Booth not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        $data = get_post_meta($post_id, $key);
        $data = $data[0] ?? $data;

        return empty($data)
            ? ['data' => (object) $data]
            : compact('data');
    }

    private function sync_booth_inside_image_video_data($booth_id, $payload)
    {
        $booth_id = intval($booth_id);

        $this->clear_all_image_or_video_rows($booth_id);

        $itemsGroupedByObject = collect($payload['imgorvideo'] ?? [])->groupBy('idOwner');

        foreach ($itemsGroupedByObject->toArray() as $items) {
            if (count($items) === 0) {
                continue;
            }

            $isImage = (int) $items[0]['typeImgOrVideo'] === 0;

            if ($isImage) {
                add_row('vrmall_booth_signboard_inside', [
                    'acf_fc_layout' => 'vrmall_booth_signboard_inside_image_group',
                    'vrmall_booth_signboard_inside_image_object_id' => $items[0]['idOwner'],
                    'vrmall_booth_signboard_inside_images' => array_map(function ($item) {
                        return [
                            'vrmall_booth_signboard_inside_image' => $item['idItem'],
                            'vrmall_booth_signboard_inside_image_display_duration' => $item['time'],
                        ];
                    }, collect($items)->sortBy('index')->toArray()),
                ], $booth_id);
            } else {
                add_row('vrmall_booth_signboard_inside', [
                    'acf_fc_layout' => 'vrmall_booth_signboard_inside_video_group',
                    'vrmall_booth_signboard_inside_video_object_id' => $items[0]['idOwner'],
                    'vrmall_booth_signboard_inside_videos' => array_map(function ($item) {
                        return [
                            'vrmall_booth_signboard_inside_video' => $item['idItem'],
                        ];
                    }, collect($items)->sortBy('index')->toArray()),
                ], $booth_id);
            }
        }
    }

    private function clear_all_image_or_video_rows($booth_id)
    {
        $layouts = ['vrmall_booth_signboard_inside_video_group', 'vrmall_booth_signboard_inside_image_group'];
        $count = $this->count_rows_of_flexible_content($booth_id);

        /**
         * When you use delete_row(), it will update the database and reindex the array of flexible field items.
         * so will count rows and loop one by one to find index of row need to delete
         */
        for ($i = 0; $i < $count; $i++) {
            $field = get_field('vrmall_booth_signboard_inside', $booth_id);
            $indexes = array_filter($field, function ($row) use ($layouts) {
                return in_array($row['acf_fc_layout'], $layouts);
            });

            $indexes = array_keys($indexes);

            if (isset($indexes[0])) {
                // acf row start at 1 not 0;
                $index = (int) $indexes[0] + 1;

                // delete row by index
                delete_row('vrmall_booth_signboard_inside', $index, $booth_id);
            }
        }
    }

    private function count_rows_of_flexible_content($booth_id)
    {
        $flex_field_array = get_post_meta($booth_id, 'vrmall_booth_signboard_inside', true);
        $count = 0;

        if (is_array($flex_field_array)) {
            $count = count($flex_field_array);
        }

        return $count;
    }

    private function sync_booth_inside_decoration_data($booth_id, $payload)
    {
        $booth_id = intval($booth_id);

        $this->clear_all_decoration_rows($booth_id);

        $itemsGroupedByObject = collect($payload['items'] ?? [])->groupBy('decoName');

        foreach ($itemsGroupedByObject->toArray() as $items) {
            if (count($items) === 0) {
                continue;
            }

            add_row('vrmall_booth_signboard_inside', [
                'acf_fc_layout' => 'vrmall_booth_signboard_inside_decoration_group',
                'vrmall_booth_signboard_inside_decoration_object_id' => $items[0]['decoName'],
                'vrmall_booth_signboard_inside_decorations' => array_map(function ($item) {
                    $metadata = collect($item)->only(['position', 'rotation', 'mulscale', 'scale'])->toJson();
                    return [
                        'vrmall_booth_signboard_inside_decoration' => $item['id'],
                        'vrmall_booth_signboard_inside_decoration_metadata' => $metadata,
                    ];
                }, collect($items)->sortBy('boardId')->toArray()),
            ], $booth_id);
        }
    }

    private function clear_all_decoration_rows($booth_id)
    {
        $layouts = ['vrmall_booth_signboard_inside_decoration_group'];
        $count = $this->count_rows_of_flexible_content($booth_id);

        /**
         * When you use delete_row(), it will update the database and reindex the array of flexible field items.
         * so will count rows and loop one by one to find index of row need to delete
         */
        for ($i = 0; $i < $count; $i++) {
            $field = get_field('vrmall_booth_signboard_inside', $booth_id);
            $indexes = array_filter($field, function ($row) use ($layouts) {
                return in_array($row['acf_fc_layout'], $layouts);
            });

            $indexes = array_keys($indexes);

            if (isset($indexes[0])) {
                // acf row start at 1 not 0;
                $index = (int) $indexes[0] + 1;

                // delete row by index
                delete_row('vrmall_booth_signboard_inside', $index, $booth_id);
            }
        }
    }
}
