<?php

if ( ! defined('WPINC')) {
    die;
}

class Product
{
    public function get_product_by_user_id($user_id)
    {
        $query = new WC_Product_Query([
            'status' => 'publish',
            'order'  => 'DESC',
            'author' => $user_id,
            'limit'  => -1,
        ]);

        $products = $this->filter_out_no_model_products($query->get_products());

        $products = array_map(function (WC_Product $product) {
            $fields = get_fields($product->get_id());

            return [
                'id'                => $product->get_id(),
                'name'              => $product->get_name(),
                'description'       => $product->get_description(),
                'short_description' => $product->get_short_description(),
                'thumbnail'         => wp_get_attachment_image_url($product->get_image_id(), ''),
                'icon_url'          => $fields['icon'] ?? '',
                'gallery_images'    => array_map(function ($id) {
                    return wp_get_attachment_image_url($id);
                }, $product->get_gallery_image_ids()),
                'settings'          => [
                    'view_mode'       => $fields['view_mode'] ?? null,
                    'view_mode_model' => [
                        'url' => $this->view_mode_model_url($fields['model'] ?? []),
                    ],
                    'view_mode_video' => [
                        'url' => $this->view_mode_video_url($fields['video'] ?? []),
                    ],
                    'view_mode_image' => [
                        'url' => $this->view_mode_image_url($fields['image'] ?? []),
                    ],
                ],
            ];
        }, $products);

        return $products;
    }


    public function get_current_user_products()
    {
        $products = self::get_product_by_user_id(get_current_user_id());

        return ['data' => $products];
    }

    public function get_current_user_products_by_booth_id(WP_REST_Request $request)
    {
        $booth_id = (int) $request->get_param('boothid');

        $post = get_posts([
            'post_type' => 'booth',
            'post__in'  => [$booth_id],
        ]);

        if (isset($post[0]) == false) {
            $response = new WP_REST_Response([
                'code'    => 'booth_not_found',
                'message' => 'Booth not found',
            ]);
            $response->set_status(404);

            return $response;
        } else {
            $args = [
                'number'      => 1,
                'count_total' => false,
                'fields'      => 'ID',
                'meta_query'  => [
                    [
                        'key'     => 'booths',
                        'value'   => '"' . $booth_id . '"',
                        'compare' => 'LIKE',
                    ]
                ],
            ];

            $users = get_users($args);

            if (isset($users[0]) == false) {
                $response = new WP_REST_Response([
                    'code'    => 'owner_booth_not_found',
                    'message' => 'Owner booth not found',
                ]);
                $response->set_status(404);

                return $response;
            } else {
                $products = self::get_product_by_user_id(get_current_user_id());

                if ($users[0] != get_current_user_id() && current_user_can('administrator')) {
                    $products_booth_owner = self::get_product_by_user_id($users[0]);
                    $products             = array_merge($products, $products_booth_owner);
                }

                return ['data' => $products];
            }
        }
    }


    protected function view_mode_model_url($model)
    {
        if ( ! $model || ! array_key_exists('ID', $model)) {
            return null;
        }

        return wp_get_attachment_url($model['ID']);
    }

    protected function view_mode_video_url($video)
    {
        if ( ! $video || ! array_key_exists('ID', $video)) {
            return null;
        }

        return wp_get_attachment_url($video['ID']);
    }

    protected function view_mode_image_url($image)
    {
        if ( ! $image || ! array_key_exists('ID', $image)) {
            return null;
        }

        return wp_get_attachment_url($image['ID']);
    }

    protected function filter_out_no_model_products($products)
    {
        return array_values(
            array_filter($products, function (WC_Product $product) {
                $fields = get_fields($product->get_id());

                return null !== $this->view_mode_model_url($fields['model'] ?? [])
                       || null !== $this->view_mode_video_url($fields['video'] ?? [])
                       || null !== $this->view_mode_image_url($fields['image'] ?? []);
            })
        );
    }
}
