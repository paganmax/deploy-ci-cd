<?php
if (! defined('WPINC')) {
    die;
}

class OtherObject
{
    private $settings;
    private $objects;
    private $idPrefix;

    public function get_objects(): array
    {
        $this->init();
        $objects = array_map([$this, 'transform_object'], $this->objects);

        return ['data' => $objects];
    }

    public function get_object_by_id(WP_REST_Request $request)
    {
        $this->init();

        $id = $request->get_param('id');

        $objects = collect($this->objects)->first(function ($item) use ($id) {
            return $id === $this->idPrefix.$item['id'];
        });

        if (! $objects) {
            $response = new WP_REST_Response([
                'code' => 'object_not_found',
                'message' => 'Object not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        return ['data' => $this->transform_object($objects)];
    }

    private function init(): void
    {
        $this->settings = collect(get_field_object('objects', 'options'));
        $this->objects = collect(get_field_object('objects', 'options'))->get('value');
        $this->parse_id_prefix();
    }

    private function parse_id_prefix(): void
    {
        $fields = collect(collect($this->settings)->get('sub_fields'));
        $idField = $fields->firstWhere('name', 'id');

        $this->idPrefix = $idField['prepend'];
    }

    private function transform_object($objects): array
    {
        $objects = $objects['objects'];

        return [
            'id' => $this->idPrefix.$objects['id'],
            'name' => $objects['name'],
            'objects' => $objects ? array_map([$this, 'transform_detail_object'], $objects) : [],
        ];
    }

    private function transform_detail_object($object): array
    {
        $object_type = $object['acf_fc_layout'];

        switch ($object_type) {
            case 'movie':
                return $this->transform_movie_object($object);

            case 'image':
                return $this->transform_image_object($object);
        }

        return [];
    }

    private function transform_movie_object($object): array
    {
        return [
            'object_type' => $object['acf_fc_layout'],
            'name' => $object['name'],
            'items' => array_map(function ($item) {
                return [
                    'url' => $item['videos'],
                ];
            }, $object['playlist']),
            'play_mode' => $object['play_mode'],
        ];
    }

    private function transform_image_object($object): array
    {
        return [
            'object_type' => $object['acf_fc_layout'],
            'name' => $object['name'],
            'items' => array_map(function ($item) {
                return [
                    'url' => $item['source'],
                    'display_time' => $item['display_time'],
                    'target_url' => $item['target_url'],
                ];
            }, $object['gallery']),
        ];
    }
}
