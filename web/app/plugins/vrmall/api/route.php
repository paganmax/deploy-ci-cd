<?php

if (! defined('WPINC')) {
    die;
}

require_once 'scene-setting.php';
require_once 'general-setting.php';
require_once 'booth.php';
require_once 'product.php';
require_once 'decoration.php';
require_once 'hall-setting.php';
require_once 'hall.php';
require_once 'google-cloud.php';
require_once 'user.php';

add_action('rest_api_init', function () {
    register_rest_route('vrmall', 'settings/general', [
        'methods' => 'GET',
        'callback' => 'getGeneralSettings',
    ]);

    register_rest_route('vrmall', 'settings/scenes', [
        'methods' => 'GET',
        'callback' => [new SceneSetting, 'getScenes'],
    ]);

    register_rest_route('vrmall', 'settings/scenes/(?P<id>[a-z0-9-]+)', [
        'methods' => 'GET',
        'callback' => [new SceneSetting, 'getScene'],
    ]);

    register_rest_route('vrmall', 'booths', [
        'methods' => 'GET',
        'callback' => [new Booth(), 'get_booths'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'me/booths', [
        'methods' => 'GET',
        'callback' => [new Booth(), 'get_current_user_booths'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)', [
        'methods' => 'GET',
        'callback' => [new Booth(), 'get_current_user_booth_by_id'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)/product-position', [
        'methods' => 'GET',
        'callback' => [new Booth(), 'get_product_position'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)/product-position', [
        'methods' => 'POST',
        'callback' => [new Booth(), 'store_product_position'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)/decoration-position', [
        'methods' => 'GET',
        'callback' => [new Booth(), 'get_decoration_position'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)/decoration-position', [
        'methods' => 'POST',
        'callback' => [new Booth(), 'store_decoration_position'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)/other-decoration-position', [
        'methods' => 'GET',
        'callback' => [new Booth(), 'get_other_decoration_position'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)/other-decoration-position', [
        'methods' => 'POST',
        'callback' => [new Booth(), 'store_other_decoration_position'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'booths/(?P<id>[a-z0-9-]+)/location', [
        'methods' => 'POST',
        'callback' => [new Booth(), 'store_booth_location_in_hall'],
        'permission_callback' => function () {
            return is_user_logged_in() && current_user_can('manage_options');
        },
    ]);

    register_rest_route('vrmall', 'me/products', [
        'methods' => 'GET',
        'callback' => [new Product(), 'get_current_user_products_by_booth_id'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'decorations', [
        'methods' => 'GET',
        'callback' => [new Decoration(), 'get_decorations'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'decorations/(?P<id>[a-z0-9-]+)', [
        'methods' => 'GET',
        'callback' => [new Decoration(), 'get_decoration_by_id'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'other-decorations', [
        'methods' => 'GET',
        'callback' => [new Decoration(), 'get_other_decorations'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'settings/halls', [
        'methods'             => 'GET',
        'callback'            => [new HallSettings(), 'get_halls'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);
    register_rest_route('vrmall', 'settings/halls/(?P<id>[a-z0-9-]+)', [
        'methods'             => 'GET',
        'callback'            => [new HallSettings(), 'get_hall_by_id'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);
    register_rest_route('vrmall', 'settings/halls/(?P<id>[a-z0-9-]+)/floors', [
        'methods'             => 'GET',
        'callback'            => [new HallSettings(), 'get_floor_by_hall_id'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'halls', [
        'methods' => 'GET',
        'callback' => [new Hall(), 'get_halls'],
        'permission_callback' => function () {
            return is_user_logged_in() && current_user_can('manage_options');
        },
    ]);

    register_rest_route('vrmall', 'halls', [
        'methods' => 'POST',
        'callback' => [new Hall(), 'store_hall_settings'],
        'permission_callback' => function () {
            return is_user_logged_in() && current_user_can('manage_options');
        },
    ]);

    register_rest_route('vrmall', 'medias', [
        'methods'             => 'GET',
        'callback'            => [new Googleclouds(), 'get_medias'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'register', [
        'methods' => 'POST',
        'callback' => [new User(), 'register'],
    ]);

    register_rest_route('vrmall', 'media', [
        'methods' => 'POST',
        'callback' => [new Googleclouds(), 'handle_upload'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);

    register_rest_route('vrmall', 'me', [
        'methods' => 'GET',
        'callback' => [new User(), 'me'],
        'permission_callback' => function () {
            return is_user_logged_in();
        },
    ]);
});

add_filter('rest_authentication_errors', function ($errors) {
    global $wp;

    if (empty($wp->query_vars['rest_route'])) {
        return $errors;
    }

    $route = ltrim($wp->query_vars['rest_route'], '/');

    if (0 !== strpos($route, 'vrmall')) {
        return $errors;
    }

    $_SERVER['HTTP_X_WP_NONCE'] = wp_create_nonce('wp_rest');

    return $errors;
}, 10);
