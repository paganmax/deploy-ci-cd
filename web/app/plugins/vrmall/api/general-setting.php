<?php

if (! defined('WPINC')) {
    die;
}

function getGeneralSettings(): array
{
    $availableSettings = [
        'walk_speed',
        'turn_speed',
        'viewpoint_height',
        'autoplay_distance',
    ];

    $settings = collect(get_fields('option'))->only($availableSettings);

    return ['data' => $settings];
}
