<?php

if ( ! defined('WPINC')) {
    die;
}

class HallSettings
{
    private $settings;
    private $halls;
    private $idPrefix;

    public function get_halls(): array
    {
        $this->init();
        $halls = array_map([$this, 'transform_hall'], $this->halls);

        return ['data' => $halls];
    }

    public function get_hall_by_id(WP_REST_Request $request)
    {
        $this->init();

        $id = $request->get_param('id');

        $hall = collect($this->halls)->first(function ($item) use ($id) {
            return $id === $this->idPrefix.$item['id'];
        });

        if ( ! $hall) {
            $response = new WP_REST_Response([
                'code'    => 'hall_not_found',
                'message' => 'Hall not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        return ['data' => $this->transform_hall($hall)];
    }

    public function get_floor_by_hall_id(WP_REST_Request $request)
    {
        $this->init();

        $id = $request->get_param('id');

        $hall = collect($this->halls)->first(function ($item) use ($id) {
            return $id === $this->idPrefix.$item['id'];
        });

        if ( ! $hall) {
            $response = new WP_REST_Response([
                'code'    => 'hall_not_found',
                'message' => 'Hall not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        return ['data' => $hall["floor"] ? array_map([$this, 'transform_hall_floor'], $hall["floor"]) : []];
    }

    private function init(): void
    {
        $this->settings = collect(get_field_object('halls', 'options'));
        $this->halls    = collect(get_field_object('halls', 'options'))->get('value');
        $this->parse_id_prefix();
    }

    private function parse_id_prefix(): void
    {
        $fields  = collect(collect($this->settings)->get('sub_fields'));
        $idField = $fields->firstWhere('name', 'id');

        $this->idPrefix = $idField['prepend'];
    }

    private function transform_hall($halls): array
    {
        $floor = $halls['floor'];

        return [
            'id'      => $this->idPrefix.$halls['id'],
            'name'    => $halls['name'],
            'display' => !!$halls['display'],
            'floor'   => $floor ? array_map([$this, 'transform_hall_floor'], $floor) : [],
        ];
    }

    private function transform_hall_floor($floor): array
    {
        return [
            'id'      => $floor['id'],
            'name'    => $floor['name'],
            'display' => !!$floor['display']
        ];
    }
}
