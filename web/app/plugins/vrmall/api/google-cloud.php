<?php

if ( ! defined('WPINC')) {
    die;
}

class Googleclouds
{
    private function get_type($mime_type)
    {
        if ($mime_type == "image/jpeg" || $mime_type == "image/jpg" || $mime_type == "image/png") {
            return "image";
        } elseif ($mime_type == "video/mp4") {
            return "video";
        } elseif ($mime_type == "application/pdf") {
            return "document";
        } elseif ($mime_type == "application/octet-stream" || $mime_type == "application/json"
                  || $mime_type == "text/plain"
                  || $mime_type == "application/zip") {
            return "model";
        } else {
            return "";
        }
    }

    public function get_medias(WP_REST_Request $request)
    {

        $type = $request->get_param('type');

        if (($type != "image" && $type != "video" && $type != "model"
             && $type != "document" && $type != "")) {
            return new WP_Error(
                'bad_request',
                __('Invalid type'),
                array('status' => 400)
            );
        }

        $list_media_id = $this->get_list_id_attachment($type);

        return ['data' => array_map([$this, 'map_post'], $list_media_id)];
    }

    private function map_post($media): array
    {
        return [
            'id'        => $media,
            'type'      => self::get_type(get_post_mime_type($media)),
            'file_name' => basename(get_attached_file($media)),
            'link'      => wp_get_attachment_url($media),
            'icon' => $this->get_icon($media),
        ];
    }

    public function get_list_id_attachment($type)
    {
        $list_mime_type = [
            "image/jpeg", "image/jpg", "image/png", "video/mp4", "application/pdf", "application/zip",
            "application/x-rar-compressed", "application/octet-stream", "application/x-zip-compressed",
            "multipart/x-zip", "application/x-rar", "application/pdf",
            "application/json", "application/x-rar", "text/plain"
        ];

        $list_mime_image    = ["image/jpeg", "image/jpg", "image/png"];
        $list_mime_video    = ["video/mp4"];
        $list_mime_document = ["application/pdf"];
        $list_mime_model    = [
            "application/octet-stream", "application/json"
            , "text/plain"
            , "application/zip"
        ];

        $list_id_attachment = [];
        $args = [
            'post_type' => 'attachment',
            'numberposts' => -1,
            'post_status' => null,
            'post_parent' => null,
            'meta_query' => [
                [
                    'key' => 'vrmall_video_thumbnail',
                    'compare' => 'NOT EXISTS',
                ],
            ],
        ];
        switch ($type) {
            case "":
                $args['post_mime_type'] = implode(',', $list_mime_type);
                break;
            case "image":
                $args['post_mime_type'] = implode(',', $list_mime_image);
                break;
            case "video":
                $args['post_mime_type'] = implode(',', $list_mime_video);
                break;
            case "document":
                $args['post_mime_type'] = implode(',', $list_mime_document);
                break;
            case "model":
                $args['post_mime_type'] = implode(',', $list_mime_model);
                break;
        }

        if ( ! current_user_can('manage_options')) {
            $args['author'] = get_current_user_id();
        }

        $attachments = get_posts($args);
        if ($attachments) {
            foreach ($attachments as $post) {
                array_push($list_id_attachment, $post->ID);
            }
        }

        return $list_id_attachment;
    }

    private function get_icon($media)
    {
        $id = get_post_thumbnail_id($media);

        return $id ? wp_get_attachment_image_url($id) : null;
    }

    public function handle_upload(WP_REST_Request $request)
    {
        $files = $request->get_file_params();

        if (isset($files['file'])) {
            require_once ABSPATH.'wp-admin/includes/file.php';
            require_once ABSPATH.'wp-admin/includes/media.php';
            require_once ABSPATH.'wp-admin/includes/image.php';

            if ($id = media_handle_sideload($files['file'])) {
                return ['data' => $this->map_post($id)];
            }
        }
    }
}
