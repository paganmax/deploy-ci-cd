<?php

if (! defined('WPINC')) {
    die;
}

class Decoration
{
    public function get_decorations()
    {
        $posts = get_posts([
            'post_status' => 'publish',
            'post_type' => 'decoration',
            'posts_per_page' => -1,
        ]);

        return ['data' => array_map([$this, 'map_post'], $posts)];
    }

    public function get_decoration_by_id(WP_REST_Request $request)
    {
        $id = $request->get_param('id');
        $post = get_post($id);

        if (! $post || get_post_type($post) != 'decoration') {
            $response = new WP_REST_Response([
                'code' => 'decoration_not_found',
                'message' => 'Decoration not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        return ['data' => $this->map_post($post)];
    }

    public function get_other_decorations()
    {
        $fields = get_fields('option');
        $objects = $fields['shop_objects'] ?? [];

        return [
            'data' => array_map(function ($object) {
                return [
                    'type' => $object['acf_fc_layout'],
                    'id' => $object['id'],
                    'name' => $object['name'],
                    'items' => strtolower($object['acf_fc_layout']) === 'movie'
                        ? array_map([$this, 'map_object_movie'], $object['playlist'])
                        : array_map([$this, 'map_object_image'], $object['gallery']),
                    'play_mode' => $object['play_mode'] ?? null,
                ];
            }, $objects),
        ];
    }

    private function map_post(WP_Post $post): array
    {
        $fields = get_field_objects($post->ID);

        return [
            'id' => $post->ID,
            'name' => $post->post_title,
            'description' => $post->post_content,
            'thumbnail' => get_the_post_thumbnail_url($post, ''),
            'icon_url' => $fields['icon']['value'] ?? '',
            '3d_url' => $this->get_3d_url($fields),
        ];
    }

    private function get_3d_url($fields): string
    {
        return $fields['url_3d']['value'] ?? '';
    }

    private function map_object_movie($item)
    {
        return [
            'url' => $item['videos'],
        ];
    }

    private function map_object_image($item)
    {
        return [
            'url' => $item['source'],
            'display_time' => $item['display_time'],
            'target_url' => $item['target_url'],
        ];
    }
}
