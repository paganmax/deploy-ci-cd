<?php

if (! defined('WPINC')) {
    die;
}

class User
{
    public function register(WP_REST_Request $request)
    {
        $email = $request->get_param('email');

        if (! $email || ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response = new WP_REST_Response([
                'code' => 'request_body_invalid',
                'message' => 'Request body invalid',
                'errors' => [
                    'email' => ['メールは、有効なメールアドレス形式で指定してください。'],
                ],
            ]);
            $response->set_status(422);

            return $response;
        }

        if (email_exists($email) || username_exists($email)) {
            $response = new WP_REST_Response([
                'code' => 'request_body_invalid',
                'message' => 'Request body invalid',
                'errors' => [
                    'email' => ['指定のメールは既に使用されています。'],
                ],
            ]);
            $response->set_status(422);

            return $response;
        }

        $data = [
            'user_email' => $email,
            'user_login' => $email,
            'role' => 'customer',
        ];

        $result = wp_insert_user($data);

        if ($result instanceof WP_Error) {
            $response = new WP_REST_Response([
                'code' => 'request_body_invalid',
                'message' => 'Request body invalid',
                'errors' => $result->errors,
            ]);
            $response->set_status(422);

            return $response;
        }

        wp_new_user_notification($result, null, 'user');

        return ['status' => 'success'];
    }

    public function me(WP_REST_Request $request)
    {
        $user = wp_get_current_user();

        return ['data' => [
            'id' => $user->ID,
            'email' => $user->user_email,
            'company' => get_user_meta($user->ID, 'billing_company', true),
            'address' => get_user_meta($user->ID, 'billing_address_1', true),
            'department' => get_user_meta($user->ID, 'department', true),
            'name' => $user->first_name,
            'phone' => get_user_meta($user->ID, 'billing_phone', true),
            'roles' => $user->roles ?? [],
        ]];
    }
}
