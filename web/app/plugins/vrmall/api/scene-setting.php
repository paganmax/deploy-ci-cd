<?php

if (! defined('WPINC')) {
    die;
}

class SceneSetting
{
    private $settings;
    private $scenes;
    private $idPrefix;

    public function getScenes(): array
    {
        $this->init();
        $scenes = array_map([$this, 'transformScene'], $this->scenes);

        return ['data' => $scenes];
    }

    public function getScene(WP_REST_Request $request)
    {
        $this->init();

        $id = $request->get_param('id');

        $scene = collect($this->scenes)->first(function ($item) use ($id) {
            return $id === $this->idPrefix.$item['id'];
        });

        if (! $scene) {
            $response = new WP_REST_Response([
                'code' => 'scene_not_found',
                'message' => 'Scene not found',
            ]);
            $response->set_status(404);

            return $response;
        }

        return ['data' => $this->transformScene($scene)];
    }

    private function init(): void
    {
        $this->settings = collect(get_field_object('scenes', 'options'));
        $this->scenes = collect(get_field_object('scenes', 'options'))->get('value');
        $this->parseIdPrefix();
    }

    private function parseIdPrefix(): void
    {
        $fields = collect(collect($this->settings)->get('sub_fields'));
        $idField = $fields->firstWhere('name', 'id');

        $this->idPrefix = $idField['prepend'];
    }

    private function transformScene($scene): array
    {
        $objects = $scene['objects'];

        return [
            'id' => $this->idPrefix.$scene['id'],
            'name' => $scene['name'],
            'objects' => $objects ? array_map([$this, 'transformSceneObject'], $objects) : [],
        ];
    }

    private function transformSceneObject($object): array
    {
        $objectType = $object['acf_fc_layout'];

        switch ($objectType) {
            case 'movie':
                return $this->transformSceneMovieObject($object);

            case 'event_point':
                return $this->transformSceneEventPointObject($object);

            case 'image':
                return $this->transformSceneImageObject($object);
        }

        return [];
    }

    private function transformSceneMovieObject($object): array
    {
        return [
            'object_type' => $object['acf_fc_layout'],
            'id' => $object['id'],
            'items' => array_map(function ($item) {
                return [
                    'url' => $item['videos'],
                ];
            }, $object['playlist']),
            'play_mode' => $object['play_mode'],
        ];
    }

    private function transformSceneEventPointObject($object): array
    {
        return [
            'object_type' => $object['acf_fc_layout'],
            'id' => $object['id'],
            'event_type' => $object['type'],
            'event_action' => $object['event'],
            'target_id' => $object['target_id'],
            'target_url' => $object['target_url'],
        ];
    }

    private function transformSceneImageObject($object): array
    {
        return [
            'object_type' => $object['acf_fc_layout'],
            'id' => $object['id'],
            'name' => $object['name'],
            'items' => array_map(function ($item) {
                return [
                    'url' => $item['source'],
                    'display_time' => $item['display_time'],
                    'target_url' => $item['target_url'],
                ];
            }, $object['gallery']),
        ];
    }
}
