<?php
if ( ! defined('WPINC')) {
    die;
}


add_filter('wpcf7_validate', 'email_shop_already_in_db', 10, 2);

function email_shop_already_in_db($result, $tags)
{
    $form = WPCF7_Submission::get_instance();

    $contact_form    = WPCF7_ContactForm::get_current();
    $contact_form_id = $contact_form->id();
    if ($contact_form_id == FORM_SIGN_UP_SHOP_STEP_1) {
        $email = $form->get_posted_data('your-email-sign-shop');
        $user  = get_user_by("email", $email);
        if (array_intersect(["shop_manager"], $user->roles)) {
            $result->invalidate('your-email-sign-shop', EMAIL_SHOP_EXISTS);
        };

        $args = [
            'meta_key'    => 'email',
            'meta_value'  => $email,
            'post_type'   => 'signupshop',
            'post_status' => 'publish',
        ];

        $posts = get_posts($args);
        if ( ! empty($posts)) {
            $result->invalidate('your-email-sign-shop', EMAIL_SUB_SHOP_EXISTS);
        }
    }

    return $result;
}


add_action('wp_footer', 'redirect_page_shop');

function redirect_page_shop()
{ ?>
    ​
    <script type="text/javascript">
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        jQuery(document).ready(function () {
            document.addEventListener('wpcf7invalid', function (e) {
                if (<?php echo FORM_SIGN_UP_SHOP_STEP_1 ?> == e.detail.contactFormId
            )
                {
                    var respone = e.detail.apiResponse.invalid_fields;
                    let message_shop = '<?php echo EMAIL_SHOP_EXISTS ?>';
                    let message_sub_shop = '<?php echo EMAIL_SUB_SHOP_EXISTS ?>';
                    for (element of respone) {
                        if (element.message.includes(message_shop)) {
                            jQuery('.wpcf7-response-output').hide(0);
                        }
                        if (element.message.includes(message_sub_shop)) {
                            jQuery('.wpcf7-response-output').hide(0);
                        }
                    }
                    jQuery('.wpcf7-response-output').show(1);
                }
            }, false);
        });
    </script>
    <?php
}

add_filter('manage_signupshop_posts_columns', 'set_custom_edit_signupshop_columns');
function set_custom_edit_signupshop_columns($columns)
{
    $offset = array_search('title', array_keys($columns));
    unset($columns['date']);
    unset($columns['title']);

    return array_merge(array_slice($columns, 0, $offset),
        ['email' => __('メールアドレス', 'text_domain')],
        ['company' => __('会社名', 'text_domain')],
        ['address' => __('会社住所', 'text_domain')],
        ['department' => __('所属部署/役職', 'text_domain')],
        ['name' => __('お名前', 'text_domain')],
        ['phone' => __('電話番号', 'text_domain')],
        ['name_store' => __('店舗名', 'text_domain')],
        ['active' => __('基本情報入力状態', 'text_domain')],
        ['submit_time' => __('登録日時', 'text_domain')],
        ['create_account' => __('登録状態', 'text_domain')]
        , array_slice($columns, $offset, null));
}

add_action('manage_signupshop_posts_custom_column', 'custom_signupshop_column', 10, 2);
function custom_signupshop_column($column, $post_id)
{
    switch ($column) {
        case 'email' :
            echo get_post_meta($post_id, 'email', true);
            break;
        case 'company' :
            echo get_post_meta($post_id, 'company', true);
            break;
        case 'address' :
            echo get_post_meta($post_id, 'address', true);
            break;
        case 'department' :
            echo get_post_meta($post_id, 'department', true);
            break;
        case 'name' :
            echo get_post_meta($post_id, 'name', true);
            break;
        case 'phone' :
            echo get_post_meta($post_id, 'phone', true);
            break;
        case 'name_store' :
            echo get_post_meta($post_id, 'name_store', true);
            break;
        case 'active' :
            if (get_post_meta($post_id, 'active', true) == 1) {
                echo "入力済";
            } else {
                echo "未入力";
            }
            break;
        case 'submit_time' :
            echo get_the_time('Y/m/d', $post_id)." at ".get_the_time('g:i a', $post_id);
            break;
        case 'create_account' :
            $active = get_post_meta($post_id, 'active', true);
            if ($active == 1) {
                $user    = get_user_by("email", get_post_meta($post_id, 'email', true));
                $user_id = $user->ID;
                if (empty($user)) {
                    $text_button = "ユーザー追加";
                } else {
                    $text_button = "出展者昇格";
                }
                printf('<button type="button"  class="button button-primary button_update" id="update_user" name="update_user" onclick="update_profile(this, '.$post_id.', '.$user_id.')">
                                              <div class="text_update">'.$text_button.'</div>
                                        <span class="spinner">
                                        </span>
                                       </button>
                                       <div id="my-dialog" class="hidden" style="max-width:400px" >
                                            <p>出展者のアカウント作成が完了しました</p>
                                        </div>
                                       ');
            } else {
                echo "基本情報未入力のため、操作できません";
            }
            break;
    }
}


function save_email_to_send_shop_token_registration($contact_form)
{
    $submission = WPCF7_Submission::get_instance();

    $posted_data = $submission->get_posted_data();

    $contact_form    = WPCF7_ContactForm::get_current();
    $contact_form_id = $contact_form->id();
    if ($contact_form_id == FORM_SIGN_UP_SHOP_STEP_1) {
        if (empty($invalid_fields)) {
            $user = get_user_by("email", $posted_data["your-email-sign-shop"]);

            $posts = get_posts([
                'post_type'  => 'signupshop',
                'post_title' => $posted_data["your-email-sign-shop"],
            ]);

            $email_sub_shop = $posts[0]->post_title;

            if ( ! array_intersect(["shop_manager"], $user->roles)) {
                $token = bin2hex(openssl_random_pseudo_bytes(16));

                $my_query = array(
                    'post_title'  => $posted_data["your-email-sign-shop"],
                    'post_type'   => 'signupshop',
                    'post_status' => 'publish',
                    'post_author' => 1,
                );
                $post_id  = wp_insert_post($my_query);

                add_post_meta($post_id, "email", $posted_data["your-email-sign-shop"]);
                add_post_meta($post_id, "user_sub_token", $token);
                add_post_meta($post_id, "active", 0);
                add_post_meta($post_id, "display_active", 1);

                //Send email
                $title   = get_field('title_email_active_sub_shop', 'options');
                $message = get_field('content_email_active_sub_shop', 'options');
                $headers = ['Content-Type: text/html; charset=UTF-8'];

                $link_token = get_home_url().'/shopmanager/?ut='.$token;
                $replaces   = [
                    '[get_link_token]' => sprintf("<a href ='".$link_token."'>$link_token</a>"),
                ];

                $message                 = strtr($message, $replaces);
                $contact_form->skip_mail = true;
                wp_mail($posted_data["your-email-sign-shop"], $title, $message, $headers);
            }
        }
    }

}

add_action('wpcf7_before_send_mail', 'save_email_to_send_shop_token_registration', 10, 1);


function create_new_user_shop_registration($contact_form)
{
    $submission = WPCF7_Submission::get_instance();

    $posted_data     = $submission->get_posted_data();
    $contact_form    = WPCF7_ContactForm::get_current();
    $contact_form_id = $contact_form->id;


    if ($contact_form_id == FORM_SIGN_UP_SHOP_STEP_2) {
        if (empty($invalid_fields)) {
            $args = [
                'meta_key'    => 'user_sub_token',
                'meta_value'  => $posted_data['your-token-sign-shop'],
                'post_type'   => 'signupshop',
                'post_status' => 'publish',
            ];

            $posts   = get_posts($args);
            $post_id = $posts[0]->ID;

            if ( ! empty($post_id)) {
                $name       = $posted_data["your-name-sign-shop"];
                $email      = $posts[0]->post_title;
                $company    = $posted_data["your-company-sign-shop"];
                $address    = $posted_data["your-address-sign-shop"];
                $phone      = $posted_data["your-phone-sign-shop"];
                $name_store = $posted_data["your-name-store-sign-shop"];
                $department = $posted_data["your-department-sign-shop"];

                add_post_meta($post_id, "company", $company);
                add_post_meta($post_id, "address", $address);
                add_post_meta($post_id, "department", $department);
                add_post_meta($post_id, "name", $name);
                add_post_meta($post_id, "phone", $phone);
                add_post_meta($post_id, "name_store", $name_store);

                update_post_meta($post_id, "active", 1);
                update_post_meta($post_id, "user_sub_token", "");

                $contact_form->skip_mail = true;
                $headers                 = ['Content-Type: text/html; charset=UTF-8'];

                $replaces = [
                    '[name_shop_manager]'            => $name,
                    '[email_shop_manager]'           => $email,
                    '[company_name_shop_manager]'    => $company,
                    '[address_shop_manager]'         => $address,
                    '[phone_shop_manager]'           => $phone,
                    '[store_name_shop_manager]'      => $name_store,
                    '[department_name_shop_manager]' => $department,
                ];

                //mail to sub shop
                $title   = get_field('title_email_success_sub_shop', 'options');
                $message = get_field('content_email_success_sub_shop', 'options');
                $message = strtr($message, $replaces);
                wp_mail($posts[0]->post_title, $title, $message, $headers);

                //mail to admin
                $title_to_admin   = get_field('title_email_sign_up_shop_to_admin', 'options');
                $message_to_admin = get_field('content_email_sign_up_shop_to_admin', 'options');
                $message_to_admin = strtr($message_to_admin, $replaces);
                wp_mail(get_option('admin_email'), $title_to_admin, $message_to_admin, $headers);

            }

        }
    }
}


add_action('wpcf7_before_send_mail', 'create_new_user_shop_registration', 10, 1);


function user_profile_update($user_id)
{
    if ( ! empty($_POST['name_store'])) {
        update_user_meta($user_id, "name_store", $_POST['name_store']);
    }
    if ( ! empty($_POST['department'])) {
        update_user_meta($user_id, "department", $_POST['department']);
    }
    if ( ! empty($_POST['know_sign_sub'])) {
        update_user_meta($user_id, "know_sign_sub", $_POST['know_sign_sub']);
    }
    if ( ! empty($_POST['reason_sign_sub'])) {
        update_user_meta($user_id, "reason_sign_sub", $_POST['reason_sign_sub']);
    }
}

add_action('profile_update', 'user_profile_update', 10, 2);

function add_admin_sign_up_submenu()
{
    remove_submenu_page('edit.php?post_type=signupshop', 'post-new.php?post_type=signupshop');
}

add_action('admin_menu', 'add_admin_sign_up_submenu');


function add_js_to_wp_footer()
{ ?>
    <style type="text/css">
        .ui-dialog-titlebar-close {
            visibility: hidden;
        }

        .spinner {
            display: none;
            float: inherit;
            margin: 4px 10px 0px;
        }

        .button_update {
            width: 100%;
            padding: 6px !important;
        }

        #create_account {
            width: 7%;
        }

        .text_update {
            white-space: initial;
            line-height: 16px;
        }
    </style>
    <script type="text/javascript">
        function update_profile(o, pId, uId) {
            let post_id = pId;
            let user_id = uId;
            jQuery(o).prop('disabled', true);
            jQuery(o).find('.spinner').addClass('is-active');
            let modal = jQuery("#my-dialog").dialog({
                autoOpen: false,
                modal: true,
                title: '通知',
                dialogClass: 'no-close',
                draggable: false,
                resizable: false,
                closeOnEscape: false,
                open: function (event, ui) {
                    jQuery(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                },
                position: {
                    my: "center",
                    at: "center",
                    of: window
                },
                'buttons': {
                    "OK": function () {
                        jQuery(this).dialog('close');
                        location.reload();
                    }
                }
            });
            jQuery.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    "action": "update_user_profile",
                    "post_id": post_id,
                    "user_id": user_id
                },
                success: function (data) {
                    modal.dialog('open');
                }
            });
            return false;
        }
    </script>
<?php }

add_action('wp_footer', 'add_js_to_wp_footer');
add_action('admin_footer', 'add_js_to_wp_footer');

function update_user_profile()
{
    $post_id    = $_POST['post_id'];
    $user_id    = $_POST['user_id'];
    $name       = get_post_meta($post_id, 'name', true);
    $company    = get_post_meta($post_id, 'company', true);
    $email      = get_post_meta($post_id, 'email', true);
    $address    = get_post_meta($post_id, 'address', true);
    $phone      = get_post_meta($post_id, 'phone', true);
    $department = get_post_meta($post_id, 'department', true);
    $name_store = get_post_meta($post_id, 'name_store', true);
    $role       = "shop_manager";
    $active     = 1;

    $replaces = [
        '[name_shop_manager]'            => $name,
        '[email_shop_manager]'           => $email,
        '[company_name_shop_manager]'    => $company,
        '[address_shop_manager]'         => $address,
        '[phone_shop_manager]'           => $phone,
        '[store_name_shop_manager]'      => $name_store,
        '[department_name_shop_manager]' => $department,
    ];


    if ( ! empty($user_id)) {
        update_user_meta($user_id, "first_name", $name);
        update_user_meta($user_id, "billing_first_name", $name);
        update_user_meta($user_id, "billing_email", $email);
        update_user_meta($user_id, "billing_company", $company);
        update_user_meta($user_id, "billing_address_1", $address);
        update_user_meta($user_id, "billing_phone", $phone);
        update_user_meta($user_id, "role", $role);

        update_user_meta($user_id, "department", $department);
        update_user_meta($user_id, "name_store", $name_store);
        update_user_meta($user_id, "active", $active);


        wp_update_user(
            array(
                'ID'           => $user_id,
                'display_name' => $name,
            )
        );

        $user_info = get_userdata($user_id);
        $user_info->set_role('shop_manager');
        $to = $user_info->user_email;

        //Send email update sub to shop
        $title   = get_field('title_update_subscriber_to_shop_manager', 'options');
        $message = get_field('content_update_subscriber_to_shop_manager', 'options');
        $headers = ['Content-Type: text/html; charset=UTF-8'];

        $message = strtr($message, $replaces);
        wp_mail($to, $title, $message, $headers);
        wp_delete_post($post_id);
    } else {
        $random_value   = rand(1, 999999);
        $create_user_id = str_pad($random_value, 5, '0', STR_PAD_LEFT);

        $user_query = array(
            'user_login' => $create_user_id,
            'user_email' => $email,
            'role'       => $role,
        );
        $user_id    = wp_insert_user($user_query);

        update_user_meta($user_id, "first_name", $name);
        update_user_meta($user_id, "billing_first_name", $name);
        update_user_meta($user_id, "billing_email", $email);
        update_user_meta($user_id, "billing_company", $company);
        update_user_meta($user_id, "billing_address_1", $address);
        update_user_meta($user_id, "billing_phone", $phone);

        update_user_meta($user_id, "name_store", $name_store);
        update_user_meta($user_id, "department", $department);
        update_user_meta($user_id, "active", $active);

        wp_update_user(
            array(
                'ID'           => $user_id,
                'display_name' => $name,
            )
        );

        $user_data = get_userdata($user_id);

        $key = get_password_reset_key($user_data);

        //Send email reset password
        $title   = get_field('title_email_active_shop_manager', 'options');
        $message = get_field('content_email_active_shop_manager', 'options');
        $headers = ['Content-Type: text/html; charset=UTF-8'];

        $link_reset_passsword = network_site_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user_data->user_login),
            'login');

        $link_term = get_home_url().'/terms/';

        $replaces = array_merge($replaces, [
            '[link_reset_passsword_shop_manager]' => sprintf("<a href ='".$link_reset_passsword."'> $link_reset_passsword </a>"),
            '[link_terms_shop_manager]'           => sprintf("<a href ='".$link_term."'> 利用規約へリンク設定 </a>"),
        ]);

        $message = strtr($message, $replaces);
        wp_mail($user_data->user_email, $title, $message, $headers);
        wp_delete_post($post_id);
    }
    exit;
}

add_action('wp_ajax_update_user_profile', 'update_user_profile');
add_action('wp_ajax_nopriv_update_user_profile', 'update_user_profile');


function dialog_enqueue_jquery()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-spinner');
}

add_action('wp_enqueue_scripts', 'dialog_enqueue_jquery');
add_action('admin_enqueue_scripts', 'dialog_enqueue_jquery');
