<?php

if (! defined('WPINC')) {
    die;
}

if (function_exists('acf_add_options_page')) {
    $parent = acf_add_options_page([
        'page_title' => __('メールフォームのカストマイズ'),
        'menu_title' => __('メールフォームのカストマイズ'),
        'menu_slug' => 'vrmall-mail-content-settings',
        'capability' => 'manage_options',
        'update_button' => __('更新'),
        'updated_message' => __('新しい設定は更新されました'),
    ]);

    acf_add_options_sub_page([
        'page_title'      => __('3D制作依頼'),
        'menu_title'      => __('3D制作依頼'),
        'menu_slug'       => 'vrmall-mail-content-3d-request',
        'parent_slug'     => $parent['menu_slug'],
        'capability'      => 'manage_options',
        'update_button'   => __('更新'),
        'updated_message' => __('新しい設定は更新されました'),
    ]);

    acf_add_options_sub_page([
        'page_title'      => __('来客者のメール設定'),
        'menu_title'      => __('来客者のメール設定'),
        'menu_slug'       => 'vrmall-mail-content-email-subscribe-settings',
        'parent_slug'     => $parent['menu_slug'],
        'capability'      => 'manage_options',
        'update_button'   => __('更新'),
        'updated_message' => __('新しい設定は更新されました'),
    ]);

    acf_add_options_sub_page([
        'page_title'      => __('出展者のメール設定'),
        'menu_title'      => __('出展者のメール設定'),
        'menu_slug'       => 'vrmall-mail-content-email-shop-manager-settings',
        'parent_slug'     => $parent['menu_slug'],
        'capability'      => 'manage_options',
        'update_button'   => __('更新'),
        'updated_message' => __('新しい設定は更新されました'),
    ]);


}
