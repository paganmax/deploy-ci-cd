<?php

class Production_Request_Form
{
    public $attachments = [];

    public function init()
    {
        add_shortcode('production_request_form', [$this, 'add_shortcode']);
        add_action('wp_enqueue_scripts', [$this, 'add_scripts']);
        add_action('admin_enqueue_scripts', [$this, 'add_admin_scripts']);
        add_action('wp_ajax_production_request', [$this, 'handle_production_request']);
        add_action('wp_ajax_nopriv_production_request', [$this, 'handle_production_request']);

        add_filter('acf/load_field', function ($field) {
            if ($field['name'] === 'vrmall_3d_model_request_email' || $field['name'] === 'vrmall_3d_model_request_phone_number' || $field['name'] === 'vrmall_3d_model_request_product_name') {
                $field['readonly'] = 'readonly';
            }

            if ($field['name'] === 'vrmall_3d_model_request_user') {
                $field['disabled'] = true;
            }

            return $field;
        });
    }

    function add_shortcode($attributes)
    {
        if (! is_user_logged_in()) {
            return file_get_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'login-required.html');
        }

        $user = wp_get_current_user();
        $html = file_get_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'form.html');

        $replaces = [
            '{{$user_name}}' => $user->display_name,
            '{{$user_email}}' => $user->user_email,
            '{{$user_phone_number}}' => get_user_meta($user->ID, 'billing_phone', true),
            '{{$max_product}}' => isset($attributes['max_product']) ? intval($attributes['max_product']) : 5,
            '{{$action_url}}' => admin_url('admin-ajax.php?action=production_request&nonce='.wp_create_nonce('production_request')),
        ];

        return strtr($html, $replaces);
    }

    function add_scripts()
    {
        wp_enqueue_script('production_request_form_scripts', plugin_dir_url(__FILE__).'/script.js');
    }

    function add_admin_scripts()
    {
        wp_enqueue_script('production_request_form_admin_scripts', plugin_dir_url(__FILE__).'/admin-script.js');
    }

    function handle_production_request()
    {
        if (! is_user_logged_in()) {
            echo json_encode(['success' => false, 'message' => 'login required']);
            exit();
        }

        $payload = $_POST;
        $data = [
            'name' => $payload['name'] ?? '',
            'email' => $payload['email'] ?? '',
            'phone_number' => $payload['phone_number'] ?? '',
            'products' => array_map(function ($item) {
                return $item['name'];
            }, $payload['products'] ?? []),
        ];

        $store_attachments_success = $this->handle_uploaded_files($_FILES['products'] ?? []);

        if (! $store_attachments_success) {
            wp_send_json_error(['error' => '商品には、jpg, png, pdfタイプのファイルを指定してください。'], 400);
        }

        $this->insert_request_item($data);
        $this->send_mail_to_admin();
        $this->send_mail_to_user($data['email']);

        wp_send_json(['success' => true]);
    }

    function send_mail_to_admin()
    {
        $title = get_field('vrmall_3d_request_mail_admin_title', 'options');
        $message = get_field('vrmall_3d_request_mail_admin_content', 'options');
        $content_type = function () {
            return 'text/html';
        };

        add_filter('wp_mail_content_type', $content_type);
        wp_mail(get_option('admin_email'), $title, $message, '');
        remove_filter('wp_mail_content_type', $content_type);
    }

    function send_mail_to_user($email)
    {
        $title = get_field('vrmall_3d_request_mail_user_title', 'options');
        $message = get_field('vrmall_3d_request_mail_user_content', 'options');
        $content_type = function () {
            return 'text/html';
        };

        add_filter('wp_mail_content_type', $content_type);
        wp_mail($email, $title, $message);
        remove_filter('wp_mail_content_type', $content_type);
    }

    function handle_uploaded_files($files)
    {
        $allowed_mimes = [
            'image/png',
            'image/jpeg',
            'application/pdf',
        ];

        foreach ($files['tmp_name'] as $index => $tmp_name) {
            $specifications = [
                'name' => $files['name'][$index]['spec'],
                'type' => $files['type'][$index]['spec'],
                'tmp_name' => $tmp_name['spec'],
                'error' => $files['error'][$index]['spec'],
                'size' => $files['size'][$index]['spec'],
            ];

            $result = wp_check_filetype_and_ext($specifications['tmp_name'], $specifications['name']);
            if (! in_array($result['type'], $allowed_mimes)) {
                return false;
            }

            $this->attachments[$index] = [];
            $this->attachments[$index]['specifications'] = media_handle_sideload($specifications);

            foreach ($tmp_name['images'] as $key => $images) {
                $image = [
                    'name' => $files['name'][$index]['images'][$key],
                    'type' => $files['type'][$index]['images'][$key],
                    'tmp_name' => $files['tmp_name'][$index]['images'][$key],
                    'error' => $files['error'][$index]['images'][$key],
                    'size' => $files['size'][$index]['images'][$key],
                ];

                $result = wp_check_filetype_and_ext($image['tmp_name'], $image['name']);
                if (! in_array($result['type'], $allowed_mimes)) {
                    return false;
                }

                $this->attachments[$index]['images'][] = media_handle_sideload($image);
            }
        }

        return true;
    }

    function insert_request_item($data)
    {
        $post_id = wp_insert_post([
            'post_title' => $data['name'].' - 3D制作依頼',
            'post_type' => '3d_model_request',
            'post_status' => 'private',
        ]);

        update_field('vrmall_3d_model_request_user', get_current_user_id(), $post_id);
        update_field('vrmall_3d_model_request_email', $data['email'], $post_id);
        update_field('vrmall_3d_model_request_phone_number', $data['phone_number'], $post_id);

        foreach ($data['products'] as $index => $product) {
            $row = [
                'vrmall_3d_model_request_product_name' => $product,
                'vrmall_3d_model_request_specifications' => $this->attachments[$index]['specifications'],
                'vrmall_3d_model_request_images' => $this->attachments[$index]['images'],
            ];
            add_row('vrmall_3d_model_request_products', $row, $post_id);
        }
    }
}

(new Production_Request_Form)->init();
