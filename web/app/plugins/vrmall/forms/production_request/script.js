jQuery(function () {
    const button = jQuery('[name="add_product"]');
    const product_container = jQuery('#vrmall_request_product_list');
    let count = 0;
    let total = 0;
    const max_product = parseInt(jQuery('[name="max_product"]').val()) || 5;

    add_product();

    button.on('click', add_product);
    jQuery(document).on('click', '[name="production_request"] .close', remove_product);
    jQuery(document).on('change', '[data-product-image]', validate_product_images);
    jQuery('[name="production_request"]').on('submit', handle_submit)

    function handle_submit(e) {
        e.preventDefault();

        const submit_button = jQuery(this).find(':submit');

        submit_button.prop('disabled', true);
        submit_button.find('.spinner').removeClass('d-none');
        jQuery('#production_request_send_error_wrapper').addClass('d-none');

        jQuery.ajax({
            method: 'POST',
            url: jQuery(this).prop('action'),
            data: new FormData(this),
            dataType: 'JSON',
            processData: false,
            contentType: false,
        }).done(function (response) {
            if (response.success) {
                jQuery('#production_request_form_wrapper').slideUp();
                jQuery('#production_request_send_ok_wrapper').removeClass('d-none');
            }
        }).fail(function (response) {
            console.log(response.responseJSON.data.error);
            if (response.responseJSON.data.error) {
                jQuery('#production_request_send_error_wrapper')
                    .removeClass('d-none')
                    .find('.alert')
                    .html(response.responseJSON.data.error);
            }
        }).always(function () {
            submit_button.prop('disabled', false);
            submit_button.find('.spinner').addClass('d-none');
        });
    }

    function add_product() {
        const template = generate_template(++count, ++total);
        product_container.append(template);
        check_total_product();
    }

    function remove_product() {
        jQuery(this).closest('div.card').remove();
        --total;
        check_total_product();
    }

    function check_total_product() {
        button.prop('disabled', total === max_product);
    }

    function generate_template(index, no) {
        let template = jQuery('#product_template').html();

        template = template.replaceAll('{{$index_no}}', no);
        template = template.replaceAll('{{$index}}', index);

        return template;
    }

    function validate_product_images() {
        if (jQuery(this)[0].files.length < 4) {
            jQuery(this).addClass('is-invalid');
            jQuery(this).next('.invalid-feedback').html('最低４枚の写真を添付ください');
            jQuery(this).val('');
        } else {
            jQuery(this).removeClass('is-invalid');
            jQuery(this).next('.invalid-feedback').html('');
        }
    }
});
