jQuery(function () {
    jQuery('[data-name="vrmall_3d_model_request_products"]').find('a[data-event="add-row"]').remove();
    jQuery('[data-name="vrmall_3d_model_request_products"]').find('a[data-event="remove-row"]').remove();
    jQuery('[data-name="vrmall_3d_model_request_products"]').find('.acf-gallery-toolbar .acf-gallery-add, .acf-gallery-toolbar .acf-gallery-sort').remove();
    jQuery('[data-name="vrmall_3d_model_request_products"]').find('.acf-gallery-attachments .actions').remove();
    jQuery('[data-name="vrmall_3d_model_request_products"]').find('.acf-file-uploader .acf-actions').remove();
});
