<?php

if ( ! defined('WPINC')) {
    die;
}


if ( ! function_exists('wp_password_change_notification')) {
    function wp_password_change_notification($user)
    {
        if (0 !== strcasecmp($user->user_email, get_option('admin_email'))) {
            /** translators: %s: user name */
            $message = sprintf(__('Password changed for user: %s'), $user->display_name)."\r\n";
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
            /** translators: %s: site title */
            wp_mail(get_option('admin_email'), sprintf(__('[%s] Password Changed'), $blogname), $message);
        }
    }
}


add_filter('wp_new_user_notification_email', 'custom_wp_new_user_notification_email', 10, 3);

function custom_wp_new_user_notification_email($wp_new_user_notification_email, $user, $blogname)
{
    $key = get_password_reset_key($user);

    $user_data  = get_userdata($user->ID);
    $user_roles = $user_data->roles;

    $company = get_user_meta($user->ID, "billing_company", true);
    $name = get_user_meta($user->ID, "billing_first_name", true);

    if (in_array("shop_manager", $user_roles)) {
        $message = "お待たせしました。"."<br/><br/>";
        $message .= "お客様のご出展受付が確定しましたので、以下の登録内容をご確認の上、管理者画面にログイン下さい。"."<br/>";
        $message .= '会社名: ( '.$company.' )'."<br/>";
        $message .= 'お名前: ( '.$name.' )'."<br/>";
        $message .= '以下のURLからパスワードを設定してください。'."<br/>";

        $link_term = get_home_url().'/terms/';

        $message .= network_site_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user->user_login),
                'login')."<br/><br/>";
        $message .= "重要事項"."<br/>";
        $message .= "ログインID、パスワードの管理はお客様の責任に置いて、管理をお願い致します。"."<br/>";
        $message .= "ログインID・パスワードの管理に関しては、ご利用規約に記載がありますので、ご確認下さい。"."<br/>";
        $message .= sprintf("<a href ='".$link_term."'> 利用規約へリンク設定 </a>")."<br/><br/>";
        $message .= "バーチャル展示会　出展受付事務局"."<br/>";
        $message .= "問合せ先：　aaaaaaa@bbbbbbb.com"."<br/>";


        $wp_new_user_notification_email['message'] = $message;
        $wp_new_user_notification_email['subject'] = "会員登録完了のお知らせ";
        $wp_new_user_notification_email['headers'] = ['Content-Type: text/html; charset=UTF-8'];
    }
    return $wp_new_user_notification_email;
}

add_filter( 'retrieve_password_message', 'my_retrieve_password_message', 10, 4 );
function my_retrieve_password_message( $message, $key, $user_login, $user_data ) {

    $message = __( 'パスワードリセットの申請を受け付けました。' ) . "\r\n\r\n";
    $message .= __( 'パスワードの再設定をご希望の場合は、以下URLをクリックし、新しいパスワードをご登録ください。' ) . "\r\n";
    $message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";
    $message .= __( 'このリンクは24時間後に無効となります。' ) . "\r\n\r\n";

    $message .= __( 'どうそよろしくお願いいたします。' ) . "\r\n";
    $message .= "ーーーーーーーーーーーーーーーーーーーーーーー".  "\r\n";
    $message .= "・このメールはVrmallより自動送信されています。".  "\r\n";
    $message .= "・このメールは送信専用です。返信できませんのでご注意ください。".  "\r\n";
    $message .= "ーーーーーーーーーーーーーーーーーーーーーーー".  "\r\n";

    return $message;

}

add_filter( 'retrieve_password_title', 'my_retrieve_password_title', 10, 5 );

function my_retrieve_password_title($title, $user_login, $user_data )
{
    return "パスワードリセットのお知らせ";
}


add_filter( 'wp_new_user_notification_email_admin', 'my_wp_new_user_notification_email_admin', 10, 6 );
function my_wp_new_user_notification_email_admin($wp_new_user_notification_email_admin, $user ) {

    $message = sprintf( __( '以下の出店者アカウントが作成されました。' )) . "\r\n";
    /* translators: %s: User login. */
    $message .= sprintf( __( 'ユーザー名: %s' ), $user->user_login ) . "\r\n\r\n";
    /* translators: %s: User email address. */
    $message .= sprintf( __( 'メール: %s' ), $user->user_email ) . "\r\n\r\n";
    $message .= "どうぞよろしくお願いいたします。";
    $wp_new_user_notification_email_admin["message"] =  $message;
    $wp_new_user_notification_email_admin['subject'] = "新しい会員登録のお知らせ";
    return $wp_new_user_notification_email_admin;
}

add_filter( 'wp_mail_from_name', 'custom_wpse_mail_from_name' );
function custom_wpse_mail_from_name( $original_email_from ) {
    return 'vrmall';
}
