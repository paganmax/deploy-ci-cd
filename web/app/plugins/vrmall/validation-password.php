<?php


if ( ! defined('WPINC')) {
    die;
}

add_filter('woocommerce_min_password_strength', 'reduce_min_strength_password_requirement');
function reduce_min_strength_password_requirement($strength)
{
    return 0;
}


add_filter('password_hint', 'indic_password_hint');
function indic_password_hint($hint)
{
    $hint = 'ヒント: 英数字含む８桁以上を入力する必要があります。';

    return $hint;
}


add_action('validate_password_reset', 'wdm_validate_password_reset', 10, 2);
function wdm_validate_password_reset($errors, $user)
{
    $exp_only_number_or_character = '/(?=.*[0-9])(?=.*[a-zA-Z])/';
    $exp_contain_special          = '/([^a-zA-Z0-9])/';

    if ( ! empty($_POST['password_1'])) {
        if (strlen($_POST['password_1']) < 8) {
            $errors->add('error', '英数字含む８桁以上を入力する必要があります。', '');
        } else {
            if (preg_match($exp_contain_special,
                $_POST['password_1'])) {
                $errors->add('error', '数字と文字しかご入力できません', '');
            }

            if ( ! preg_match($exp_only_number_or_character, $_POST['password_1']) && ! preg_match($exp_contain_special,
                    $_POST['password_1'])) {
                $errors->add('error', '数字と文字混合をご入力ください', '');
            }
        }

        if (wp_check_password($_POST['password_1'], $user->user_pass, $user->ID)) {
            $errors->add('error', 'このパスワードは最近使用されています。別のパスワードを入力してください', '');
        }
    }

    if ( ! empty($_POST['pass1'])) {
        if (strlen($_POST['pass1']) < 8) {
            $errors->add('error', '英数字含む８桁以上を入力する必要があります。', '');
        } else {
            if ( preg_match($exp_contain_special,
                $_POST['pass1'])) {
                $errors->add('error', '数字と文字しかご入力できません', '');
            }
            if ( ! preg_match($exp_only_number_or_character, $_POST['pass1']) && ! preg_match($exp_contain_special,
                    $_POST['pass1'])) {
                $errors->add('error', '数字と文字混合をご入力ください', '');
            }
        }

        if (wp_check_password($_POST['pass1'], $user->user_pass, $user->ID)) {
            $errors->add('error', 'このパスワードは最近使用されています。別のパスワードを入力してください', '');
        }
    }

    return $errors;
}

add_action('woocommerce_process_registration_errors', 'validatePasswordReg', 10, 2);

function validatePasswordReg($errors, $user)
{
    $exp_only_number_or_character = '/(?=.*[0-9])(?=.*[a-zA-Z])/';
    $exp_contain_special          = '/([^a-zA-Z0-9])/';

    if ( ! empty($_POST['password_1'])) {
        if (strlen($_POST['password_1']) < 8) {
            $errors->add('error', '英数字含む８桁以上を入力する必要があります。', '');
        } else {
            if (preg_match($exp_contain_special,
                $_POST['password_1'])) {
                $errors->add('error', '数字と文字しかご入力できません', '');
            }

            if ( ! preg_match($exp_only_number_or_character, $_POST['password_1']) && ! preg_match($exp_contain_special,
                    $_POST['password_1'])) {
                $errors->add('error', '数字と文字混合をご入力ください', '');
            }
        }

        if (wp_check_password($_POST['password_1'], $user->user_pass, $user->ID)) {
            $errors->add('error', 'このパスワードは最近使用されています。別のパスワードを入力してください', '');
        }
    }

    if ( ! empty($_POST['pass1'])) {
        if (strlen($_POST['pass1']) < 8) {
            $errors->add('error', '英数字含む８桁以上を入力する必要があります。', '');
        } else {
            if ( preg_match($exp_contain_special,
                $_POST['pass1'])) {
                $errors->add('error', '数字と文字しかご入力できません', '');
            }
            if ( ! preg_match($exp_only_number_or_character, $_POST['pass1']) && ! preg_match($exp_contain_special,
                    $_POST['pass1'])) {
                $errors->add('error', '数字と文字混合をご入力ください', '');
            }
        }

        if (wp_check_password($_POST['pass1'], $user->user_pass, $user->ID)) {
            $errors->add('error', 'このパスワードは最近使用されています。別のパスワードを入力してください', '');
        }
    }

    return $errors;
}

function worldless_login_custom_script()
{
    wp_enqueue_script('my_custom_script_hide_pw', plugins_url('scripts/hide-pw-weak.js', __FILE__), array('jquery'),
        '1.0',
        true);
}

add_action('login_enqueue_scripts', 'worldless_login_custom_script');


add_action('woocommerce_save_account_details_errors', 'wooc_validate_custom_field', 20, 2);

function wooc_validate_custom_field($args, $user)
{
    $exp_only_number_or_character = '/(?=.*[0-9])(?=.*[a-zA-Z])/';
    $exp_contain_special          = '/([^a-zA-Z0-9])/';

    if ( ! empty($_POST['password_1'])) {
        if (strlen($_POST['password_1']) < 8) {
            $args->add('error', '英数字含む８桁以上を入力する必要があります。', '');
        } else {
            if (preg_match($exp_contain_special,
                $_POST['password_1'])) {
                $args->add('error', '数字と文字しかご入力できません', '');
            }

            if ( ! preg_match($exp_only_number_or_character, $_POST['password_1']) && ! preg_match($exp_contain_special,
                    $_POST['password_1'])) {
                $args->add('error', '数字と文字混合をご入力ください', '');
            }
        }

        if (wp_check_password($_POST['password_1'], $user->user_pass, $user->ID)) {
            $args->add('error', 'このパスワードは最近使用されています。別のパスワードを入力してください', '');
        }
    }
}

