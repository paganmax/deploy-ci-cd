<?php

if (! defined('WPINC')) {
    die;
}

require_once 'helper.php';

function restrict_index_booth(WP_Query $query)
{
    if (! function_exists('get_current_screen')) {
        return $query;
    }

    if (! $query->is_admin || 'edit-booth' !== get_current_screen()->id) {
        return $query;
    }

    if (! current_user_can('manage_options') && 'booth' === $query->get('post_type')) {
        $ids = array_merge([0], get_current_user_booth_ids());
        $query->set('post__in', $ids);
    }

    return $query;
}

function restrict_editing_booth($capabilities, $capability, $args)
{
    if (('edit_post' != $args[0] && 'delete_post' != $args[0]) || ! empty($capabilities['manage_options']) || empty($capabilities['edit_posts'])) {
        return $capabilities;
    }

    $post = get_post($args[2]);

    if ($post->post_type !== 'booth') {
        return $capabilities;
    }

    if (! in_array($post->ID, get_current_user_booth_ids())) {
        $capabilities[$capability[0]] = false;
    }

    return $capabilities;
}

function restrict_count_booth(WP_Query $query)
{
    if (! function_exists('get_current_screen')) {
        return $query;
    }

    if (! $query->is_admin || 'edit-booth' !== get_current_screen()->id) {
        return $query;
    }

    if (! current_user_can('manage_options') && 'booth' === $query->get('post_type')) {
        add_filter('views_edit-booth', function ($views) {
            return fix_post_counts($views, 'booth');
        });
    }

    return $query;
}

add_filter('pre_get_posts', 'restrict_index_booth');
add_filter('pre_get_posts', 'restrict_count_booth');
add_filter('user_has_cap', 'restrict_editing_booth', 10, 3);

add_filter('acf/load_field/type=message', function ($field) {
    $post = get_post();

    if ($post && $post->post_type === 'booth') {
        $field['message'] = str_replace('{{VRUI_BASE_URL}}', rtrim(\Roots\WPConfig\Config::get('VRUI_BASE_URL'), '/'), $field['message']);
        $field['message'] = str_replace('{{BOOTH_ID}}', $post->ID, $field['message']);
    }

    return $field;
});

function booth_setting_booth_template_field_config($field)
{
    $templates = get_posts([
        'post_status' => 'publish',
        'post_type' => 'booth_template',
    ]);

    $noThumbnail = '<img src="'.plugins_url("images/placeholder-image.jpg", __FILE__).'" style="width:100px; height:100px; object-fit: cover;" />';

    foreach ($templates as $template) {
        $thumbnail = get_the_post_thumbnail($template, [100, 100]);
        $images = array_map(function ($image) {
            return '<img src="'.wp_get_attachment_url($image['image']).'" style="width:100px; height:100px; object-fit: cover;">';
        }, get_field('images', $template->ID) ?? []);

        $optionValue = $thumbnail.implode('', $images);

        $value = $optionValue ? $optionValue : $noThumbnail;

        $choices[$template->ID] = '<div><h3>'.$template->post_title.'</h3><p>'.$value.'</p></div>';
    }

    $field['required'] = false;
    $field['choices'] = $choices;
    $field['class'] = $field['class'].' booth-template-picker';

    return $field;
}

add_filter('acf/load_field/name=booth_setting__booth_template', 'booth_setting_booth_template_field_config');

function booth_setting__booth_template_admin_style()
{
    wp_enqueue_style('admin_style', plugins_url('css/acf.css', __FILE__));
}

add_action('admin_init', 'booth_setting__booth_template_admin_style');


function hide_add_new_booth()
{
    if(! current_user_can('manage_options'))
    {?>
        <style type="text/css">
            .post-php.post-type-booth .page-title-action {
                display: none;
            }
            .edit-php.post-type-booth .page-title-action {
                display: none;
            }
        </style>
    <?php }
}
add_action('admin_enqueue_scripts', 'hide_add_new_booth');

function add_admin_hide_add_new_booth_submenu()
{
    if(! current_user_can('manage_options')){
        remove_submenu_page('edit.php?post_type=booth', 'post-new.php?post_type=booth');
    }
}
add_action('admin_menu', 'add_admin_hide_add_new_booth_submenu');

add_filter('acf/load_value/name=vrmall_booth_signboard_outside', function ($value) {
    $value = is_array($value) ? $value : [];

    if (! function_exists('get_current_screen')) {
        return $value;
    }

    $screen = get_current_screen();
    if ($screen->post_type !== 'booth') {
        return $value;
    }

    $layouts = array_map(function ($item) {
        return $item['acf_fc_layout'];
    }, $value);

    $default_layouts = [
        'vrmall_booth_signboard_outside_signboard_group',
        'vrmall_booth_signboard_outside_image_group',
        'vrmall_booth_signboard_outside_video_group',
    ];

    foreach ($default_layouts as $default_layout) {
        if (! in_array($default_layout, $layouts)) {
            $value[] = ['acf_fc_layout' => $default_layout];
        }
    }

    return $value;
});

add_filter('acf/load_value/name=vrmall_booth_signboard_inside', function ($value) {
    $value = is_array($value) ? $value : [];

    if (! function_exists('get_current_screen')) {
        return $value;
    }

    $screen = get_current_screen();
    if ($screen->post_type !== 'booth') {
        return $value;
    }

    $layouts = array_map(function ($item) {
        return $item['acf_fc_layout'];
    }, $value);

    $default_layouts = [
        'vrmall_booth_signboard_inside_video_group',
        'vrmall_booth_signboard_inside_image_group',
        'vrmall_booth_signboard_inside_decoration_group',
    ];

    foreach ($default_layouts as $default_layout) {
        if (! in_array($default_layout, $layouts)) {
            $value[] = ['acf_fc_layout' => $default_layout];
        }
    }

    return $value;
});

add_filter('acf/fields/post_object/query/name=vrmall_booth_signboard_inside_decoration', function ($args, $field, $id) {
    $args['meta_key'] = 'url_3d';
    $args['meta_value'] = [''];
    $args['meta_compare'] = 'NOT IN';

    return $args;
}, 10, 3);

add_filter('acf/fields/post_object/result/name=vrmall_booth_signboard_inside_decoration', function ($text, $post, $field, $post_id) {
    $url3d = get_field('url_3d', $post->ID);

    if (!$url3d) {
        return null;
    }

    return $text;
}, 10, 4);

add_action('admin_enqueue_scripts', function () {
    $screen = get_current_screen();

    if ('post' === $screen->base && 'booth' === $screen->post_type) {
        wp_enqueue_script('vrmall_admin_booth', plugin_dir_url(__FILE__).'/scripts/booth.js', [], '20211116');
    }
});

// update image/video inside booth position to api backward compatibility
add_action('save_post', function ($post_id, $post) {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if ('booth' === $post->post_type) {
        $rows = get_field('vrmall_booth_signboard_inside', $post_id);

        $items = array_filter($rows, function ($row) {
            return in_array($row['acf_fc_layout'], [
                'vrmall_booth_signboard_inside_video_group',
                'vrmall_booth_signboard_inside_image_group',
            ]);
        });

        $items = collect($items)
            ->filter(function ($item) {
                return trim($item['vrmall_booth_signboard_inside_video_object_id'])
                    || trim($item['vrmall_booth_signboard_inside_image_object_id']);
            })
            ->flatMap(function ($item) {
                if ($item['acf_fc_layout'] === 'vrmall_booth_signboard_inside_image_group') {
                    $object_id = $item['vrmall_booth_signboard_inside_image_object_id'];

                    return collect($item['vrmall_booth_signboard_inside_images'])
                        ->map(function ($image, $index) use ($object_id) {
                            return [
                                'idItem' => $image['vrmall_booth_signboard_inside_image']['ID'],
                                'idOwner' => $object_id,
                                'index' => $index + 1,
                                'linkIcon' => null,
                                'linkImgOrVideo' => $image['vrmall_booth_signboard_inside_image']['url'],
                                'time' => (int) $image['vrmall_booth_signboard_inside_image_display_duration'],
                                'link' => null,
                                'typeImgOrVideo' => 0,
                            ];
                        })
                        ->toArray();
                }

                $object_id = $item['vrmall_booth_signboard_inside_video_object_id'];

                return collect($item['vrmall_booth_signboard_inside_videos'])
                    ->map(function ($image, $index) use ($object_id) {
                        return [
                            'idItem' => $image['vrmall_booth_signboard_inside_video']['ID'],
                            'idOwner' => $object_id,
                            'index' => $index + 1,
                            'linkIcon' => null,
                            'linkImgOrVideo' => $image['vrmall_booth_signboard_inside_video']['url'],
                            'time' => 0,
                            'link' => null,
                            'typeImgOrVideo' => 1,
                        ];
                    })
                    ->toArray();
            })->toArray();

        $data = get_post_meta($post_id, 'vrmall_booth_product_position', true);
        $data = is_array($data) ? $data : [];
        $data['imgorvideo'] = array_values($items);

        update_post_meta($post_id, 'vrmall_booth_product_position', $data);
    }
}, 10, 2);

// update 3d decoration inside booth
add_action('save_post', function ($post_id, $post) {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if ('booth' === $post->post_type) {
        $rows = get_field('vrmall_booth_signboard_inside', $post_id);

        $items = array_filter($rows, function ($row) {
            return $row['acf_fc_layout'] === 'vrmall_booth_signboard_inside_decoration_group';
        });

        $i = 0;

        $items = collect($items)
            ->filter(function ($item) {
                return trim($item['vrmall_booth_signboard_inside_decoration_object_id']);
            })
            ->flatMap(function ($item) use (&$i) {
                $object_id = $item['vrmall_booth_signboard_inside_decoration_object_id'];

                return collect($item['vrmall_booth_signboard_inside_decorations'] ?? [])
                    ->map(function ($decoration, $index) use ($object_id, &$i) {
                        /** @var \WP_Post $post */
                        $post = $decoration['vrmall_booth_signboard_inside_decoration'];
                        $post_field = get_fields($post->ID);
                        $metadata_field = json_decode($decoration['vrmall_booth_signboard_inside_decoration_metadata'] ?? '', true);
                        $metadata_field = is_array($metadata_field) ? $metadata_field : [];

                        $i++;

                        return [
                                'decoName' => $object_id,
                                'boardId' => $index,
                                'objId' => $i,
                                'mulscale' => 1,
                                'link' => null,
                                'id' => $post->ID,
                                'name' => $post->post_title,
                                'description' => $post->post_content,
                                'short_description' => null,
                                'thumbnail' => null,
                                'icon_url' => $post_field['icon'] ?? null,
                                'item_type' => 1,
                                'settings' => [
                                    'view_mode' => null,
                                    'view_mode_model' => [
                                        'url' => null,
                                    ],
                                    'view_mode_image' => [
                                        'url' => null,
                                    ],
                                    'view_mode_video' => [
                                        'url' => null,
                                    ],
                                ],
                                '3d_url' => $post_field['url_3d'] ?? null,
                            ] + $metadata_field;
                    })
                    ->toArray();
            })->toArray();

        $data = get_post_meta($post_id, 'vrmall_booth_decoration_position', true);
        $data = is_array($data) ? $data : [];
        $data['items'] = array_values($items);

        update_post_meta($post_id, 'vrmall_booth_decoration_position', $data);
    }
}, 10, 2);

