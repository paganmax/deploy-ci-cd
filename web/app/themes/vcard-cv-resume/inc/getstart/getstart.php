<?php
//about theme info
add_action( 'admin_menu', 'vcard_cv_resume_gettingstarted' );
function vcard_cv_resume_gettingstarted() {
	add_theme_page( esc_html__('About Vcard CV Resume', 'vcard-cv-resume'), esc_html__('About Vcard CV Resume', 'vcard-cv-resume'), 'edit_theme_options', 'vcard_cv_resume_guide', 'vcard_cv_resume_mostrar_guide');
}

// Add a Custom CSS file to WP Admin Area
function vcard_cv_resume_admin_theme_style() {
	wp_enqueue_style('vcard-cv-resume-custom-admin-style', esc_url(get_template_directory_uri()) . '/inc/getstart/getstart.css');
	wp_enqueue_script('vcard-cv-resume-tabs', esc_url(get_template_directory_uri()) . '/inc/getstart/js/tab.js');
	wp_enqueue_style( 'font-awesome-css', esc_url(get_template_directory_uri()).'/assets/css/fontawesome-all.css' );
}
add_action('admin_enqueue_scripts', 'vcard_cv_resume_admin_theme_style');

//guidline for about theme
function vcard_cv_resume_mostrar_guide() { 
	//custom function about theme customizer
	$vcard_cv_resume_return = add_query_arg( array()) ;
	$vcard_cv_resume_theme = wp_get_theme( 'vcard-cv-resume' );
?>

<div class="wrapper-info">
    <div class="col-left">
    	<h2><?php esc_html_e( 'Welcome to Vcard CV Resume', 'vcard-cv-resume' ); ?> <span class="version"><?php esc_html_e( 'Version', 'vcard-cv-resume' ); ?>: <?php echo esc_html($vcard_cv_resume_theme['Version']);?></span></h2>
    	<p><?php esc_html_e('All our WordPress themes are modern, minimalist, 100% responsive, seo-friendly,feature-rich, and multipurpose that best suit designers, bloggers and other professionals who are working in the creative fields.','vcard-cv-resume'); ?></p>
    </div>

    <div class="tab-sec">
    	<div class="tab">
			<button class="tablinks" onclick="vcard_cv_resume_open_tab(event, 'lite_theme')"><?php esc_html_e( 'Setup With Customizer', 'vcard-cv-resume' ); ?></button>
			<button class="tablinks" onclick="vcard_cv_resume_open_tab(event, 'gutenberg_editor')"><?php esc_html_e( 'Setup With Gutunberg Block', 'vcard-cv-resume' ); ?></button>
		</div>

		<?php
			$vcard_cv_resume_plugin_custom_css = '';
			if(class_exists('Ibtana_Visual_Editor_Menu_Class')){
				$vcard_cv_resume_plugin_custom_css ='display: block';
			}
		?>
		<div id="lite_theme" class="tabcontent open">
			<?php if(!class_exists('Ibtana_Visual_Editor_Menu_Class')){ 
				$plugin_ins = Vcard_CV_Resume_Plugin_Activation_Settings::get_instance();
				$vcard_cv_resume_actions = $plugin_ins->recommended_actions;
				?>
				<div class="vcard-cv-resume-recommended-plugins">
				    <div class="vcard-cv-resume-action-list">
				        <?php if ($vcard_cv_resume_actions): foreach ($vcard_cv_resume_actions as $key => $vcard_cv_resume_actionValue): ?>
				                <div class="vcard-cv-resume-action" id="<?php echo esc_attr($vcard_cv_resume_actionValue['id']);?>">
			                        <div class="action-inner">
			                            <h3 class="action-title"><?php echo esc_html($vcard_cv_resume_actionValue['title']); ?></h3>
			                            <div class="action-desc"><?php echo esc_html($vcard_cv_resume_actionValue['desc']); ?></div>
			                            <?php echo wp_kses_post($vcard_cv_resume_actionValue['link']); ?>
			                            <a class="ibtana-skip-btn" get-start-tab-id="lite-theme-tab" href="javascript:void(0);"><?php esc_html_e('Skip','vcard-cv-resume'); ?></a>
			                        </div>
				                </div>
				            <?php endforeach;
				        endif; ?>
				    </div>
				</div>
			<?php } ?>
			<div class="lite-theme-tab" style="<?php echo esc_attr($vcard_cv_resume_plugin_custom_css); ?>">
				<h3><?php esc_html_e( 'Lite Theme Information', 'vcard-cv-resume' ); ?></h3>
				<hr class="h3hr">
				<p><?php esc_html_e('Create a highly dynamic and incredibly professional website for a Resume club using this Free Resume WordPress Theme. Using this readymade theme, you can hammer out a web page for representing any Resume such as soccer, football, basketball, tennis, badminton, motorResume, or any other Resume. Resumepersons may also find this theme useful to show their profession online. It is a top-notch theme with modern quality and expertly written codes making your site perform well under any circumstances and take the user experience to the next level. Its design is currently trending as it very well shows the details right in front and visitors will find it easy to navigate on your website as it comprises simple menus. With some fantastic customization options for changing the default colors given in the color palette, Font Awesome integration with 100+ typography options will give you a chance to at least give some fresh look.','vcard-cv-resume'); ?></p>
			  	<div class="col-left-inner">
			  		<h4><?php esc_html_e( 'Theme Documentation', 'vcard-cv-resume' ); ?></h4>
					<p><?php esc_html_e( 'If you need any assistance regarding setting up and configuring the Theme, our documentation is there.', 'vcard-cv-resume' ); ?></p>
					<div class="info-link">
						<a href="<?php echo esc_url( VCARD_CV_RESUME_FREE_THEME_DOC ); ?>" target="_blank"> <?php esc_html_e( 'Documentation', 'vcard-cv-resume' ); ?></a>
					</div>
					<hr>
					<h4><?php esc_html_e('Theme Customizer', 'vcard-cv-resume'); ?></h4>
					<p> <?php esc_html_e('To begin customizing your website, start by clicking "Customize".', 'vcard-cv-resume'); ?></p>
					<div class="info-link">
						<a target="_blank" href="<?php echo esc_url( admin_url('customize.php') ); ?>"><?php esc_html_e('Customizing', 'vcard-cv-resume'); ?></a>
					</div>
					<hr>
					<h4><?php esc_html_e('Having Trouble, Need Support?', 'vcard-cv-resume'); ?></h4>
					<p> <?php esc_html_e('Our dedicated team is well prepared to help you out in case of queries and doubts regarding our theme.', 'vcard-cv-resume'); ?></p>
					<div class="info-link">
						<a href="<?php echo esc_url( VCARD_CV_RESUME_SUPPORT ); ?>" target="_blank"><?php esc_html_e('Support Forum', 'vcard-cv-resume'); ?></a>
					</div>
					<hr>
					<h4><?php esc_html_e('Reviews & Testimonials', 'vcard-cv-resume'); ?></h4>
					<p> <?php esc_html_e('All the features and aspects of this WordPress Theme are phenomenal. I\'d recommend this theme to all.', 'vcard-cv-resume'); ?></p>
					<div class="info-link">
						<a href="<?php echo esc_url( VCARD_CV_RESUME_REVIEW ); ?>" target="_blank"><?php esc_html_e('Reviews', 'vcard-cv-resume'); ?></a>
					</div>
					<div class="link-customizer">
						<h3><?php esc_html_e( 'Link to customizer', 'vcard-cv-resume' ); ?></h3>
						<hr class="h3hr">
						<div class="first-row">
							<div class="row-box">
								<div class="row-box1">
									<span class="dashicons dashicons-buddicons-buddypress-logo"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[control]=custom_logo') ); ?>" target="_blank"><?php esc_html_e('Upload your logo','vcard-cv-resume'); ?></a>
								</div>
								<div class="row-box2">
									<span class="dashicons dashicons-format-gallery"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[section]=vcard_cv_resume_post_settings') ); ?>" target="_blank"><?php esc_html_e('Post settings','vcard-cv-resume'); ?></a>
								</div>
							</div>

							<div class="row-box">
								<div class="row-box1">
									<span class="dashicons dashicons-slides"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[section]=vcard_cv_resume_slidersettings') ); ?>" target="_blank"><?php esc_html_e('Slider Settings','vcard-cv-resume'); ?></a>
								</div>
								<div class="row-box2">
									<span class="dashicons dashicons-category"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[section]=vcard_cv_resume_services') ); ?>" target="_blank"><?php esc_html_e('Game Section','vcard-cv-resume'); ?></a>
								</div>
							</div>
						
							<div class="row-box">
								<div class="row-box1">
									<span class="dashicons dashicons-menu"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[panel]=nav_menus') ); ?>" target="_blank"><?php esc_html_e('Menus','vcard-cv-resume'); ?></a>
								</div>
								<div class="row-box2">
									<span class="dashicons dashicons-screenoptions"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[panel]=widgets') ); ?>" target="_blank"><?php esc_html_e('Footer Widget','vcard-cv-resume'); ?></a>
								</div>
							</div>
							
							<div class="row-box">
								<div class="row-box1">
									<span class="dashicons dashicons-admin-generic"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[section]=vcard_cv_resume_left_right') ); ?>" target="_blank"><?php esc_html_e('General Settings','vcard-cv-resume'); ?></a>
								</div>
								<div class="row-box2">
									<span class="dashicons dashicons-text-page"></span><a href="<?php echo esc_url( admin_url('customize.php?autofocus[section]=vcard_cv_resume_footer') ); ?>" target="_blank"><?php esc_html_e('Footer Text','vcard-cv-resume'); ?></a>
								</div>
							</div>
						</div>
					</div>
			  	</div>
				<div class="col-right-inner">
					<h3 class="page-template"><?php esc_html_e('How to set up Home Page Template','vcard-cv-resume'); ?></h3>
				  	<hr class="h3hr">
					<p><?php esc_html_e('Follow these instructions to setup Home page.','vcard-cv-resume'); ?></p>
                  	<p><span class="strong"><?php esc_html_e('1. Create a new page :','vcard-cv-resume'); ?></span><?php esc_html_e(' Go to ','vcard-cv-resume'); ?>
					  	<b><?php esc_html_e(' Dashboard >> Pages >> Add New Page','vcard-cv-resume'); ?></b></p>
                  	<p><?php esc_html_e('Name it as "Home" then select the template "Custom Home Page".','vcard-cv-resume'); ?></p>
                  	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/inc/getstart/images/home-page-template.png" alt="" />
                  	<p><span class="strong"><?php esc_html_e('2. Set the front page:','vcard-cv-resume'); ?></span><?php esc_html_e(' Go to ','vcard-cv-resume'); ?>
					  	<b><?php esc_html_e(' Settings >> Reading ','vcard-cv-resume'); ?></b></p>
				  	<p><?php esc_html_e('Select the option of Static Page, now select the page you created to be the homepage, while another page to be your default page.','vcard-cv-resume'); ?></p>
                  	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/inc/getstart/images/set-front-page.png" alt="" />
                  	<p><?php esc_html_e(' Once you are done with setup, then follow the','vcard-cv-resume'); ?> <a class="doc-links" href="<?php echo esc_url( VCARD_CV_RESUME_FREE_THEME_DOC ); ?>" target="_blank"><?php esc_html_e('Documentation','vcard-cv-resume'); ?></a></p>
			  	</div>
			</div>
		</div>
		
		<div id="gutenberg_editor" class="tabcontent">
			<?php if(!class_exists('Ibtana_Visual_Editor_Menu_Class')){ 
			$plugin_ins = Vcard_CV_Resume_Plugin_Activation_Settings::get_instance();
			$vcard_cv_resume_actions = $plugin_ins->recommended_actions;
			?>
				<div class="vcard-cv-resume-recommended-plugins">
				    <div class="vcard-cv-resume-action-list">
				        <?php if ($vcard_cv_resume_actions): foreach ($vcard_cv_resume_actions as $key => $vcard_cv_resume_actionValue): ?>
				                <div class="vcard-cv-resume-action" id="<?php echo esc_attr($vcard_cv_resume_actionValue['id']);?>">
			                        <div class="action-inner plugin-activation-redirect">
			                            <h3 class="action-title"><?php echo esc_html($vcard_cv_resume_actionValue['title']); ?></h3>
			                            <div class="action-desc"><?php echo esc_html($vcard_cv_resume_actionValue['desc']); ?></div>
			                            <?php echo wp_kses_post($vcard_cv_resume_actionValue['link']); ?>
			                        </div>
				                </div>
				            <?php endforeach;
				        endif; ?>
				    </div>
				</div>
			<?php }else{ ?>
				<h3><?php esc_html_e( 'Gutunberg Blocks', 'vcard-cv-resume' ); ?></h3>
				<hr class="h3hr">
				<div class="vcard-cv-resume-pattern-page">
			    	<a href="<?php echo esc_url( admin_url( 'admin.php?page=ibtana-visual-editor-templates' ) ); ?>" class="vw-pattern-page-btn ibtana-dashboard-page-btn button-primary button"><?php esc_html_e('Ibtana Settings','vcard-cv-resume'); ?></a>
			    </div>
			<?php } ?>
		</div>

	</div>
</div>

<?php } ?>