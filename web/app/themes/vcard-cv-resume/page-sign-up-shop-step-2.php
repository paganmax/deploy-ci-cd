<?php
/**
 * Template Name: Sign up shop step 2
 */

get_header(); ?>

<?php do_action('vcard_cv_resume_page_top'); ?>

<main id="maincontent" class="middle-align pt-5" role="main">
    <div class="container">
        <div class="row" style="height: 950px;">
            <id
            ="our-services" class="services col-lg-8 col-md-8">
            <?php

            $url   = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $parts = parse_url($url);
            parse_str($parts['query'], $query);

            $args  = [
                'post_type'   => 'signupshop',
                'post_status' => 'publish',
                'meta_key'  => 'user_sub_token',
                'meta_value' => $query['ut']
            ];
            $posts = get_posts($args);
            if (!empty($posts)) {
                echo do_shortcode(SHORTCODE_SHOP_STEP_2);
            } else {
                ?>
                <div class="container">
                    <main id="maincontent" role="main">
                        <div class="page-content">
                            <h1><?php esc_html_e('リンクは無効になりました。', 'vcard-cv-resume'); ?></h1>
                            <p class="text-404"><?php esc_html_e('お手数ですが、再度登録をお願いします。',
                                    'vcard-cv-resume'); ?></p>
                            <div class="more-btn mt-4 mb-4">
                                <a class="p-3" href="<?php echo esc_url(get_home_url()); ?>"><?php esc_html_e('戻る',
                                        'vcard-cv-resume'); ?><span
                                            class="screen-reader-text"><?php esc_html_e('戻る',
                                            'vcard-cv-resume'); ?></span></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </main>
                </div>
                <?php
            } ?>
        </div>
    </div>
    <div class="clearfix"></div>
</main>

<?php do_action('vcard_cv_resume_page_bottom'); ?>

<?php get_footer(); ?>
