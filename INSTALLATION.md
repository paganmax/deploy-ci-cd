## Project Structure

```
├── composer.json
├── config
│   ├── application.php
│   └── environments
│       ├── development.php
│       ├── staging.php
│       └── production.php
├── vendor
└── web
    ├── app
    │   ├── mu-plugins
    │   ├── plugins
    │   ├── themes
    │   └── uploads
    ├── wp-config.php
    ├── index.php
    └── wp
```

## Server Requirements
- PHP 7.4.x
- MySQL 8.0.x
- Composer 2.x ([https://getcomposer.org/download/](https://getcomposer.org/download/))
  - [Installation](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos)

## Installation Guide
- [SSH](./docs/SSH.md)
- [Deploy Source Code](./docs/DEPLOY_CODE.md)
- [SSL Configuration](./docs/SSL.md)
- [JWT Authentication for WP-API Configuration](./docs/JWT_AUTHENTICATION.md)
- [Create Cloud Storage Bucket And Get Storage Bucket Key](./docs/CREATE_CLOUD_STORAGE.md)
- [Google Cloud Storage -> Configuring cross-origin resource sharing (CORS)](./docs/GOOGLE_CLOUD_STORAGE.md)
- [Setup Offload Media Plugin](./docs/CONFIG_OFFLOAD_MEDIA.md)
- [Create Email To Send Email](./docs/SETUP_EMAIL_SEND_EMAIL.md)
- [Setup Easy WP SMTP](./docs/SETUP_EASY_SMTP.md)
- [Config Title Page](./docs/CONFIG_TITLE_PAGE.md)
- [Change Wordpress Language](./docs/CONFIG_LANGUAGE_PAGE_WORDPRESS.md)
- [Use Plugin Loco Translate To Translate Wordpress](./docs/USE_LOCAL_TRANSLATE.md)

## API Document
- [API Document](https://documenter.getpostman.com/view/16194988/TzeTHUJG)

## Installation
1. Create a new file name `.env` in project root folder (make a copy from `.env.example` with example configuration)
```shell
cp .env.example .env
```

2. Update environment variables in the `.env` file:
- Database variables
  - `DB_NAME` - Database name
  - `DB_USER` - Database user
  - `DB_PASSWORD` - Database password
  - `DB_HOST` - Database host
- `WP_ENV` - Set to environment (`development`, `staging`, `production`)
- `WP_HOME` - Full URL to WordPress home (https://example.com)
- `WP_SITEURL` - Full URL to WordPress including subdirectory (https://example.com/wp)
- `VRUI_BASE_URL` - Full URL to Unity home help Unity and Wordpress can exchange data (https://example-unity.com) 
- `GCP_KEY` - Google cloud platform key files (.json file) to store file to google cloud storage
- `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT` `JWT_AUTH_SECRET_KEY`
  - Generate with [WordPress salts generator](https://roots.io/salts.html)
- Set the document root on your webserver to web folder: `/path/to/site/web/`

3. Install dependency by running below command:
```shell
composer install --optimize-autoloader --no-dev
```
4. Import DB from `db/vrmallwp_2021-11-18.sql`
```shell
mysql -uroot -p 
```

```shell
create database vrmall;
exit;
```

```shell
mysql -u root -p vrmall < vrmallwp_2021-11-18.sql
```

5. Assign ownership to the account that we are currently signed in on or default www-data

```shell
sudo chown -R www-data:www-data your webserver (example: vrmall)
```

6. assign permissions read,write,execute on project

```shell
sudo chmod -R 755 your webserver (example: vrmall)
```

7. Default username and password is
```
username: admin
password: 1234567890
```

## nginx config example
```
upstream php {
  server unix:/run/php/php7.4-fpm.sock;
}

server {
  listen 80;
  listen [::]:80;
  server_name vrmall.net;

  return 302 https://$server_name$request_uri;
}

server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  ssl_certificate      /etc/nginx/ssl/vrmall.pem;
  ssl_certificate_key  /etc/nginx/ssl/vrmall.key;

  root /var/www/vrmall/web;
  index index.php index.html index.htm index.nginx-debian.html;
  server_name vrmall.net;

  client_max_body_size 200M;

  location / {
    try_files $uri $uri/ /index.php?$args;
  }

  location ~ \.php$ {
    include fastcgi_params;
    fastcgi_intercept_errors on;
    fastcgi_pass php;
    fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
  }

  location ~ /\.ht {
    deny all;
  }
}
```
