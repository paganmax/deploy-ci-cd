## Config Title Page

- Access domain Wordpress project and press admin's username, admin's password

![image](images/setup_easy_smtp/easy_SMTP_1.JPG)

- Find and choose submenu 'カスタマイズ' in menu '外観'

![image](images/config_title/config_title_1.jpg)

- Find and choose 'サイト基本情報' and setup like bellow

![image](images/config_title/config_title_2.jpg)

![image](images/config_title/config_title_3.jpg)

![image](images/config_title/config_title_4.jpg)

![image](images/config_title/config_title_5.jpg)

- Your title's page will change like bellow

![image](images/config_title/config_title_6.jpg)
