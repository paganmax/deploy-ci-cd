## Config Language Wordpress
- Access server by ssh we have like bellow

![image](images/setting_page_language/change_language_1.JPG)

- Access role's root and access your project's folder with command like bellow
```shell
sudo su -
 ```

```shell
cd /var/www/your-project-folder
Example: cd /var/www/vrmallwp
 ```
![image](images/setting_page_language/change_language_2.JPG)

- Access config folder(folder setting variable for your press) with command

```shell
cd config
 ```

![image](images/setting_page_language/change_language_3.JPG)

- Open file application.php and add command below like image

```shell
nano application.php
 ```

 ```shell
define('FS_METHOD', 'direct');
  ```

![image](images/setting_page_language/change_language_4.JPG)

- Press 'Ctrl + x' to save
- Press 'Y' to agree
- Press "Enter" 
- Press like below to back root project

```shell
cd ..
 ```

- Change WP_ENV to 'development' in .env file like bellow

```shell
nano .env
 ```

![image](images/setting_page_language/change_language_5.JPG)

- Press 'Ctrl + x' to save
- Press 'Y' to agree
- Press "Enter" 

- Run command like below to refresh nginx and fpm

```shell
systemctl restart nginx
 ```

```shell
systemctl restart php7.4-fpm
 ```

- return and access domain Wordpress project and press admin's username, admin's password

![image](images/setting_page_language/change_language_6.JPG)

- Find and choose 'General' in 'Setting' like below

![image](images/setting_page_language/change_language_7.JPG)

- Change your language like bellow

![image](images/setting_page_language/change_language_8.JPG)

- After that log out

![image](images/setting_page_language/change_language_9.JPG)


- We have like that

![image](images/setting_page_language/change_language_10.JPG)

- Login again and change language display in account super admin

![image](images/setting_page_language/change_language_11.JPG)

![image](images/setting_page_language/change_language_12.JPG)

![image](images/setting_page_language/change_language_13.JPG)
 
- Your web change like bellow

![image](images/setting_page_language/change_language_14.JPG)

- Return ssh server and change WP_ENV to 'production' in env file like bellow

 ```shell
 nano .env
```

![image](images/setting_page_language/change_language_15.JPG)

- Press 'Ctrl + x' to save
- Press 'Y' to agree
- Press "Enter" 

- Run command like below to refresh nginx and fpm

```shell
systemctl restart nginx
 ```

```shell
systemctl restart php7.4-fpm
 ```

- Return your web and press 'F5' to refresh web we have like bellow

![image](images/setting_page_language/change_language_16.JPG)


