## Create Email To Send Email <a name="setup_email"></a>
### Requirement
- Use google email account
- Email must turn on 'Turn your phone to sign in'
- Email must turn on '2 Steps Verification'
- Email must turn on 'App password'
- Your phone must sign in google email setup send email
### Steps setup email
- Add email want setup send email to your phone like tutorial of gmail ([https://support.google.com/mail/answer/6078445?hl=en&co=GENIE.Platform%3DAndroid#zippy=%2Cadd-an-account](https://support.google.com/mail/answer/6078445?hl=en&co=GENIE.Platform%3DAndroid#zippy=%2Cadd-an-account)) 
- Sign in account on your pc and setup like below

![image](images/setup_email_send_email/easy_smtp_step_1.JPG)

![image](images/setup_email_send_email/easy_smtp_step_2.JPG)

![image](images/setup_email_send_email/easy_smtp_step_3.JPG)

![image](images/setup_email_send_email/easy_smtp_step_4.JPG)

![image](images/setup_email_send_email/easy_smtp_step_5.JPG)

![image](images/setup_email_send_email/easy_smtp_step_6.JPG)

![image](images/setup_email_send_email/easy_smtp_step_7.JPG)

![image](images/setup_email_send_email/easy_smtp_step_8.JPG)

![image](images/setup_email_send_email/easy_smtp_step_9.JPG)

![image](images/setup_email_send_email/easy_smtp_step_10.JPG)

![image](images/setup_email_send_email/easy_smtp_step_11.JPG)

![image](images/setup_email_send_email/easy_smtp_step_12.JPG)

![image](images/setup_email_send_email/easy_smtp_step_13.JPG)

![image](images/setup_email_send_email/easy_smtp_step_14.JPG)

![image](images/setup_email_send_email/easy_smtp_step_15.JPG)

![image](images/setup_email_send_email/easy_smtp_step_16.JPG)

![image](images/setup_email_send_email/easy_smtp_step_17.JPG)

![image](images/setup_email_send_email/easy_smtp_step_18.JPG)

![image](images/setup_email_send_email/easy_smtp_step_19.JPG)

![image](images/setup_email_send_email/easy_smtp_step_20.JPG)

![image](images/setup_email_send_email/easy_smtp_step_21.JPG)

![image](images/setup_email_send_email/easy_smtp_step_22.JPG)

![image](images/setup_email_send_email/easy_smtp_step_23.JPG)

![image](images/setup_email_send_email/easy_smtp_step_24.JPG)

![image](images/setup_email_send_email/easy_smtp_step_25.JPG)

![image](images/setup_email_send_email/easy_smtp_step_26.JPG)

![image](images/setup_email_send_email/easy_smtp_step_27.JPG)

![image](images/setup_email_send_email/easy_smtp_step_28.JPG)

![image](images/setup_email_send_email/easy_smtp_step_29.JPG)

![image](images/setup_email_send_email/easy_smtp_step_30.JPG)

![image](images/setup_email_send_email/easy_smtp_step_31.JPG)

![image](images/setup_email_send_email/easy_smtp_step_32.JPG)
