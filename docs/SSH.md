# SSH

## Generating a new SSH key
1. Open Terminal.
2. Paste the text below, substituting in your email address.
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
This creates a new SSH key, using the provided email as a label.

```
>  Generating public/private algorithm key pair.
```
- When you're prompted to "Enter a file in which to save the key," press Enter. This accepts the default file location.
```
>  Enter a file in which to save the key (/Users/you/.ssh/id_algorithm): [Press enter]
```
- At the prompt, type a secure passphrase.
```
> Enter passphrase (empty for no passphrase): [Type a passphrase]
> Enter same passphrase again: [Type passphrase again]
```

## Add the public SSH key to a GCP Virtual Machine
1. Copy you `~/.ssh/id_rsa.pub` in case you have generated your SSH keys
2. Create an instance if not already created, to which you want to SSH.
3. Go to `Compute Engine → Metadata`

![gcpssh](images/gcp_ssh_1.png)
5. Then just edit or add your SSH key there.

![gcpssh](images/gcp_ssh_2.png)

Now you can SSH into this VM instance by running on your terminal
```
ssh <username>@<IP>
```

Now if you want to provide others the ability to SSH using public keys, just append their public key to `~/.ssh/authorized_keys`. Now you’re all set to do use your new server according to your will
