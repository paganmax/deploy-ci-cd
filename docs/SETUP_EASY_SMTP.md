## Setup Easy WP SMTP <a name="easy_smtp"></a> 

- Access domain Wordpress project and press admin's username, admin's password

![image](images/setup_easy_smtp/easy_SMTP_1.JPG)


- Check and turn on if  plugins Easy WP SMTP is turn off
![image](images/setup_easy_smtp/easy_SMTP_2.JPG)

- Like bellow is successfull

![image](images/setup_easy_smtp/easy_SMTP_3.JPG)

- Find and choose plugins Easy WP SMTP

![image](images/setup_easy_smtp/easy_SMTP_4.JPG)

- We setup plugin to send email like below

![image](images/setup_easy_smtp/easy_SMTP_5.JPG)

- We test send email like below

![image](images/setup_easy_smtp/easy_SMTP_6.JPG)

![image](images/setup_easy_smtp/easy_SMTP_7.JPG)

