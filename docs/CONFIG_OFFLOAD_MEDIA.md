## Setup Offload Media Plugin <a name="offload_media"></a>
- Access domain Wordpress project and press admin's username, admin's password

![image](images/config_offload_media/offload_media_1.JPG)

- Check and turn on if  plugins Offload Media Lite is turn off
![image](images/config_offload_media/offload_media_2.JPG)

- Like bellow is successfull

![image](images/config_offload_media/offload_media_3.JPG)

- Find and choose plugins Offload Media Lite

![image](images/config_offload_media/offload_media_4.JPG)

- We setup bucket to save media like below

![image](images/config_offload_media/offload_media_5.JPG)

![image](images/config_offload_media/offload_media_6.JPG)

![image](images/config_offload_media/offload_media_7.JPG)

- Plugin Offload Media Lite config success when like image below

![image](images/config_offload_media/offload_media_8.JPG)
