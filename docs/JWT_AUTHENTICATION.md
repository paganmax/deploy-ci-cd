# JWT Authentication for WP-API Configuration

1. Generate jwt secret key. using included helper to generate salt by running bellow command.
```bash
php docs/salt
```
2. Copy result of line with key `JWT_AUTH_SECRET_KEY` to your `.env` file
3. To enable CORS for authenticate route, add bellow config to your `.env` file
```
JWT_AUTH_CORS_ENABLE=true
```
4. After all, result will be something same as bellow
```
JWT_AUTH_SECRET_KEY='s*}X+lk,IS_~V5,ur\Lu3w-*lzB&;rfnkhh#v=?]P+bO2=y^E(U!,!@i;,P:BT>t'
JWT_AUTH_CORS_ENABLE=true
```
5. To share cookie with UI (unity), you also need to config cookie domain to something as bellow and put it to your `.env` files
```
COOKIE_DOMAIN=.alpha-tech.net
```
replace `.alpha-tech.net` with your domain, config value need a dot (.) at first to share cookie to domain and all subdomain.
Without this, seamless login can't work and your client will need to manual login when access UI (unity) page.
