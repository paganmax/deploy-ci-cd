# SSL Configuration

## 1. Upload the certificates on the server where your website is hosted

Assumption your certificates containing a Certificate file, with the `.crt` extension, and a Certificate Authority (CA) bundle file, with the `.ca-bundle` extension.

Upload both files to your server whatever way you prefer. By using an FTP client, for example.

## 2. Combine all the certificates into a single file

You need to have all the Certificates (`your_domain.crt` and `your_domain.ca-bundle`) combined in a single `.crt` file.

The Certificate for your domain should come first in the file, followed by the chain of Certificates (CA Bundle).

Enter the directory where you uploaded the certificate files. Run the following command to combine the files:
```
cat your_domain.crt your_domain.ca-bundle >> your_domain_chain.crt
```

Alternatively, you can combine the files using [this online tool](https://decoder.link/) and following the steps below:
- Open `your_domain.crt` file in a text editor and copy the certificate code, including the `-----BEGIN CERTIFICATE-----` and `-----END CERTIFICATE-----` tags.
- Go to [decoder.link](https://decoder.link/) and open the SSL&CSR Decoder tab.
- Paste `your_domain.crt` text code to the required field and hit Decode.
![decoder](images/decoder.link.png "decoder")
- Next, scroll down the results and find Bundle (Nginx) section within the General Information part.
- Click on the floppy disk icon over on the right to download the generated file.
![decoder](images/decoder.link_2.png "decoder")
- The `nginx_bundle.zip` file will be downloaded to your PC. Unzip it and use the `nginx_bundle_l3s4k9n1l0s3.crt` file (the `l3s4k9n1l0s3` part of the name is a random alphanumeric string) for installation.

That's it!

## 3. Creating a separate Nginx server block or Modifying the existing configuration file
To install the SSL certificate on Nginx, you need to show the server which files to use, either by a) creating a new configuration file, or b) editing the existing one.

### a) By adding a new configuration file for the website you can make sure that there are no issues with the separate configuration file. Furthermore, it will be quite easier to troubleshoot the installation in case of any issues with the new configuration.

We suggest creating a new configuration file in this folder: `/etc/nginx/conf.d`
That can be done via this command:
```
sudo nano /etc/nginx/conf.d/your_domain*-ssl.conf
```
Where `your_domain*-ssl.conf` is the name of the newly created file.

Next, copy and paste one of the below server blocks for the 443 port and edit the directories. Ensure the server name and path to webroot match in both the server block for port 80 and the one for port 443. If you have any other important values that need to be saved, move them to the newly created server block too.

### b) Edit the default configuration file of the web-server, which is named `nginx.conf`. It should be in one of these folders:
```
/usr/local/nginx/conf
```
```
/etc/nginx
```
```
/usr/local/etc/nginx
```
You can also use this command to find it:
```
sudo find / -type f -iname "nginx.conf"
```
Once you find it, open the file with:
```
sudo nano nginx.conf
```
Then copy and paste one of the server blocks for the 443 port given below and edit the directories according to your server block for the 80 port (with matching server name, path to webroot, and any important values you need). Alternatively you can copy the server block for 80 port, then paste it below, update the port and add the necessary SSL-related directives.

> Note: Replace the file names values, like `your_domain_chain.crt`, in the server block with your details, and modify the routes to them `using/path/to/`.

```
server {
    listen 443 ssl;
    
    ssl_certificate /path/to/certificate/your_domain_chain.crt;
    ssl_certificate_key /path/to/your_private.key;
    
    root /path/to/webroot;
    server_name your_domain.com;
}
```
- `ssl_certificate` should be pointed to the file with combined certificates you’ve created earlier.
- `ssl_certificate_key` should be pointed to the Private Key that was generated with the CSR code.

Once the corresponding server block is added to the file, ensure you save the edits. Then, you can double-check the changes made with the following steps.

Run this command to verify that the configuration file syntax is ok:
```
sudo nginx -t
```
![nginx](images/nginx_inst_2.png "nginx")

If you receive errors, double check that you followed the guide properly. 
