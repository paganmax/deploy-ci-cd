## Create Cloud Storage Bucket And Get Storage Bucket Key <a name="cloud_storage"></a> 
### Step 1 : Access google cloud storage: https://cloud.google.com/
- Click 'Sign in'

![image](images/create_storage/create_storage_1.JPG)

- Input google account and password

![image](images/create_storage/create_storage_2.JPG)

### Step 2: After 'Sign in'
-  Click console

![image](images/create_storage/create_storage_3.JPG)

- After click console button, the desktop will be displayed as bellow

![image](images/create_storage/create_storage_4.JPG)

- Searching 'cloud storage' and choose as bellow

![image](images/create_storage/create_storage_5.JPG)

- Click  'CREATE BUCKET' button

![image](images/create_storage/create_storage_6.JPG)

### Step 3: Create bucket and set information as bellow and click create

![image](images/create_storage/create_storage_7.JPG)

![image](images/create_storage/create_storage_8.JPG)

![image](images/create_storage/create_storage_9.JPG)

- After creating

![image](images/create_storage/create_storage_10.JPG)

![image](images/create_storage/create_storage_11.JPG)

![image](images/create_storage/create_storage_12.JPG)

![image](images/create_storage/create_storage_13.JPG)

![image](images/create_storage/create_storage_14.JPG)

![image](images/create_storage/create_storage_15.JPG)

![image](images/create_storage/create_storage_16.JPG)

![image](images/create_storage/create_storage_17.JPG)

![image](images/create_storage/create_storage_18.JPG)

![image](images/create_storage/create_storage_19.JPG)

![image](images/create_storage/create_storage_20.JPG)

![image](images/create_storage/create_storage_21.JPG)

![image](images/create_storage/create_storage_22.JPG)

![image](images/create_storage/create_storage_23.JPG)

![image](images/create_storage/create_storage_24.JPG)

![image](images/create_storage/create_storage_25.JPG)

![image](images/create_storage/create_storage_26.JPG)

![image](images/create_storage/create_storage_27.JPG)

![image](images/create_storage/create_storage_28.JPG)

### Step 4: Upload Key To Server
- Use putty key generator ([https://www.puttygen.com/](https://www.puttygen.com/))
- Open putty configuration and setup like bellow

![image](images/create_storage/create_storage_29.JPG)

![image](images/create_storage/create_storage_30.JPG)

- Choose folder contain private key to connect to server

![image](images/create_storage/create_storage_31.JPG)

![image](images/create_storage/create_storage_32.JPG)

![image](images/create_storage/create_storage_33.JPG)

![image](images/create_storage/create_storage_34.JPG)

- Use command like below to access root's role
```shell
sudo su -
``` 

- Use command like below to access project (Change your project folder directory (var/www/your-project-folder))
```shell
cd var/www/your-project-folder
Example: cd /var/www/vrmallwp/current
``` 
![image](images/create_storage/create_storage_35.JPG)

- Use command like below to create a new folder contain key (folder's name can different if you want)

```shell
mkdir shared_cloud_key
``` 
- Use command like below to move on folder contain key

```shell
cd shared_cloud_key
``` 
![image](images/create_storage/create_storage_36.JPG)

- Return user's computer and copy content's key google cloud download in step 3 like below

![image](images/create_storage/create_storage_37.JPG)

![image](images/create_storage/create_storage_38.JPG)

![image](images/create_storage/create_storage_39.JPG)

- Return putty to create file storage content key in server

```shell
nano google-cloud-key.json
``` 
![image](images/create_storage/create_storage_40.JPG)

- Paste content key copy from user's to file like bellow

![image](images/create_storage/create_storage_41.JPG)

- Press 'Ctrl + x' to save
- Press 'Y' to agree

![image](images/create_storage/create_storage_42.JPG)

- Press "Enter" 

- We save key to file successfull if like image below

![image](images/create_storage/create_storage_43.JPG)

- Press command to turn root's project

```shell
cd ..
``` 
- Add role for nginx manpulation with google cloud key like below

```shell
chown -R (your-acount-have-role-manpulation-with-project):(your-acount-have-role-manpulation-with-project)  (folder-name-contain-google-cloud-key you created above)
Example: chown -R vrmall:vrmall shared_cloud_key
``` 

```shell
chmod -R 775 (folder-name-contain-google-cloud-key you created above)
Example: chmod -R 775 shared_cloud_key
``` 
![image](images/create_storage/create_storage_44.JPG)

- Chane path GCP key in env like below

```shell
nano .env
``` 

- Change folder's directory to folder contain key as below

```shell
GCP_KEY= "/var/www/(directory-of-file-Json-of-google-cloud-key)"
Example: GCP_KEY= "/var/www/vrmallwp/current/shared_cloud_key/google-cloud-key.json"
``` 
  
![image](images/create_storage/create_storage_45.JPG)

- Press 'Ctrl + x' to save
- Press 'Y' to agree
- Press "Enter" 
