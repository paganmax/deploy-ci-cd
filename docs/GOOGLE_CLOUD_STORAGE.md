# Configuring CORS for Google Cloud Storage

Cloud Storage allows you to set CORS configuration at the bucket level only. You can set the CORS configuration for a bucket using the gsutil command-line tool, the XML API, or the JSON API.

Here’s an example of setting the CORS configuration with `gsutil`:

### 1. Active key for gsutil connect with  Google Cloud Storage
- We have use key created in [Create Cloud Storage Bucket And Get Storage Bucket Key](CREATE_CLOUD_STORAGE.md)
and press below command
- You can read more about in [Google Cloud Authentication](https://cloud.google.com/storage/docs/gsutil/addlhelp/CredentialTypesSupportingVariousUseCases)
```
gcloud auth activate-service-account --key-file path/your-key.json
Example: gcloud auth activate-service-account --key-file /var/www/vrmallwp/shared_cloud/google-cloud.json
```

### 2. Create a JSON file that contains the following:
```json
[
  {
    "origin": [
      "http://origin1.example.com"
    ],
    "responseHeader": [
      "Content-Type"
    ],
    "method": [
      "GET"
    ],
    "maxAgeSeconds": 3600
  }
]
```
This JSON document explicitly allows cross-origin GET requests from origin1.example.com and may include the Content-Type response header.

- `origin` is an origin allowed for cross origin resource sharing with this bucket. For example, https://example.appspot.com. (In this situation it's your Full URL to Unity home)
- `method` is an HTTP method allowed for cross origin resource sharing with this bucket. For example, `GET` or `PUT`.
- `responseHeader` is a header allowed for cross origin resource sharing with this bucket. For example, `Content-Type`.
- `maxAgeSeconds` is the number of seconds the browser is allowed to make requests before it must repeat the preflight request. For example, `3600`.

- Note that you can specify multiple origins, methods, or headers using a comma-separated list. For example, `"method": ["GET", "PUT"]`.

### 3. Use the [gsutil cors](https://cloud.google.com/storage/docs/gsutil/commands/cors) command to configure CORS on a bucket:
```
gsutil cors set JSON_FILE_NAME.json gs://BUCKET_NAME
```
Where
- `JSON_FILE_NAME` is the path to the JSON file you created in Step 1.
- `BUCKET_NAME` is the name of the bucket. For example, `my-bucket`.

### Viewing the CORS configuration for a bucket
To view the CORS configuration for a bucket:
```
gsutil cors get gs://BUCKET_NAME
```
Where `BUCKET_NAME` is the name of the bucket. For example, `my-bucket`.

### Removing CORS from a bucket
To remove the CORS configuration from a bucket:
#### 1. Create a JSON file that contains the following:
```json
[]
```
#### 2. Use the gsutil cors command to configure CORS on a bucket:
```
gsutil cors set EMPTY_CONFIG_FILE_NAME.json gs://BUCKET_NAME
```
Where
- `EMPTY_CONFIG_FILE_NAME` is the path to the JSON file with the empty CORS configuration you created in Step 1.
- `BUCKET_NAME` is the name of the bucket. For example, `my-bucket`.
