# Table of Contents
1. [Project Structure](#project_structure)
2. [Server Requirements](#server_requirements)
3. [Installation](#installation)
4. [Nginx Config Example](#nginx_config)
5. [Create Cloud Storage Bucket And Get Storage Bucket Key](#cloud_storage)
6. [Setup Offload Media Plugin](#offload_media)
7. [Create Email To Send Email](#setup_email)
8. [Setup Easy WP SMTP](#easy_smtp)

## 1. Project Structure <a name="project_structure"></a>

```
├── composer.json
├── config
│   ├── application.php
│   └── environments
│       ├── development.php
│       ├── staging.php
│       └── production.php
├── vendor
└── web
    ├── app
    │   ├── mu-plugins
    │   ├── plugins
    │   ├── themes
    │   └── uploads
    ├── wp-config.php
    ├── index.php
    └── wp
```

## 2. Server Requirements <a name="server_requirements"></a>
- PHP 7.4.x
- MySQL 8.0.x
- Composer 2.x ([https://getcomposer.org/download/](https://getcomposer.org/download/))
  - [Installation](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos)

## 3. Installation <a name="installation"></a>
3.1. Create a new file name `.env` in project root folder (make a copy from `.env.example` with example configuration)
```shell
cp .env.example .env
```

3.2. Update environment variables in the `.env` file:
- Database variables
  - `DB_NAME` - Database name
  - `DB_USER` - Database user
  - `DB_PASSWORD` - Database password
  - `DB_HOST` - Database host
- `WP_ENV` - Set to environment (`development`, `staging`, `production`)
- `WP_HOME` - Full URL to WordPress home (https://example.com)
- `WP_SITEURL` - Full URL to WordPress including subdirectory (https://example.com/wp)
- `GCP_KEY` - Google cloud platform key files (.json file) to store file to google cloud storage
- `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT` `JWT_AUTH_SECRET_KEY`
  - Generate with [WordPress salts generator](https://roots.io/salts.html)
- Set the document root on your webserver to web folder: `/path/to/site/web/`

3.3. Install dependency by running below command:
```shell
composer install --optimize-autoloader --no-dev
```
3.4. Import DB from `db/vrmallwp_2021-11-01.sql.gz`

## 4.Nginx Config Example <a name="nginx_config"></a> 
```
upstream php {
  server unix:/run/php/php7.4-fpm.sock;
}

server {
  listen 80;
  listen [::]:80;
  server_name vrmall.net;

  return 302 https://$server_name$request_uri;
}

server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  ssl_certificate      /etc/nginx/ssl/vrmall.pem;
  ssl_certificate_key  /etc/nginx/ssl/vrmall.key;

  root /var/www/vrmall/web;
  index index.php index.html index.htm index.nginx-debian.html;
  server_name vrmall.net;

  client_max_body_size 200M;

  location / {
    try_files $uri $uri/ /index.php?$args;
  }

  location ~ \.php$ {
    include fastcgi_params;
    fastcgi_intercept_errors on;
    fastcgi_pass php;
    fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
  }

  location ~ /\.ht {
    deny all;
  }
}
```
## 5.Create Cloud Storage Bucket And Get Storage Bucket Key <a name="cloud_storage"></a> 
### Step 1 : Access google cloud storage: https://cloud.google.com/
- Click 'Sign in'

![image](https://drive.google.com/uc?export=view&id=1WgRW7NDTwMGSi3Wqyz2vJz5EchE1oX2S)

- Input google account and password

![image](https://drive.google.com/uc?export=view&id=1uImboW3zcs8udKUb9NFT1AFtRuCIWOSn)

### Step 2: After 'Sign in'
-  Click console

![image](https://drive.google.com/uc?export=view&id=1G1GMDP5tQ_W3_tiKpEQ-ISZUZ9rpIxiL)

- After click console button, the desktop will be displayed as bellow

![image](https://drive.google.com/uc?export=view&id=1IpSq0Ea_Si6iEkJbs_bA-UtZyWiHF4gY)

- Searching 'cloud storage' and choose as bellow

![image](https://drive.google.com/uc?export=view&id=1ciDTAWdmJzhBKOoT9c4tmdR4VYz3cXHU)

- Click  'CREATE BUCKET' button

![image](https://drive.google.com/uc?export=view&id=1OMPfUYE3_qmx7nBcS_4A8NOxa1VOiYd_)

### Step 3: Create bucket and set information as bellow and click create

![image](https://drive.google.com/uc?export=view&id=1ANTGPZa515s0iPXyPz-y9bQk5xRK-5rL)

![image](https://drive.google.com/uc?export=view&id=1MMcxqDPl2xluiVEwTMoZbM9XyqNRL-TB)

![image](https://drive.google.com/uc?export=view&id=1QRWqN5gwcGF2cckPHjwi_LzNlLsIo5m2)

- After creating

![image](https://drive.google.com/uc?export=view&id=1EhG416IDGFCmdQAguonQv-LhnYRdXPid)

![image](https://drive.google.com/uc?export=view&id=1F7WJjvAQkUttzSrRmZceZ7Gd9SfC_Yof)

![image](https://drive.google.com/uc?export=view&id=1RYygjqVerOJTnTX72R80Lux-oPloUpWr)

![image](https://drive.google.com/uc?export=view&id=1Li9SIlFRTeArTd3_x_MCDTraIKEfLsOP)

![image](https://drive.google.com/uc?export=view&id=1aL16DR18Z800sMnmCvCHmWldyrFpWg1p)

![image](https://drive.google.com/uc?export=view&id=1ze_Qxc7U0zaOjGHmuP7dH7x4DBJ76xl8)

![image](https://drive.google.com/uc?export=view&id=1thEf8HankOiXFoAsnLiMOyUyiVlwcuDT)

![image](https://drive.google.com/uc?export=view&id=1Efi7U-TZJJRGS_swCQff4ULNXyWFjq8a)

![image](https://drive.google.com/uc?export=view&id=1CIGzRkrgzXyphUmwe_1VnUYd8RbndnIe)

![image](https://drive.google.com/uc?export=view&id=12xWmUYBxIhxCWk8sIk8vQG5KdiRNjRIT)

![image](https://drive.google.com/uc?export=view&id=1hazENhkz3emv2OP3YMjJwyvj8vTrsha-)

![image](https://drive.google.com/uc?export=view&id=1ltuYfTAT6fhFTQkNFMjtWhPLuiEFoNQi)

![image](https://drive.google.com/uc?export=view&id=1nkgdRTYhNCmCrf-Z9W2DP0UJMFiqdr5d)

![image](https://drive.google.com/uc?export=view&id=1OBWSJZspimM60BX4fBy9yFVojtE0BNtX)

![image](https://drive.google.com/uc?export=view&id=15w-wPKpn4LZzZwpyixtz9rCHI16Lxr2_)

![image](https://drive.google.com/uc?export=view&id=1x66QTSXJ-qe5x45enYHw34oX3uoSzDAk)

![image](https://drive.google.com/uc?export=view&id=1eIaF69ZTQTp4spu6G5wKqtF-11LWfMtF)

![image](https://drive.google.com/uc?export=view&id=1HFNvKjTr4-ZToUKN7-MTV7j_q4GaG9uu)

### Step 4: Upload Key To Server
- Use putty key generator ([https://www.puttygen.com/](https://www.puttygen.com/))
- Open putty configuration and setup like bellow

![image](https://drive.google.com/uc?export=view&id=1CeMyAwXfsihMmznUZZpw1oyTQa4tComX)

![image](https://drive.google.com/uc?export=view&id=1HeVqQ7SlNXeNKh9NbWbJyBpL-BmG_8Lp)

- Choose folder contain private key to connect to server

![image](https://drive.google.com/uc?export=view&id=1AMldGu0J7cCtEpqt7VDQB_lLMWz3J5cX)

![image](https://drive.google.com/uc?export=view&id=1NhfBl5b0HQoDaEZQpefJQZL8xZg-WLUK)

![image](https://drive.google.com/uc?export=view&id=1-wv6GvTlkSV7NA8uO9eLVRSejnD-VjWn)

![image](https://drive.google.com/uc?export=view&id=1_LAkOOtnu9fOZjs9veFL1lcceBEK_WgG)

- Use command like below to access root's role
```shell
sudo su -
``` 

- Use command like below to access project (Change your project folder directory (var/www/your-project-folder))
```shell
cd var/www/your-project-folder
Example: cd /var/www/vrmallwp/current
``` 

![image](https://drive.google.com/uc?export=view&id=1nXyDknJyYFYLrBBCMbys9ubZCADkM_TY)

- Use command like below to create a new folder contain key (folder's name can different if you want)

```shell
mkdir shared_cloud_key
``` 
- Use command like below to move on folder contain key

```shell
cd shared_cloud_key
``` 

![image](https://drive.google.com/uc?export=view&id=1NpYfQvjVRg7Xt_1KjrL_wdG5_R3BHU9T)

- Return user's computer and copy content's key google cloud download in step 3 like below

![image](https://drive.google.com/uc?export=view&id=1JmqaK-0I4xnwuz3bSkmOHUGr0o5FsnUc)

![image](https://drive.google.com/uc?export=view&id=1uBXldakrTZRiG-I5LdzrvrEZ5Ghc00l5)

![image](https://drive.google.com/uc?export=view&id=17dqeDOV2pLkCg_TLQ7Lfaj9gW5bS7GSu)

- Return putty to create file storage content key in server

```shell
nano google-cloud-key.json
``` 

![image](https://drive.google.com/uc?export=view&id=1nR8k_wunxkViv76XSTFiLcpVvfdNp3Pc)

- Paste content key copy from user's to file like bellow

![image](https://drive.google.com/uc?export=view&id=1IvjaVKA8z_fQeR-O50hApJhOShlyK_c-)

- Press 'Ctrl + x' to save
- Press 'Y' to agree

![image](https://drive.google.com/uc?export=view&id=1BjM5oaSU8QWmU7tezCWXG_5qtul5ZNi8)

- Press "Enter" 

- We save key to file successfull if like image below

![image](https://drive.google.com/uc?export=view&id=1KxFVYTsnYSFIoDeG4rshYcHrIYs9w7Vr)

- Press command to turn root's project

```shell
cd ..
``` 
- Add role for nginx manpulation with google cloud key like below

```shell
chown -R (your-acount-have-role-manpulation-with-project):(your-acount-have-role-manpulation-with-project)  (folder-name-contain-google-cloud-key you created above)
Example: chown -R vrmall:vrmall shared_cloud_key
``` 

```shell
chmod -R 775 (folder-name-contain-google-cloud-key you created above)
Example: chmod -R 775 shared_cloud_key
``` 

![image](https://drive.google.com/uc?export=view&id=1qgGGghvWWY__vm9LPR8u0xccJOsZfrZQ)

- Chane path GCP key in env like below

```shell
nano .env
``` 

- Change folder's directory to folder contain key as below

```shell
GCP_KEY= "/var/www/(directory-of-file-Json-of-google-cloud-key)"
Example: GCP_KEY= "/var/www/vrmallwp/current/shared_cloud_key/google-cloud-key.json"
``` 
  

![image](https://drive.google.com/uc?export=view&id=1UtCDjpOQsbVYwbOLq_P4NrAf27H-cQPc)

- Press 'Ctrl + x' to save
- Press 'Y' to agree
- Press "Enter" 

## 6. Setup Offload Media Plugin <a name="offload_media"></a>
- Access domain Wordpress project and press admin's username, admin's password

![image](https://drive.google.com/uc?export=view&id=1OsBkDdztNB2dfASKHLvsYyS38efB3qcW)

- Find and choose plugins Offload Media Lite

![image](https://drive.google.com/uc?export=view&id=1OFAKYwFsbiMLGW5SwDX21jN5RhLTYarn)

- We setup bucket to save media like below

![image](https://drive.google.com/uc?export=view&id=1KBpou9xLK83yWOCXvyxITd2WyDmSp_TG)

![image](https://drive.google.com/uc?export=view&id=1-ufdg0lZ_lhFvmrA_kcKkqHnvMXFQMcd)

![image](https://drive.google.com/uc?export=view&id=1vjRQMoPvcGQOlmqjG8aaae4J-WE0Jtwi)

- Plugin Offload Media Lite config success when like image below

![image](https://drive.google.com/uc?export=view&id=12843MPg7MHL70XmjPVNCF0-Al1ZrA8OY)

## 7. Create Email To Send Email <a name="setup_email"></a>
### 7.1 Requirement
- Use google email account
- Email must turn on 'Turn your phone to sign in'
- Email must turn on '2 Steps Verification'
- Email must turn on 'App password'
- Your phone must sign in google email setup send email
### 7.2 Steps setup email
- Add email want setup send email to your phone like tutorial of gmail ([https://support.google.com/mail/answer/6078445?hl=en&co=GENIE.Platform%3DAndroid#zippy=%2Cadd-an-account](https://support.google.com/mail/answer/6078445?hl=en&co=GENIE.Platform%3DAndroid#zippy=%2Cadd-an-account)) 
- Sign in account on your pc and setup like below

![image](https://drive.google.com/uc?export=view&id=1qkLgyT8mgPb3MHw1drFGv3CTzq-Yvl0E)

![image](https://drive.google.com/uc?export=view&id=1AvIBVWixG3z3vIDSO-EbOM0jzMFDSsTp)

![image](https://drive.google.com/uc?export=view&id=1GG98B3k-2E3eu9EQmyRxkImabbO0tyZe)

![image](https://drive.google.com/uc?export=view&id=1-dEw1N5KdRAABJlPc0f0nkk2WAb8SVFW)

![image](https://drive.google.com/uc?export=view&id=1WfT3PyfExRqqW0QMQt1MDgzVD_ciXPb5)

![image](https://drive.google.com/uc?export=view&id=18BqerJR4ArosFCa7UdoZZjL9dhRAgPqd)

![image](https://drive.google.com/uc?export=view&id=19ktm-D-FkfmGYV088ECS4QwnoqBNvbRM)

![image](https://drive.google.com/uc?export=view&id=119NmEXTmS5ILLojI2zGT_3f34uD5FIrGf)

![image](https://drive.google.com/uc?export=view&id=1YvLi5Zqegb3FCquGlTP0bCEsEYXo_jhB)

![image](https://drive.google.com/uc?export=view&id=1eT-QaNI9NMa3nM40ETLItYBgJIWtzUt-)

![image](https://drive.google.com/uc?export=view&id=1MPyVWkaaLT8QnMgKNWmnkWG5x19SU3Pc)

![image](https://drive.google.com/uc?export=view&id=11LkuqPfJw9ecCViih8IawrwHvzYLv-Y4)

![image](https://drive.google.com/uc?export=view&id=1LFR_pPkEtTUDvsK8Upc6F6i5ywS7t5gg)

![image](https://drive.google.com/uc?export=view&id=1EOnxGa0kq0fmOELYFZiFawXTogBn5zG9)

![image](https://drive.google.com/uc?export=view&id=1bQ2YLPSpwnjvQtK1r7JerOr-KYs9Ku-N)

![image](https://drive.google.com/uc?export=view&id=1SizEa92pJb5pLwI4yhnFikrzFGSLwf5G)

![image](https://drive.google.com/uc?export=view&id=1FI5RB8StqlHCF-0NPUFA3WmsLK8RL5Ei)

![image](https://drive.google.com/uc?export=view&id=13WpXsxCt-LuziZDlM0FjvR1bjDLyGvqj)

![image](https://drive.google.com/uc?export=view&id=10_WoiM5SXyVySLffOP1px_pP2aM5Pvvf)

![image](https://drive.google.com/uc?export=view&id=1uxuFIXgeQKxioho--HuQc2efhzyW9Hue)

![image](https://drive.google.com/uc?export=view&id=1uKx9DTplzn6XXfShQXe9XfZO853-lFIW)

![image](https://drive.google.com/uc?export=view&id=1jB18sSYlGDjBXbbxKNka9J0oLqilHH3C)

![image](https://drive.google.com/uc?export=view&id=1o9T9e4wd0UKm4O_CH_38aU_V_KpOkFBm)

![image](https://drive.google.com/uc?export=view&id=1nb8aq3jDMZycmO-TUIm3H0TSaPcZ1-Zl)

![image](https://drive.google.com/uc?export=view&id=1rbXGUEWNgldDFC_5p20zCePRIz4vwUIF)

![image](https://drive.google.com/uc?export=view&id=1ErSGBmY9DEqjeitEyftbrAv-EVS0yEHz)

![image](https://drive.google.com/uc?export=view&id=1uoBTCPleG5siB83GXcCdVqA4kJYdHVCk)

![image](https://drive.google.com/uc?export=view&id=1CP1pRHei-S7yZOxiC-3CPMxxXcrpcvqr)

![image](https://drive.google.com/uc?export=view&id=1hr5SWvfI1YkWpWDlF9e-8nJ-yHjCVlgy)

![image](https://drive.google.com/uc?export=view&id=1lHHs_AWYCHlmJwpsvkR-K1cphMdR5P1x)

![image](https://drive.google.com/uc?export=view&id=1BeQUHjklzPLiUQQFLkfsBHhfULh1ySXu)

![image](https://drive.google.com/uc?export=view&id=1IujIhMVOPM4k0B8c6tPbDvCaJ3R4VvIH)

## 7. Setup Easy WP SMTP <a name="easy_smtp"></a> 

- Access domain Wordpress project and press admin's username, admin's password

![image](https://drive.google.com/uc?export=view&id=1-9Y6zhf9GQZAJHVkSEBXVax4E-_ZZuaZ)

- Find and choose plugins Easy WP SMTP

![image](https://drive.google.com/uc?export=view&id=1Fn2O6cINn2PK1xQ9yprth3goK9KLazc3)

- We setup plugin to send email like below

![image](https://drive.google.com/uc?export=view&id=1-8y9D8N2TueTZWQ5Me-XgKIP4tww0mUa)

![image](https://drive.google.com/uc?export=view&id=1CzooBNR4sYIBbu5jNyAzoNRvRSb6a1Ly)

- We test send email like below

![image](https://drive.google.com/uc?export=view&id=1-yYE1nvwKq3zObs2mJgosIGi-5_dgIPS)

![image](https://drive.google.com/uc?export=view&id=16v6KExKG7tYDaljPU7mg4VOJkh9X0wzA)
