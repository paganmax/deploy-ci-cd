## Use Plugin Loco Translate To Translate Wordpress
### Tutorial bellow demo with plugin WooCommerce

- Access server by ssh we have like bellow

![image](images/setting_page_language/change_language_1.JPG)

- Access role's root and access your project's folder with command like bellow
```shell
sudo su -
 ```

```shell
cd /var/www/your-project-folder
Example: cd /var/www/vrmallwp
 ```
![image](images/setting_page_language/change_language_2.JPG)

- Assign ownership of config folder to  www-data (if you not assign it before)
```shell
sudo chown -R www-data:www-data config
 ```
- Assign permissions have read,write,execute on config folder (if you not assign it before)

```shell
sudo chmod -R 755 config
 ```

- Access config folder(folder setting variable for your press) with command

```shell
cd config
 ```
![image](images/setting_page_language/change_language_3.JPG)

- Open file application.php and change command below like image

```shell
nano application.php
 ```

```shell
Config::define('DISALLOW_FILE_MODS', false);
 ```
![image](images/local_translate/local_translate_1.JPG)

- Press 'Ctrl + x' to save
- Press 'Y' to agree
- Press "Enter" 
- Press like below to back root project

```shell
cd ..
 ```
- Run command like below to refresh nginx and fpm

```shell
systemctl restart nginx
 ```

```shell
systemctl restart php7.4-fpm
 ```

- Return your browser and access link bellow

```shell
https://translate.wordpress.org/projects/wp-plugins/woocommerce/
 ```

![image](images/local_translate/local_translate_2.JPG)

- Find and choose your language

![image](images/local_translate/local_translate_3.JPG)

- Choose 'Stable (latest release)'

![image](images/local_translate/local_translate_4.JPG)

- We have like bellow

![image](images/local_translate/local_translate_5.JPG)

- Choose like bellow

![image](images/local_translate/local_translate_6.JPG)

- Your file is download

![image](images/local_translate/local_translate_7.JPG)

- Open your local contain file download 

![image](images/local_translate/local_translate_8.JPG)

Change file's name like bellow

![image](images/local_translate/local_translate_9.JPG)

- Return your web and access domain Wordpress project and press admin's username, admin's password

![image](images/setup_easy_smtp/easy_SMTP_1.JPG)

- Check and turn on if  plugins Loco Translate is turn off

![image](images/local_translate/local_translate_10.JPG)

- Find and choose plugins Loco Translate

![image](images/local_translate/local_translate_11.JPG)

- Choose like bellow

![image](images/local_translate/local_translate_12.JPG)

![image](images/local_translate/local_translate_13.JPG)

![image](images/local_translate/local_translate_14.JPG)

- Upload your choose

![image](images/local_translate/local_translate_15.JPG)

- After that we operation like bellow

![image](images/local_translate/local_translate_16.JPG)

![image](images/local_translate/local_translate_17.JPG)

- Your plugin WooCommerce is translate

![image](images/local_translate/local_translate_18.JPG)








